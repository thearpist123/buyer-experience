title: Events
description: Where you'll find information on the GitLab happenings circuit
header:
  title: GitLab Events
  centered_by_default: true
  text: GitLab is The DevOps Platform that empowers organizations to maximize the overall return on software development. Join an event to learn how your team can deliver software faster and efficiently, while strengthening security and compliance. We'll be attending, hosting, and running numerous events this year, and cannot wait to see you there!
event_videos:
  header: Event Videos
  videos:
    - title: "Commit Virtual - Four Ways to Further FOSS"
      photourl: /nuxt-images/events/1.jpg
      video_link: https://www.youtube.com/embed/8h95PjdUyn4
      carousel_identifier:
        - 'Event Videos'

    - title: "How Delta became truly cloud native"
      photourl: /nuxt-images/events/2.jpg
      video_link: https://www.youtube-nocookie.com/embed/zV_hFcxoN8I
      carousel_identifier:
        - 'Event Videos'

    - title: "The Power of GitLab"
      photourl: /nuxt-images/events/3.jpg
      video_link: https://www.youtube-nocookie.com/embed/tIm643kyQqs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevOps Culture at Porsche - A GitLab success story"
      photourl: /nuxt-images/events/4.jpg
      video_link: https://www.youtube-nocookie.com/embed/O9MdFhaosRo
      carousel_identifier:
        - 'Event Videos'

    - title: "Creating a CI/CD Pipeline with GitLab and Kubernetes in 20 Minutes"
      photourl: /nuxt-images/events/5.jpg
      video_link: https://www.youtube-nocookie.com/embed/-shvwiBwFVI
      carousel_identifier:
        - 'Event Videos'

    - title: "GitLab Product Keynote at Commit SF"
      photourl: /nuxt-images/events/6.jpg
      video_link: https://www.youtube-nocookie.com/embed/lFIk7E38Djs
      carousel_identifier:
        - 'Event Videos'

    - title: "DevSecOps & GitLab's Security Solutions"
      photourl: /nuxt-images/events/7.jpg
      video_link: https://www.youtube-nocookie.com/embed/CjX1TsCZgoQ
      carousel_identifier:
        - 'Event Videos'

    - title: "Gitlab Connect Paris 2019"
      photourl: /nuxt-images/events/8.jpg
      video_link: https://www.youtube-nocookie.com/embed/YwzpNNSdx_I
      carousel_identifier:
        - 'Event Videos'

    - title: "Verizon Connect achieves datacenter deploys in under 8 hours with GitLab"
      photourl: /nuxt-images/events/9.jpg
      video_link: https://www.youtube-nocookie.com/embed/zxMFaw5j6Zs
      carousel_identifier:
        - 'Event Videos'

    - title: "DataOps in a Cloud Native World"
      photourl: /nuxt-images/events/10.jpg
      video_link: https://www.youtube-nocookie.com/embed/PLe9sovhtGA
      carousel_identifier:
        - 'Event Videos'

events:

  - topic: AI in DevSecOps - Hands-on Workshop
    type: Webcast
    date_starts: July 20, 2023
    date_ends: July 20, 2023
    description: Join our exclusive AI in DevSecOps workshop and witness the incredible potential of harnessing Artificial Intelligence (AI) to revolutionize your workflows. The AI in DevSecOps workshop is designed to showcase the immediate productivity boost you can achieve by leveraging an AI-powered DevSecOps platform. 
    location: Virtual
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/workshop-july20-ai-ml-emea.html

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: July 26, 2023
    date_ends: July 26, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Chicago, IL
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: August 17, 2023
    date_ends: August 17, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Melbourne, AU
    region: APAC
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: AWS Summit São Paulo
    type: Conference
    date_starts: August 3, 2023
    date_ends: August 3, 2023
    description: Junte-se a nós presencialmente no AWS Summit São Paulo. Assista ao Keynote, caminhe pela Expo e converse com os especialistas. Veja como seus colegas e concorrentes estão usando a nuvem a seu favor e descubra todas as formas que você pode utilizar a AWS para iniciar, crescer ou impulsionar seus negócios e carreira para o próximo nível. O AWS Summit São Paulo é um evento gratuito.
    location: São Paulo, Brazil
    region: LATAM
    social_tags:
    event_url: https://register.awsevents.com/ereg/index.php?eventid=750069&categoryid=4907060&reference=61ff38a5-c463-45ae-a8ca-d47aa85c0ab8&sc_channel=el

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 12, 2023
    date_ends: September 12, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: London, UK
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 19, 2023
    date_ends: September 19, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Palo Alto, CA
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: Como provar o valor de um CI/CD centralizado em sua organização
    type: Conference
    date_starts: July 13, 2023
    date_ends: July 13, 2023
    description: A prática de integração contínua e entrega contínua (CI/CD) é fundamental para o desenvolvimento de software moderno. No entanto, muitas organizações ainda usam processos manuais, lentos e propensos a erros para criar, testar e implantar software. Imagine como o seu dia a dia mudaria se, em vez de estar em intermináveis ​​reuniões de gerenciamento de mudanças, você pudesse colocar seu código em produção simplesmente integrando-o com o branch master enquanto a automação cuida do resto.
    location: Virtual
    region: LATAM
    social_tags:
    event_url: https://page.gitlab.com/webcast-july13-cicd-em-su-organizacao-LATAM.html

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: September 21, 2023
    date_ends: September 21, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Irvine, CA
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 5, 2023
    date_ends: October 5, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Dallas, TX
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 11, 2023
    date_ends: October 11, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: New York, NY
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 17, 2023
    date_ends: October 17, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Paris, FR
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 19, 2023
    date_ends: October 19, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Berlin, DE
    region: EMEA
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: October 25, 2023
    date_ends: October 25, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Washington, D.C.
    region: AMER
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: DevSecOps World Tour
    type: Conference
    date_starts: November 8-9, 2023
    date_ends: November 8-9, 2023
    description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for everyone—technology champions, executives, and all stakeholders responsible for building, operating, and securing software.
    location: Tokyo, JP - Virutal
    region: APAC
    social_tags:
    event_url: https://about.gitlab.com/events/devsecops-world-tour/

  - topic: GBI Impact CIO Exec Gathering SF
    type: Conference
    date_starts: July 12, 2023
    date_ends: July 12, 2023
    description: Join GitLab and top IT Executives at the GBI CIO Evening Gathering in San Francisco on July 12, 2023. This is an invite-only intimate networking event bringing together the IT community to discuss the top trends taking place not in the future but right now. You'll learn from industry experts are utilizing technology, process, and leadership to gain a competitive advantage. The gathering features over 20 professionals creating a dynamic, agile mix of thought leadership and best practice strategies.
    location: San Francisco, CA
    region: AMER
    social_tags:
    event_url: https://www.gbiimpact.com/evening-gatherings

  - topic: D+CH GitLab Roadshow 2023
    type: MeetUp
    date_starts: April 18, 2023
    date_ends: November 14, 2023
    description: GitLab are on the road and coming to a location near you! Join D+CH GitLab Roadshow 2023 and learn more about DORA and SBOM. We'll bring some interesting content, networking opportunities and a tasty experience! Join the GitLab Roadshow 2023 in one of the main cities in Germany (and Zurich) and reserve your spot today!
    location: Various Germany cities
    region: EMEA
    social_tags:
    event_url: https://page.gitlab.com/dach-roadshows-events.html

  - topic: Crunch Data Conference 2023
    type: Conference
    date_starts: October 5, 2023
    date_ends: October 6, 2023
    description: A great chance to get the experience of DevSecOps and Data stories directly from producers of well-known and trusted DevSecOps platform with more than 30 million users. Firmly believe that story can aid and inspire an audience to try to do amazing things in their data world. You are in the right place if you want to find out how the GitLab Data team embraces DevOps and Open Source culture to move fast in a rapidly growing environment. We will put a spotlight on how to use the advantage of the internal product to make Data Engineers more agile and flexible in their daily job with a mission of assembling great data products.
    location: Budapest, Hungary
    region: EMEA
    social_tags:
    event_url: https://crunchconf.com/2023/talk/radovans-talk

  - topic: GitLab Hackathon
    type: Community Event
    date_starts: July 9, 2023
    date_ends: July 16, 2023
    description: The Hackathon is a virtual event open to anyone who is interested in contributing code, documentation, translations, UX designs and more to GitLab. Prizes are awarded to participants for having merge requests (MRs) merged.
    location: Virtual
    region: Global
    social_tags:
    event_url: https://about.gitlab.com/community/hackathon/
    featured:
    background: /images/community/community-banner.png

  - topic: Hack in the Box - Phuket
    type: Security Conference
    date_starts: August 21, 2023
    date_ends: August 25, 2023
    description: Automated Incident Response - GitLab SIRT goes to Phuket to teach attendees how to develop multiple tools to automate and standardize incident response processes. 
    location: Phuket, Thailand
    region: Global
    event_url: https://conference.hitb.org/hitbsecconf2023hkt/

  - topic: Innovate Victoria
    type: Conference 
    date_starts: July 13, 2023
    date_ends: July 13, 2023
    description: The Innovate VIC Showcase returns to promote digital inclusivity and address critical issues in the state’s Digital Strategy, ensuring benefits for all Victorians. Public Sector Network and EY are collaborating to host this event, where over 300 government leaders and influencers will chart a strategic direction for a digitally inclusive state with skilled public sector workforce and tools for all Victorians. 
    location: Melbourne, Australia
    region: APAC 
    social_tags:
    event_url: https://publicsectornetwork.com/event/innovate-vic-2023/
     
  - topic: CISO Melbourne 2023 
    type: Conference 
    date_starts: July 17, 2023
    date_ends: July 19, 2023
    description: Join us at CISO Melbourne 2023 to share insights on transformative leadership, increasing awareness, taking a holistic approach to cybersecurity, and embracing technology innovation with confidence.
    location: Melbourne, Australia
    region: APAC
    social_tags:
    event_url: https://ciso-mel.coriniumintelligence.com/
 
  - topic: AWS Cloud Day Perth
    type: Conference
    date_starts: July 26, 2023
    date_ends: July 26, 2023
    description: Join us in-person for AWS Cloud Day, Perth to see how you can use AWS to accelerate innovation and uncover new opportunities for your organisation. The event will bring together business and technology leaders who will share how they drive greater efficiencies, modernise, and uncover new opportunities for their businesses using AI/ML, data, analytics, modern applications, and more. 
    location: Perth, Australia 
    region: APAC
    social_tags:
    event_url: https://pages.awscloud.com/aws-cloud-day-perth.html
  
  - topic: GitLab Connect Sydney
    type: GitLab Connect
    date_starts: August 16, 2023
    date_ends: August 16, 2023
    description: GitLab Connect  is the can't miss event  that connects you to the ideas, technologies, and people that are driving business and software transformation. Get inspired by industry leaders, connect with peers, and gain game-changing DevSecOps best practices, as well as insights into the GitLab roadmap. You'll learn from our customers driving innovation as well as key partners as they share their approach on how they can help you on your DevSecOp journey.
    location: Sydney, Australia
    region: APAC 
    social_tags: 
    event_url: https://page.gitlab.com/event_August16_Connect_Sydney_APAC.html
  
  - topic: CISO Singapore 2023
    type: Conference 
    date_starts: August 22, 2023
    date_ends: August 23, 2023
    description: Network with Singapore's Key InfoSec Decision Makers - expand your C-level network at our exclusive, must-attend VIP breakfast, lunch and dinner and find out how your peers are reporting cybersecurity risks to the board to achieve senior buy-in.
    location: Singapore
    region: APAC
    social_tags:
    event_url: https://ciso-sing.coriniumintelligence.com/
  
  - topic: Application Strategy Summit Melbourne 
    type: Conference 
    date_starts: August 22, 2023
    date_ends: August 22, 2023
    description: Applications are a fundamental compenent of how customers interact with business and have greatly empowered the productivity of the workforce. In just under a decade they have gone from a ‘nice to have’ to a must have for many organisations and a necessity for any modern business. This summit will be an opportunity to gain strategic and technical understanding from some of Australia's senior leaders in technology, applications, architecture and engineering.
    location: Melbourne, Australia
    region: APAC
    social_tags:
    event_url: https://forefrontevents.co/event/application-strategy-summit-vic-2023/
  
  - topic: AWS Public Sector Symposium Canberra 
    type: Conference 
    date_starts: August 30, 2023
    date_ends: August 30, 2023
    description: Join us for AWS Public Sector Symposium Canberra 2023, and discover how organisations of all sizes are using Amazon Web Services (AWS) to accelerate innovation and drive their missions forward. Explore how the cloud can help you enhance security, analyse data at scale, advance sustainability, and achieve your mission—faster and at lower cost. Returning in 2023 with all-new content and speakers, this must-attend event features inspiring keynotes from Australia’s tech leaders, breakout sessions, hands-on workshops, networking hubs, and more.
    location: Canberra, Australia
    region: APAC
    social_tags:
    event_url: https://aws.amazon.com/government-education/symposiums/canberra/
  
  - topic: Future of Security, Sydney 2023
    type: Conference 
    date_starts: August 30, 2023
    date_ends: August 30, 2023
    description: The Future of Security, Sydney is FST Media's dedicated security forum for New South Wales, exploring the most pressing issues of cybersecurity, resilience and diligence in the financial services industry. Join us as we help you navigate the key post-pandemic trends in 2023, including digital transformation accelerating cyber-attacks, data security concerns with the next phase of Open Banking, and partnering with key stakeholders and financial regulators to shape security practices.
    location: Sydney, Australia	 	
    region: APAC
    social_tags:
    event_url: https://fst.net.au/event/future-of-security-sydney-2023/
  
  - topic: DevOps Summit Singapore
    type: Conference 
    date_starts:  September 28, 2023
    date_ends: September 28, 2023
    description: The DevOps Summit will bring together 100+ senior ICT and business leaders to discuss the latest trends and best practices in DevOps. The Summit will offer the opportunity for leaders to network, brainstorm and decide future strategies for success. Whilst many are now well on their way with the journey, the question is, what does it take to develop a World Class DevOps strategy?
    location: Singapore
    region: APAC 
    social_tags:
    event_url: https://forefrontevents.co/event/devops-summit-sg-2023/
