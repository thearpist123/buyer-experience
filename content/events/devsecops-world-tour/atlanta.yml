---
  title: "Atlanta World Tour"
  og_title: Atlanta World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/atlanta-hero.png
  twitter_image: /nuxt-images/events/world-tour/cities/atlanta-hero.png

  registrationClosed: true
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Atlanta
    header: Atlanta
    subtitle: June 15, 2023
    location: |
      TK Elevator Innovation and Qualification Center
      780 Circle 75 Parkway
      Atlanta, GA 30339
    image:
      src: /nuxt-images/events/world-tour/cities/atlanta-hero.png
    button:
      text: Register for Atlanta
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 9:30 am
      name: Registration & Breakfast
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab’s Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us as we kick off the GitLab DevSecOps World Tour in Atlanta with a comprehensive overview of the evolving DevSecOps landscape and how DevSecOps is helping organizations confidently secure the software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Christie Lenneville
          title: Vice President of User Experience
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Christie Lenneville.jpeg
          biography:
            text: Christie Lenneville is the Vice President of User Experience at GitLab, which includes Product Design, UX Research, and Technical Documentation. Over the past 4 years, she's led the team on initiatives intended to make GitLab the UX leader in DevSecOps tooling. That's included projects like building the highly regarded Pajamas Design System, redesigning docs.gitlab.com, and launching a new product navigation. Prior to GitLab, Christie led UX teams at General Motors, Rackspace, and HEB.
        - name: David DeSanto
          title: Chief Product Officer
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/David DeSanto.png
          biography:
            text: David DeSanto is GitLab’s Chief Product Officer where he focuses on delivering a stellar product experience to GitLab’s users from startups to global enterprises. David defines GitLab’s product strategy and vision as well as leads the Product division who executes on this vision. David has led the launches of capabilities that are critical to customer success, including expanding GitLab’s security and compliance capabilities, improving enterprise agile planning capabilities, expanding Value Stream Analytics capabilities, introducing AI assisted workflows, and launching GitLab Dedicated.

      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes, too. Join us to hear our vision for the GitLab DevSecOps Platform, take a deep dive into our latest innovations, and learn how you can keep security at the forefront of your growth.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: In conversation with Nandu Shah, Hermeus
      speakers:
        - name: Francis Ofungwu
          title: Global Field CISO
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Francis Ofungwu.jpeg
          biography:
            text: |
              Francis is the Global Field CISO at GitLab and leads the organization’s software security field practice. Francis is currently focused on helping organizations implement a prescriptive security architecture for DevSecOps.

              Francis has two decades of experience in cybersecurity and compliance management. His expertise includes strategic planning, leading cross-functional teams, application security program development, digital privacy, and incident management.

              Prior to joining GitLab, Francis led the design and execution of cybersecurity programs for several large enterprises in various industries.
        - name: Nandu Shah
          title: Vice President of Information
          company: Hermeus
          image:
            src: /nuxt-images/events/world-tour/speakers/Nandu Shah.jpeg
          biography:
            text: Nandu Shah is the Vice President of Information at Hermeus, where he leads the teams responsible for every computer on the ground, including IT, software engineering, DevOps, and cybersecurity. Prior to joining Hermeus, he has spent over 25 years helping teams large and small find truly agile approaches to software engineering and operational challenges at companies such as Asurion, Evernote, KyckGlobal, and UserIQ.

      description:
          text: Join us for a fireside chat with Nandu Shah at Hermeus to learn how Hermeus has implemented DevSecOps best practices into their software development lifecycle to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions with minimal impact to their delivery velocity.
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: David Astor
          title: Solutions Architect Manager
          company: GitLab
          biography:
            text: |
              David Astor is a Solutions Architect Manager with GitLab. He’s worked with numerous organizations of all sizes to assist them in simplifying their DevOps workflows. He has a great passion for learning new things and loves getting to the “wow!” moment with the folks he helps.

              He’s a listener, a story teller, an agilist, a part time runner, an avid college football fan and loyal husband and friend. He’s a father to an incredible daughter and general purveyor of all manner of useless trivia.

              He’s a huge proponent of breaking down walls and getting people to work together. Oh, and dad jokes.
          image:
            src: /nuxt-images/events/world-tour/speakers/David Astor.png

        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg
        - name: Reshmi Krishna
          title: Director, Enterprise Solutions Architecture
          company: GitLab
          biography:
            text: Reshmi has extensive experience in being an engineer and solution architect and has held leadership positions for multiple years. Reshmi leads with empathy, follows servant leadership, values selling strategies, and loves to dive deeper into technical details. Reshmi has expertise in areas such as cloud-native technologies, digital transformation, DevOps, and value stream mapping. Outside of work, Reshmi loves to represent solution architecture in various conferences, meetups, and podcasts. Reshmi recently started writing her own blogs encouraging folks to become solution architects with the hopes that one day, she will no longer be a minority in the field.
          image:
            src: /nuxt-images/events/world-tour/speakers/Reshmi Krishna.jpg
        - name: Robert Jackson
          title: Sr. Solutions Architect
          company: GitLab
          biography:
            text: Rob has played multiple roles over the course of a 25-year career, including sales engineer, solutions engineer, integration engineer, network engineer, and product manager. Throughout all of these engineering positions he never learned how to drive a train, but he was able to experience the digital transformation from traditional data centers to cloud computing from multiple viewpoints. Based in the northeastern United States, he's passionate about solving problems, both human and technical.
          image:
            src: /nuxt-images/events/world-tour/speakers/Robert Jackson.jpg
        - name: Simon Mansfield
          title: Senior Manager, Solutions Architecture
          company: GitLab
          biography:
            text: Simon is a seasoned DevOps and value stream expert with over 17 years in the tech industry. As the Senior Manager of Solutions Architecture at GitLab, he specializes in optimizing software development and delivery processes through cutting-edge DevOps practices. A sought-after speaker, Simon frequently shares his insights on DevOps, automation, and CI/CD pipelines at international conferences. Connect with him on LinkedIn and Twitter for his latest thoughts on the future of software development.
          image:
            src: /nuxt-images/events/world-tour/speakers/Simon Mansfield.jpg
      description:
        text: |
          If your team is looking to optimize your DevSecOps practices, join the GitLab Value Stream experts for an interactive workshop on how to drive visibility and continuous improvement in the software development lifecycle. We’ll introduce you to Value Stream Assessments (VSAs) and give you an opportunity to apply VSA methodologies to your teams to identify key areas for improvement. You’ll also learn how to accelerate value streams to generate business value more quickly. At the end of the workshop, you’ll have a solid understanding of VSA principles and practical tools and techniques to set you ahead on the path to improving your DevSecOps processes.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: 'Customer Spotlight: Ankur Marfatia from Delta Airlines'
      speakers:
        - name: Ankur Marfatia
          title: DevSecOps Experience Manager
          company: Delta Airlines
          image:
            src: /nuxt-images/events/world-tour/speakers/Ankur Marfatia.jpeg
          biography:
            text: |
              Ankur is currently an Agile Engineering Advocate at Delta Airlines. His team has set up the development standards for Delta in line with Delta's short-term and long-term visions. The team regularly organizes and facilitates enterprise-wide developer discussions on various topics. Ankur also develops and conducts both in-person and video-based corporate training on the newest tools and technology. He works periodically with the Enterprise Architecture group to evaluate and create proposals for the adoption of the latest technologies at Delta.

              Ankur has been at Delta for 15+ years. Over the years, he has enjoyed working in various roles as Project Architect, Scrum Master, as well as Senior Developer working on frontend and backend applications, SOAP, and API services. He likes learning new things as well as participating in innovation challenges. Off work you can often find him at various meetups around Atlanta.
      description:
          text: Join Ankur Marfatia, Agile Engineering Advocate at Delta Airlines, to learn how Delta's technology team empowers the business by delivering delightful customer and developer experiences. Find out how they digitally transformed, innovated, and are delivering new, cutting-edge services to customers and stakeholders.
    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            Which is more difficult: technology or culture? We’ve assembled industry leaders for an in-depth discussion on the role and impact of people and culture on DevSecOps processes. What are effective strategies for introducing DevSecOps into businesses? Is education on Agile a good place to start when thinking about building a DevSecOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability, and develop a human-centered design for DevSecOps processes.
    - time: 4:10 pm
      name: Closing Remarks
      speakers:
        - name: Kate Demarest
          title: Director, Corporate Events
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Kate Demarest.jpeg
    - time: 4:30 pm
      name: Networking Reception
  form:
    header: Register for DevSecOps World Tour in Atlanta
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: 3596
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
  disclaimer: 
    header: Notice of Photography and Filming
    text: Photography and filming are taking place at this event. GitLab reserves the right to use content captured depicting your likeness for any purpose. If you do not wish to be included in such content, notify the photographer and GitLab will use reasonable endeavours to comply with your request.