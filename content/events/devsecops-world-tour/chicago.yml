---
  title: "Chicago World Tour"
  og_title: Chicago World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/Chicago illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/Chicago illustration.png
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Chicago
    header: Chicago
    subtitle: July 26, 2023
    location: |
      Chicago Sports Museum
      835 N Michigan Ave
      Chicago, IL 60611
    image:
      src: /nuxt-images/events/world-tour/cities/Chicago illustration.png
    button:
      text: Register for Chicago
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 9:30 am
      name: Registration & Breakfast
      speakers:
        - name: Kate Demarest
          title: Director, Corporate Events
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Kate Demarest.jpeg
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Dave Steer
          title: VP of Brand & Product Marketing
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/Dave Steer.jpg
          biography:
            text: |
              Dave Steer is the Vice President of Brand and Product Marketing at GitLab. Dave leads the team responsible for the company's positioning and messaging strategy, serving at the intersection of product, sales, customer success, and marketing. Dave has more than 20 years of marketing leadership experience at some of the most iconic consumer and business technology brands, including Facebook, Twitter, eBay, PayPal, and Cloudflare.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us as we kick off the GitLab DevSecOps World Tour in Chicago with a comprehensive overview of the evolving DevSecOps landscape and how DevSecOps is helping organizations confidently secure the software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: David DeSanto
          title: Chief Product Officer
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/David DeSanto.png
          biography:
            text: David DeSanto is GitLab’s Chief Product Officer where he focuses on delivering a stellar product experience to GitLab’s users from startups to global enterprises. David defines GitLab’s product strategy and vision as well as leads the Product division who executes on this vision. David has led the launches of capabilities that are critical to customer success, including expanding GitLab’s security and compliance capabilities, improving enterprise agile planning capabilities, expanding Value Stream Analytics capabilities, introducing AI assisted workflows, and launching GitLab Dedicated.
        - name: Justin Farris
          title: Senior Director, Product
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Justin Farris.jpeg
          biography:
            text: Justin Farris is currently the Senior Director of Product at GitLab. With a background in Product Management, Strategy and Growth across numerous verticles. Justin brings a wealth of knowledge to help teams grow, scale & succeed. Prior to his role at GitLab, Justin served as the Director of Product at Zillow overseeing Growth teams.

      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes, too. Join us to hear our vision for the GitLab DevSecOps Platform, take a deep dive into our latest innovations, and learn how you can keep security at the forefront of your growth.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: In conversation with Hank Preston, Cisco Systems
      speakers:
        - name: Hank Preston
          title: Principal Engineer
          company: Cisco Systems
          image:
            src: /nuxt-images/events/world-tour/speakers/Hank Preston.jpeg
          biography:
            text: Hank Preston is a Principal Engineer for Learning at Cisco. By "day", Hank is leading an aggressive project to modernize and optimize the architecture, design, engineering and operations of the platform that deliver the large catalog of Cisco Certification classes, labs, demos, and exams. This multi-year project balances the daily operational needs of running a multi-million dollar business with the need for robust transformation. Much of his focus revolves around guiding a team of skilled engineers in evolving to an agile and "as code" approach to infrastructure management, but he takes great joy in laying down some code each week. Hank is super passionate about helping bring networking and network engineers into the new programmable age, and loves to talk with anyone about they own journey.
        - name: Nico Ochoa
          title: Senior Major Account Executive
          company: GitLab
          biography:
            text: Nico Ochoa has been in the software development space for 20 plus years. Starting off at Rational Software Development which was later acquired by IBM. Nico's career has been spent helping Fortune 500 companies find inefficiencies in their software development factories. His work at GitLab focuses on developer experience/efficiency, software supply chain security all while supporting positive business outcomes for the enterprise. 
          image: 
            src: /nuxt-images/events/world-tour/speakers/Nico Ochoa.jpg
      description:
          text: Join us for this fireside chat and hear from Cisco on how they successfully transformed their DevOps processes. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: Brian Henzelmann
          title: Manager, Solutions Architecture
          company: GitLab
          biography:
            text: After 6 years enabling DevOps and coaching quality engineering for large enterprises across the Midwest, Brian Henzelmann now leads Enterprise Solutions Architecture for US West at GitLab. Coupling this with his over 10 years of hands-on experience as a full stack engineer delivering cloud native software gives him a unique vantage point for driving improved efficiencies in complex software value streams.
          image:
            src: /nuxt-images/events/world-tour/speakers/Brian Henzelmann.jpg
        - name: Christian Weber
          title: Senior Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Christian Weber.jpeg
          biography:
            text: |
              Christian is a Senior Solutions Architect at GitLab! After spending the early part of his career in Financial Technology, Christian focuses on helping Developers use their tools more efficiently. In his spare time, Christian hobbies around with Photography, Flight Simulation, and tickles a piano key from time to time. Christian is based in Chicago, IL with his wife and newborn twins!
        - name: John Bush
          title: Solutions Architect
          company: GitLab
          biography:
            text: |
              John Bush is a Solutions Architect for GitLab from Saint Louis, Missouri. He started his IT career at age twelve by writing in BASIC on his parent’s Apple II, and has since gained extensive experience as a software engineer and technical support provider for his family. During his leisure time, John enjoys hiking and is on a mission to visit all national parks.
          image:
            src: /nuxt-images/events/world-tour/speakers/John Bush.jpg
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg
        - name: Sarah Bailey
          title: Manager, Solutions Architecture
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/sarah-bailey.jpeg
          biography:
            text: |
              Ever since she can remember, Sarah Bailey has had a deep passion for automation. With a strong focus on DevOps and testing, she has accumulated a decade of experience in leadership and architect roles, collaborating with multiple Fortune 100 companies. Sarah takes great pride in her ability to accelerate processes while maintaining high quality through her work in building test automation frameworks and driving efficient solutions for Big Data and DevOps environments.

              One of Sarah's notable achievements was her successful introduction of test automation to an organization heavily reliant on manual processes. Through her efforts, she empowered developers to seamlessly transition their code from Jenkins to GitLab, benefitting the entire enterprise. Sarah found immense satisfaction in this endeavor, which further solidified her decision to pursue opportunities at GitLab.

              At GitLab, Sarah is thrilled to bring together her areas of expertise, combining her passion for DevOps and automation. She has been at GitLab for over two years and is a leader for the Solution Architecture Commercial team for the West and Central segments. 
      description:
        text: |
          If your team is looking to optimize your DevSecOps practices, join the GitLab Value Stream experts for an interactive workshop on how to drive visibility and continuous improvement in the software development lifecycle. We’ll introduce you to Value Stream Assessments (VSAs) and give you an opportunity to apply VSA methodologies to your teams to identify key areas for improvement. You’ll also learn how to accelerate value streams to generate business value more quickly. At the end of the workshop, you’ll have a solid understanding of VSA principles and practical tools and techniques to set you ahead on the path to improving your DevSecOps processes.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: Bringing GitLab to work
      speakers:
        - name: Steven Pritchard
          title: Vice President, Infrastructure and Security
          company: Sicura
          image:
            src: /nuxt-images/events/world-tour/speakers/Steven Pritchard.jpg
          biography:
            text: |
              As an amateur programmer, Steven Pritchard founded the Southern Illinois Linux Users Group in 1994, mostly for an excuse to work with Linux more. Little did he know that this would lead to lengthy careers working with free software for both himself and his future wife Kara, as well as long-term inclusion in a number of open-source communities such as Fedora, Puppet, and most recently GitLab.

              Currently Steven is the Vice President, Infrastructure and Security at Sicura, a startup specializing in compliance automation. He lives in Fairview Heights with Kara, their daughter Emma, and a few cats.
      description:
          text: Join us for a conversation with Steven Pritchard, a GitLab Hero and open source advocate. Discover why Steven loves the GitLab DevSecOps platform, how he contributes as a community leader, and what empowers developers to push their ideas to new heights. Engage with this true advocate of collaborative development in this inspiring conversation and learn more about how GitLab fosters innovation with the community.
    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 4:10 pm
      name: Closing Remarks
      speakers:
        - name: Kate Demarest
          title: Director, Corporate Events
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Kate Demarest.jpeg
    - time: 4:30 pm
      name: Networking Reception
  form:
    header: Register for DevSecOps World Tour in Chicago
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: 3662
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
