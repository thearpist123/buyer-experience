---
  data:
    customer: The Zebra
    customer_logo: /nuxt-images/logos/zebra.svg
    heading: How The Zebra achieved secure pipelines in black and white
    key_benefits:
      - label: Enhanced CI/CD
        icon: increase-thin
      - label: Less software maintenance
        icon: bulb-bolt
      - label: Improved developer workflow
        icon: code
    header_image: /nuxt-images/customers/thezebra_header.jpeg
    customer_industry: Technology
    customer_employee_count: 350
    customer_location: Austin, TX
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: fewer tools
        stat: 66%
      - label: faster deployments
        stat: 2x
    blurb: The Zebra adopted GitLab to replace GitHub and Jenkins for source code management, CI/CD, and security.
    introduction: |
      The Zebra, an online insurance comparison site, adopted GitLab for SCM, CI/CD, and SAST and DAST.
    quotes:
      - text: |
          The biggest value (of GitLab) is that it allows the development teams to have a greater role in the deployment process. Previously only a few people really knew how things worked, and now pretty much the whole development organization knows how the CI pipeline works, can work with it, add new services, and get things into production without infrastructure being the bottleneck.
        author: Dan Bereczki
        author_role: Sr. Software Manager
        author_company: The Zebra
      - text: |
          We've gone from deploying once a week, to twice a week. We're getting to a place where we're getting comfortable with our testing verification. We'll be at continuous deployment hopefully by the end of the year and be able to deploy at will.
        author: Dan Bereczki
        author_role: Sr. Software Manager
        author_company: The Zebra
    content:
      - title: 'Insurance in black and white'
        description: |
          The Zebra was established to provide customers with a simplified way to compare insurance providers. Established in 2012, The Zebra is an online insurance comparison shop that researches car insurance options and delivers the best rates available. [The Zebra](https://www.thezebra.com/about/) has recently expanded into homeowners and renters insurance.
      - title: Too many plugins without any advantages
        description: |
          The Zebra was using GitHub as its repository and Jenkins for deployments. The teams were also using Terraform to deploy to AWS. The number of Jenkins plugins created an overwhelming amount of management work. On top of that, the variety of plugins caused security vulnerabilities because some tools were no longer supported or too fragile to update in the deployment environment.

          “The biggest problem that we were having was that we were using Jenkins to do our deploys before GitLab. We put enough plugins into that. It was so fragile that nobody wanted to touch it,” said Dan Bereczki, Sr. Software Manager. “Anybody that did try to touch it broke it, and then deploys were down for half a day or a day to try to fix things, or keep things all upgraded.”

          Teams wanted to improve the existing CI/CD process, but that meant adding plugins to Jenkins, further complicating the existing level of maintenance. The Zebra needed a new solution that would integrate testing and security, as well as allow for deploys to a [variety of different platforms](/partners/){data-ga-name="different platforms" data-ga-location="customers content"}.
      - title: A fast migration with zero plugins
        description: |
          The Zebra researched a variety of platforms to replace the existing plugins and ease management stress. They adopted GitLab because it provides an enhanced repository without having to manage plugins. Moreover, the CI/CD capabilities were the selling point.

          On top of that, the teams were eager to adopt GitLab because it offers features that other solutions don’t offer, like built-in security. “People realized how much more control they had of their processes, and how easy it was to make the transition. We got the migration done in under three months,” Bereczki said. Ninety-five percent of Jenkins code was migrated over in that time, and they have since completely moved off of Jenkins and GitHub.

          All six of the application development teams and even a few other teams outside of development are using GitLab. “Now instead of one or two people who understand the intricacies of Jenkins and can fix the things that are problematic, everybody knows how to work with the GitLab pipeline,” Bereczki said. The teams went from using 3 tools — GitHub, Codeship CI, and Jenkins Deploy — to using only [GitLab CI/CD](/features/continuous-integration/){data-ga-name="continuous integration" data-ga-location="customers content"}, fully integrated and fully automated.
      - title: One platform, many solutions
        description: |
          With GitLab, The Zebra can now focus on moving towards continuous deployment because teams can deploy at will, without waiting for other schedules. All of the development teams have a greater role in the deployment process because they understand how the CI pipeline works and can work within it. On top of that, the infrastructure is no longer a bottleneck for deployment.

          The workflow usually begins with a request from marketing. From there, it becomes a technical brief which gets broken into a set of JIRA tickets and then assigned to the appropriate team. It then gets worked on, code gets generated and goes into the GitLab repository. Then, the team will use the GitLab CI/CD pipeline to get it deployed in the development environment. Terraform is used to implement infrastructure as code to ensure configuration changes are maintained throughout the test and deployment process.

          Teams use Amazon EKS with RDS. Traffic routing is initially handled by Cloudflare, then internal Elastic Load Balancing. When developers need to connect The Zebra services to outside, third-party services they use Amazon Virtual Private Cloud. “We don’t want systems where it’s this black box that nobody knows how it works. We’re slowly getting rid of that,” Bereczki said.

          GitLab has enabled cross-functional relationships between the development teams because now they own their own code all the way into production. Developers can understand each step to deployment and can work through any issues and make changes without worrying about disrupting other parts of the workflow.

          [GitLab SAST and DAST](/solutions/security-compliance/){data-ga-name="sast and dast" data-ga-location="customers content"} makes it easier for compliance for SOC2 Type 1 certification and now the teams are in the middle of SOC2 Type 2 certification. It has also provided additional testing and security measures reducing their risk. “The biggest impact is, we’ve got a whole bunch of vulnerabilities that we didn’t even know were there, that we’re dealing with right now. We’re remediating those,” Bereczki states. They have eliminated all identified Criticals and Highs on four projects so far. “The nice thing about it is that, since it’s part of the pipeline now, we won’t be playing catch up for when we schedule a penetration test, or when we run some quarterly or biannual tests,” added Bereczki.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
