---
  title: University of Cambridge
  description: University of Cambridge Information Services paves path to DevOps with GitLab
  image_title: /nuxt-images/blogimages/cambridge_university_cover_image.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/cambridge_university_cover_image.jpg
  data:
    customer: University of Cambridge
    customer_logo: /nuxt-images/logos/university-cambridge-logo.png
    heading: University of Cambridge Information Services paves path to DevSecOps with GitLab
    key_benefits:
      - label: Increased collaboration
        icon: collaboration
      - label: Enables self-service
        icon: user-laptop
      - label: Successful workflow implementation
        icon: accelerate
    header_image: /nuxt-images/blogimages/cambridge_university_cover_image.jpg
    customer_industry: Education
    customer_employee_count: 24,450 students
    customer_location: Cambridge, UK
    customer_solution: |
      [GitLab Self Managed Ultimate](/pricing/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: Project growth
        stat: 500 to 4,000
      - label: Issue tracking growth
        stat: 100s to 50,000
      - label: Merge request growth
        stat: 0 to 12,700+
    blurb: University Information Services (UIS) is helping to drive improved collaboration across departments and evolve new processes along with new digital transformation initiatives.
    introduction: |
        The need to keep pace with a more rapid rate of versioning, as well as new demands of global research, led a leading university to explore DevOps software.
    quotes:
      - text: |
          Cloud native GitLab efforts aligned well with our internal deployment processes.
        author: Dr Abraham Martin
        author_role: Head of Development and Operations (DevOps)
        author_company: University of Cambridge
    content:
      - title: Leading Oxbridge University transforms its DevSecOps function
        description: |
          At the the Development and Operations (DevOps) Division within University Information Services (UIS) at the University of Cambridge, GitLab now serves as part of efforts to manage software builds and updates across a highly active and varied user base. The GitLab instance is maintained by the Development and Operations (DevOps) Division within University Information Services (UIS) at the University of Cambridge. 
          
          It has used GitLab as it works to achieve better visibility and collaboration in and among diverse academic and research programs. Such tooling is part of the effort to continually improve the educational and research capabilities of the institution. UIS provides University-wide services to staff and students with a wide scope of requirements. These range from Student Groups like the Cambridge University Robotics Lab or Cam FM, Cambridge and Anglia Ruskin Student Radio, to Research Groups like the Computational and Digital Archaeology Lab; the Cambridge Advanced Imaging Centre; Cancer Research UK, Cambridge Institute; and the Wolfson Brain Imaging Centre. UIS also supports academic services, including Moodle and student admissions systems that were stress tested during the Covid-19 pandemic. This all translates into a dynamic, ever-changing environment. The focus is on delivering internal services that display the same fit and finish that public-facing services present.
      - title: Fragmented tools led to inefficiencies and siloed teams
        description: |
          UIS faced a number of issues that led teams to explore new source code management options. It was difficult to keep up with the rapid pace of modern versioning, or gain adequate visibility into operations. Teams wanted to evolve new paths toward automated software development practices. Among the challenges identified by UIS was the deployment of software components that required separate and continual on-premises tool versioning. Also, development teams looked to ensure academic researchers' efforts to use more open data sets, and open-source software development, were supported. Add to this challenge the fact that different University departments were maintaining and using their own different tooling. 
          
          Overall, a more unified approach to the process was desired. After a series of interviews with users, the group chose to upgrade from basic use of Git repository services, while ensuring reliable continuity for long-running research projects. 
      - title: A robust code repository facilitates collaboration
        description: |
          GitLab was employed to help support production, staging and development for the group, as well as to achieve greater coherence for development and operations. GitLab's open-source lineage also fits within University of Cambridge's overarching goal to support open-source culture. “Having an institutionally supported git hosting service provides a useful way to share materials collaboratively with colleagues. Mirroring functionality lets me keep copies of open-source projects that I am using in corpora for research" explains Professor Andrew Rice, Computer Science in The University of Cambridge Department of Computer Science and Technology.
      - title: A unified, optimized workflow
        description: |
          GitLab is used to integrate the varied parts of full-cycle DevOps over the array of the University's customer departments. In doing this, it allows the UIS team to focus on deploying a single tool that enables a broad range of development options. The software provides a unified platform for complex pipelines, including building, different types of testing, integration, and deployment. Agile responses to pressing pandemic challenges were among the successes for these projects. While the University had already started developing digital tooling to aid admission processes, the events of 2020 brought this into sharp focus. GitLab served as a central tool in adapting to the new normal. GitLab CI/CD tooling was instrumental in allowing daily and even hourly releases - some of which by necessity were continually adjusted to the changing demands of pandemic response. GitLab has supported the University to continue its focus on innovation and providing its stakeholder groups with one DevOps platform.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/

