---
  title: OW2
  description: OW2 may not develop software, but the consortium is using GitLab to bolster the European and Chinese open source communities and provide development teams with the tools they need to do their jobs even better
  image_title: /nuxt-images/blogimages/ow2-case-study-image.png
  image_alt: 
  twitter_image: /nuxt-images/blogimages/ow2-case-study-image.png
  data:
    customer: OW2
    customer_logo: /nuxt-images/customers/ow2-logo.svg
    customer_logo_full: true
    heading: Global nonprofit OW2 uses GitLab to help the developer community create software more efficiently
    key_benefits:
      - label: Open source
        icon: open-source
      - label: Standard environment
        icon: bulb-bolt
      - label: Improved productivity
        icon: increase
    header_image: /nuxt-images/blogimages/ow2-case-study-image.png
    customer_industry: Computer software
    customer_employee_count: 2-10
    customer_location: Paris, France
    customer_solution: |
      [GitLab Core](/solutions/open-source/){data-ga-name="open source solution" data-ga-location="customers hero"}
    blurb: OW2 is a global, independent nonprofit dedicated to the development and promotion of open source software infrastructure technology and middleware.
    introduction: |
      OW2 may not develop software, but the consortium is using GitLab to bolster the European and Chinese open source communities and provide development teams with the tools they need to do their jobs even better
    quotes:
      - text: |
          It’s good to make this transition in terms of our image because it shows the community that we are providing cutting-edge technology.
        author: Stéphane Laurière
        author_role: Chief Technology Officer
        author_company: OW2
    content:
      - title: Supports and promotes the open source community in Europe and China
        description: |
          [OW2](https://www.ow2.org/){data-ga-name="ow2" data-ga-location="body"} was borne out of a desire to create a European community around open source software. The first iteration of this community was called ObjectWeb. Founded in 2007, OW2 is now a global nonprofit, open source organization supporting a code base of some 100 infrastructure software projects for enterprise information systems. In its commitment to grow the open source ecosystem, OW2 acts as an industry platform that connects open source developers to one another and software users and promotes the quality and market usability of its software. OW2 membership is open to corporations, public entities, educational, and nonprofit organizations as well as individuals. OW2 member organizations include the City of Paris, Airbus, Engineering Group, the University of Oslo, Rocket.Chat, Inria, Orange, Bonitasoft, and the French Ministry of the Interior.
      - title: Need for a more integrated, easy-to-use tool with a large open source community
        description: |
          Motivated by its duty to grow, promote, and create tools for the open source community, OW2 decided it was time to move to a more integrated issue tracking tool with an easy-to-use interface and comprehensive set of features. As a nonprofit positioning itself as a business ecosystem platform for open source software development, it was imperative that their new tool have a robust open source community.

          “The open source aspect is our heart,” said Stéphane Laurière, Chief Technology Officer at OW2. “It was absolutely key for us to use an open source project in as many ways as we could. The open source strategy, and commitment to it by GitLab, was absolutely key to us.”
      - title: GitLab Core gets nonprofit in line with open source industry trends
        description: |
          After testing out a few software development and management products, OW2 selected [GitLab](/pricing/){data-ga-name="gitlab pricing" data-ga-location="customers content"} as their tool of choice to replace JIRA, Bamboo and GForge. In addition to the open source structure and vast set of features, OW2 found GitLab’s workflow to be fast as well as easy to follow and upgrade – making it a good fit for their member organizations.

          “OW2 does not develop software, we are providing tools to help others develop software in an efficient manner,” Laurière explained. “The pace of evolution with GitLab was very complete.”

          In addition, many of OW2’s university members use GitLab internally, so it was “another strong argument for us to move to GitLab and to remain in the same environment within the community.”

          OW2 is currently in the midst of [transitioning to GitLab](https://gitlab.ow2.org/explore){data-ga-name="ow2 project repository" data-ga-location="customers content"}, but has already begun using a number of the product’s features, including GitLab CI, GitLab Pages, issue tracking and the Docker Registry.

      - title: A new open source tool is created as confidence in OW2’s innovation is set to grow
        description: |
          OW2 may still be in the midst of the GitLab migration process, but the transition has already moved the organization forward in its mission to create tools that fill gaps in the open source space and help developers do their jobs more dynamically.

          “We have developed code for migrating JIRA and GForge to GitLab,” Laurière said. “We are introducing code we haven’t found in other migration tools. These things include the capacity to keep the references between issues, and also the dates of the issues, and the references between the commits and the issues. All of these kinds of references were very important to our community and we developed Python scripts that we were eager to share with the community. They are available in open source in our GitLab.”

          OW2 has also found GitLab to be a boon to their internal workflow.

          “We are using GitLab for issue management,” Laurière detailed. “We appreciate all the issue tracking and workflow that we get from GitLab on a daily basis to improve our productivity.”

          Laurière is also excited about the long-term impact the transition will have on the nonprofit’s reputation and its OW2 Forge application, which now lives on GitLab and provides technical support to projects via a number of tools, including debugging, code contribution management, licenses, contributors, and much more.

          “It’s good to make this transition in terms of our image as it demonstrates our commitment to provide our community with cutting-edge technology,” Laurière said. “And it’s easier to work with GitLab than GForge. The use of GitLab will be a good opportunity for innovative projects to use the consortium. OW2 Forge is now becoming a real engine and very advanced in terms of features and quality.”
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
