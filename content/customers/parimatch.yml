---
  data:
    title: Parimatch
    description: How Parimatch scores big with GitLab
    og_title: Parimatch
    twitter_description: How Parimatch scores big with GitLab
    og_description: How Parimatch scores big with GitLab
    og_image: /nuxt-images/blogimages/pmbet_cover.jpg
    twitter_image: /nuxt-images/blogimages/pmbet_cover.jpg

    customer: Parimatch
    customer_logo: /nuxt-images/logos/Logo-Parimatch-yellow-CMYK.png
    heading: How Parimatch scores big with GitLab
    key_benefits:
      - label: Enhanced collaboration
        icon: collaboration
      - label: Intuitive user interface
        icon: dev-enablement
      - label: Increased operational efficiency
        icon: accelerate-thin
    header_image: /nuxt-images/blogimages/pmbet_cover.jpg
    customer_industry: Gaming
    customer_employee_count: 2,000
    customer_location: Cyprus
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: User growth in one year
        stat: 57%
      - label: Fewer tools
        stat: 75%
    blurb: Parimatch, a gaming platform designed for football fans to bet on matches, adopted GitLab to eliminate toolchain maintenance
    introduction: |
      Learn how GitLab Premium helps Parimatch enhance visibility, collaboration, and integration management
    quotes:
      - text: |
          We chose GitLab because it’s the industry standard and a good open-source project for any development process
        author: Anatolii Kovalenko
        author_role: Senior DevOps Engineer
        author_company: Parimatch
    content:
      - title: A gaming platform dedicated to football fans
        description: |
          Parimatch is a popular platform football fans use to bet on matches. With a rich 25-year history, it's known for providing leaderboard competitions, boosted odds, and cash-back incentives to encourage fans to join in the fun of betting before and during matches. Created by football fans, Parimatch strives to provide fans with a unique and engaging experience.
      - title: Complex integration management and insecure infrastructure
        description: |
          Like many scaling organizations, Parimatch struggled to increase operational efficiency while managing and maintaining multiple tools. Developers used a variety of tools according to their personal preferences, so the team lacked a cohesive, standard workflow. They also lacked the ability to collaborate easily without having a singular solution in place.

          The team also spent a significant amount of time maintaining a complicated toolchain and combating context switching as they moved from one application to the next. The time spent on maintenance pulled developer focus away from feature development and slowed velocity.

          In addition to workflow difficulties, Parimatch also experienced a unique regulation challenge. As an organization that operates in the gaming industry and handles financial information, Parimatch has to adhere to strict security and compliance requirements. Its previous infrastructure didn’t meet the company's stringent regulations.

          In the past, the development team was able to get by using legacy tools, like Jenkins and TeamCity, but an enhanced focus on modernizing tools, maintaining compliance, and simplifying the development workflow inspired them to seek a new solution.
      - title: A single application simplifies integrations and increases collaboration
        description: |
          In assessing the team’s needs, Parimatch’s ideal solution called for a single application that enabled multiple integrations, features that created a secure infrastructure, and an open-source foundation.

          When considering options, the team immediately dismissed GitHub based on cost and infrastructure incompatibility. They also decided Jenkins was too complex and clunky for their needs. Team members had been using GitLab Free for several years and expressed satisfaction with its features and and their experience with it, so they moved 458 developers onto the GitLab platform.

          Fully adopting GitLab easily solved the integration issue that had been frustrating the team for quite some time. “We chose GitLab because it makes it easy to maintain multiple integrations in one place. If teams prefer different tools, they can continue to use them seamlessly without struggling with complex workflows or context switching. Some developers prefer Jira, and GitLab provides a simple integration to satisfy those contributors,” says Anatolii Kovalenko, Senior DevOps Engineer.

          GitLab’s features meet Parimatch’s commitment to [compliance and security](/solutions/dev-sec-ops/){data-ga-name="compliance and security" data-ga-location="body"}, and the team appreciates that they can manage their own instance. Developers have been happy with GitLab’s user-friendly user interface (UI). “GitLab is the best wrapper for Git, and the web interface is very intuitive. In comparison with Jenkins, GitLab has a much nicer UI,” Kovalenko said.

          Using GitLab, Parimatch increased operational efficiency and collaboration across the entire software development lifecycle.


      - title: Increased operational efficiency
        description: |
          Parimatch has experienced an increase in collaboration as teams across the software development lifecycle embraced GitLab. “Many teams use GitLab, including developers, IT, security, and DevOps engineers. Everyone can cooperate efficiently with each other to meet their needs. Every team has its own criteria on how to deliver value, and now we can answer questions in a single place,” Anatolii explains. Because many teams now are using GitLab, there is an increase in visibility and communication, which makes for a stronger application.

          Anatolii says GitLab has positively impacted the team’s ability to maintain stability and ship high-quality code. “If something goes wrong, GitLab offers a simple fix. For example, if we deployed the wrong ratio of our services to the server, we can see the history, read the commit message, and identify the author to roll back commits.”

          The move to GitLab also resulted in a new deployment strategy: GitOps. This framework enables the team to enhance collaboration on infrastructure changes, deliver rapidly, and reduce risk. Using GitLab, Parimatch is able to drive infrastructure automation and benefit from integrated [CI/CD pipelines](/features/continuous-integration/){data-ga-name="ci/cd" data-ga-location="body"}.

          Parimatch has increased collaboration, simplified integrations, and strengthened operational efficiency. “GitLab, for us, is one source of truth,” Anatolii shared.

