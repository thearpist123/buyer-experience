---
  data:
    twitter_text: Learn how @PotatoStudios_ adopted @GitLab as a single platform for CI and workflow efficiency.

    customer: Potato
    customer_logo: /nuxt-images/logos/potato.png
    heading: How Potato uses GitLab CI for cutting edge innovation
    key_benefits:
      - label: Improved workflow efficiency
        icon: speed-alt-2
      - label: Less tool maintenance
        icon: cog-code
      - label: Monthly cost savings
        icon: piggy-bank-alt
    header_image: /nuxt-images/blogimages/potato-london.jpg
    customer_industry: Technology
    customer_location: London and San Francisco
    customer_employee_count: 80
    customer_solution: |
      [GitLab Gold](/pricing/){data-ga-name="gold solution" data-ga-location="customers hero"}
    sticky_benefits:
      - stat: 100% 
        label: of projects in GitLab
      - stat:  6,000   
        label: deployments in 6 months
      - stat: 856   
        label: projects
    blurb: Potato was looking for a continuous integration (CI) solution that would allow developers and designers to collaborate effectively and ensure total workflow visibility   on one robust platform.  
    introduction: Potato adopted GitLab as a single platform for CI and workflow efficiency.
    quotes:
      - text: One of the major benefits of GitLab is the ability to keep the whole development workflow in a single tool.
        author: Alessandro Artoni 
        author_role: Tech Community Lead
        author_company: Potato
    content:
      - title: Cross-functional web development
        description: |
          Potato is a digital product development studio that builds complex, scalable web applications with offices in London and San Francisco. [Potato](https://p.ota.to/) works collaboratively with its clients and often invites them into the development process to improve transparency and communication. Potato provides clients with cross-functional teams that can research and validate a business or product idea before full market launch. In addition, its talented teams design, test, and build innovative digital products for leading brand names.
      
      - title: Too many tools, not enough functionality
        description: |
          Over the last few years, Potato has transformed itself from an engineer- and development-focused company to a more well-rounded product development company. Team members have transitioned to product leads, designers, delivery leads, and coaches that weren’t historically company roles. In making this update, the teams discovered that they needed a software tool that is more suitable for the entire product development lifecycle, rather than just code hosting.   
    
          Potato’s London studio was using Codebase for its code hosting needs. However, it was missing a merge request workflow. “When we were using Codebase, we had to write basically git commit hooks and various scripts to keep everything updated. We couldn't always store the code in Codebase for various client reasons. And so we had all kinds of glues and stuff to keep things working together,” said Luke Benstead, Technology Director, Potato. Sprint planning involved a lot of cumbersome manual planning such as post-it notes on actual whiteboards. They also didn’t have CI, so they were running tests locally which delayed the development process. 
      
          With Codebase, the teams didn’t have an effective method to conduct code reviews, so they used a variety of different tools for clients. Each developer had their own approach — some would follow the commits in a feature branch to review them individually and others would use the command line to create a big diff file and review that. In any instance, giving feedback was difficult and developers were frustrated.
      
          Potato was looking for a way to decrease the number of disparate tools and improve [project management workflows](/stages-devops-lifecycle/auto-devops/){data-ga-name="project management" data-ga-location="customers content"}. It also wanted a platform that could cater to other teams, like the UX and design teams, in order for developers and designers to use one centralized system for issue tracking. On top of that, they were looking for a way to incorporate CI to improve the quality and speed with which teams could build, test, and ship features. “The biggest negative of our previous environment was the lack of advanced features such as CI/CD support,” said Alessandro Artoni, Technology Community Lead, Potato. 

      - title: CI support for all teams
        description: |
          The development team researched a variety of CI platforms that could provide first-class support for Agile-oriented issue tracking and a flexible permissions system, which would allow them to include external stakeholders on a project-by-project basis. It was also very important that the tool integrate with [Google Cloud Platform](/partners/technology-partners/google-cloud-platform/){data-ga-name="google cloud platform" data-ga-location="customers content"}. GitLab checked all the boxes. “GitHub was too focused on developers and we wanted a solution that was also for the designers and the delivery team,” Benstead added.
    
          Because the design team struggled to be effective using Codebase, Potato did an internal survey with various tools and the decision was made that designers would use Asana and the developers would use GitLab. “It quickly became clear that having two separate issue tracking solutions didn't work very well, so the designers moved across to GitLab as well. And then from that point on, we've used GitLab as our default tool for everything,” Benstead said.
      
          Now, Potato uses GitLab for all the code hosting and issue tracking needs for a majority of the projects and sprint planning efforts. Some projects use GitLab heavily for CI and some projects use it for release planning.  

      - title: Integrated CI, GCP, and customer relations
        description: |
          Potato now has the [robust CI](/features/continuous-integration/){data-ga-name="continuous integration" data-ga-location="customers content"} support that it previously didn’t have. “Having everything under one roof and under one issue tracker definitely simplifies the end-to-end process of developing a product so we can track everything,” Benstead said. Projects are more consistent with the ability to merge and automate the development process. 
    
          The sprint planning process involves the entire team and is now seamless because the development, CI, and deployment pipeline are all combined on one platform. The workflow process is transparent and simplified. “I think the visibility of that and the fact that everyone can see what's happening and where everything is has definitely simplified the whole development workflow of a product. Definitely compared to all the disparate tools we were using before,” Benstead said.  
      
          Potato is now able to give clients full visibility of the application development within GitLab. This transparency has enabled Potato to build better relationships with clients and vendors because everyone can contribute to the issues and discussions are no longer dependent on emails. “It's a lot easier to build that type of relationship with a tool like GitLab,” Artoni added. 
      
          Eighty to 90% of Potato’s projects are now on GCP and teams leverage CI as part of their workflow. “We almost always use Google App Engine Standard Environment, so it's very easy to integrate deployment pipelines to that environment and link back to the environment for a client's test or for internal teams to test, as well as having to hold automatic testing running in the meantime,” Artoni said.
      
          Potato has also reduced the number of tools used across the company and mitigated the need to write and maintain integrations for different tools. “Previously we had to maintain tools to make all these different systems work together, which is no longer the case now. That's another significant source of saving money,” according to Artoni.
      
          The teams can count on higher quality merge requests thanks to integrated CI. They have improved collaboration within their own teams, as well as with clients. Overall, the workflow capabilities have surpassed what teams could previously accomplish, with over 6,000 deployments in the past six months. “We've definitely improved the quality and efficiency of the products we're building because we can leverage all the tools of GitLab, and we've definitely improved the efficiency of our teams,” Benstead added.
