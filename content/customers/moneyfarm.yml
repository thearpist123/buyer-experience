---
  title: Moneyfarm
  description: Learn how @moneyfarmUK uses GitLab CI/CD to produce better code faster.
  image_title: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
  image_alt: Moneyfarm deploys faster using fewer tools with GitLab
  twitter_image: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
  data:
    customer: Moneyfarm
    customer_logo: /nuxt-images/logos/moneyfarm_logo.png
    heading: Moneyfarm deploys faster using fewer tools with GitLab
    header_image: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
    key_benefits:
      - label: Tighter feedback loops
        icon: release
      - label: Faster pipelines
        icon: gitlab-pipeline-alt
      - label: Improved collaboration
        icon: collaboration-alt-4
    customer_industry: Finance
    customer_employee_count: 130
    customer_location: UK and Italy
    customer_solution: |
      [GitLab SaaS Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: faster CI/CD pipelines
        stat: 3x
      - label: more deployments
        stat: 2x
    blurb: Moneyfarm wanted to help its small team be as efficient as possible, but its existing continuous delivery solution was cumbersome to manage.
    introduction: |
      With GitLab, the online wealth management firm now has happier developers — and those happier developers are producing more code and deploying more value to customers.
    quotes:
      - text: |
          It's easy to underestimate the developer's feelings about the tool. Developers like using GitLab. They didn't like what we were using before. That helps us all over the different metrics we have.
        author: Nicholas Faulkner
        author_role: Director of Engineering
        author_company: Moneyfarm
    content:
      - title: A European online financial management platform
        description: |
          Moneyfarm is an online wealth management company with offices in the United Kingdom and Italy. “We are a digital wealth manager, and of course, our mission is to make sure that people can build and manage their wealth in a hassle free way and get advice from experts like us,” explained Emanuele Blanco, CTO of Moneyfarm. Blanco’s team wants to support flawless customer service and believes continuous delivery is the way to achieve it. “We want to use and strengthen our continuous delivery capability. We believe in delivering small chunks of value, in releasing incremental software.”

          But [Moneyfarm](https://www.moneyfarm.com/) doesn’t have a huge team and wants to be as efficient as possible, all the while supporting offices in two countries and making the most out of its continuous delivery philosophy. “To do all of that, we need to have a tech organization coupled with a process and a platform that allows us to do what we need to do,” Blanco said.
      - title: Too much babysitting
        description: |
          Moneyfarm had an existing continuous delivery platform, Concourse CD, and everything was running on Amazon Web Services (AWS). The Concourse solution worked, but required a tremendous amount of time and attention to keep it up and running. Almost as frustrating, “it was costing us quite a lot of money in terms of our AWS bill,” according to Nicholas Faulkner, director of engineering. Concourse was self-hosted but Faulkner says it was “very temperamental. It took people full time to manage it and we weren’t interested in investing (that much time) into it.”

          The complex nature of Moneyfarm’s CD platform also created another issue: There was no possibility of self-service. Stakeholders started to treat the platform team like they were outsourced service providers, a situation that simply wasn’t going to work over the long haul.

          And, finally, Moneyfarm just needed to a solution that worked with its “not so big team” rather than against it. “For us the advantage of (moving to) a software as a service solution is that we can focus our people on what matters the most for us,” Blanco said.
      - title: GitLab Premium makes CD nearly hands free
        description: |
          Moneyfarm was already familiar with GitLab because the team was running the self-managed version internally on its private network. In January 2020, Blanco and Faulkner and team began the process of transferring all their code from Concourse to GitLab in the cloud. The team has integrated GitLab into AWS deployment with a custom script that runs in the pipeline and releases the container into production. Their migration is complete and the team has moved between 80 and 1000 pipelines related to their most important tasks over to GitLab. The full migration to GitLab took about four months to complete.

          The move to GitLab “made things a little bit simpler because it’s one less tool to manage,” Blanco says. Moneyfarm’s value proposition is clear, he says: “We deliver value when we put software in front of our customers. Having the infrastructure and a tool that (operates) seamlessly means developers can just focus on building features and making code that works. We have a tool that supports that in production (now) and it made a difference.”

          With GitLab, Moneyfarm has:
            * Shrunk the cycle time between idea and production from 45 minutes down to 13.5 minutes

            * Improved the working relationships between developers and stakeholders

            * Enabled greatly enhanced developer self-service

            * Achieved predictable timing in the deployment process

            * Doubled the number of deployments from 18 deploys per week to 35 per week

            * Increased code production

          There is also, quite simply, less time spent waiting. “With GitLab we go from a developer’s keyboard to a customer environment much faster,” Faulkner said. “It used to be the case that developers were sitting watching a progress bar on Concourse with a stakeholder standing behind them. I don’t remember that happening since we’ve moved to GitLab.”

          But there are also other, less concrete outcomes. Improved collaboration has led to brainstorming conversations the likes of which the Moneyfarm team had never seen before. “Conversations that wouldn’t have happened before are now happening and this, in turn, spreads knowledge, and that in turns helps us to have a better understanding of the tool and how to use it better,” according to Blanco.

          And finally, the Moneyfarm team was pleasantly surprised to find the cost of GitLab is roughly the same as what was spent on self-hosting and local management of the previous tool. The GitLab bonus, though, is that it doesn’t require a dedicated staff to manage and maintain it.
      - title: Happy developers = better code and faster deployments
        description: |
          Although Moneyfarm has seen a number of concrete benefits from the shift to GitLab, one in particular was relatively surprising. “Our developer’s happiness shot to the sky when we migrated to GitLab. Everybody was satisfied that we have a new solution because everybody felt it was a breath of fresh air and quite easy to understand,” Blanco said. “This definitely made our developers happier.”

          Developer happiness matters because happy developers simply do better work, according to Blanco. “You need to keep your developer experience at a high level because that’s the only way you really can deliver value fast. GitLab has played … a significant part in helping us increase our developer experience.”

          One obvious way the developer experience has improved is that things are moving more quickly. Their previous CI/CD solution took between 35 to 45 minutes to get from commit to staging, Faulkner said, but [GitLab comes in reliably](/features/continuous-integration/){data-ga-name="continuous integration" data-ga-location="customers content"} at just 13.5 minutes. The process is faster and more reliable and that translates to less context-switching and an increased ability to focus on a single task.

          “Before, developers would effectively have to pick up another task while waiting,” Faulkner said. “Now people can be much more focused on keeping on the same task and getting it through production.”

          That focus translates into deployments that are twice as frequent as before and increased code production. “I definitely can tell you we spend less time worrying about the CD tool and worrying about the idiosyncrasies. Sometimes the CD tool was down or somebody had to restart it – now we don’t think about that anymore,” Blanco said. “I can see a kind of correlation between the fact that we’re producing more code, deploying more value, and the fact that we are using GitLab.”


