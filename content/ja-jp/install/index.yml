---
  title: GitLabをダウンロードしてインストールします
  description: Linux、Kubernetes、Docker、Google Cloudなどのさまざまなインストールパッケージとダウンロードを使用して、独自のGitLabインスタンスをダウンロード、インストール、および保守します。
  side_menu:
    anchors:
      text: "このページの目次"
      data:
      - text: 公式リナックスパッケージ
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Kubernetes のデプロイ
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: サポートされているクラウド
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: その他の公式方法
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: コミュニティ貢献
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: すでにインストールされていますか?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "このトピックの詳細"
      data:
        - text: "無料の究極の試用版を入手"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "自己管理型のインストール"
          href: "ja-jp/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "SaaSの使用を開始する"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "マーケットプレイスからの購入"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: 自己管理型の GitLab をインストールする
    subtitle: 今すぐGitLabをお試しください。 独自のGitLabインスタンスをダウンロード、インストール、および保守します。
    text: |
      [トライアル](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} - 今すぐ無料アルティメットトライアルを始めましょう

      [自己管理](ja-jp/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} – 独自のインフラストラクチャにインストールする

      [SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"}  – SaaS 製品を使い始める

      [マーケットプレイス](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"}  – 選択したクラウドマーケットプレイスを使用してシームレスに購入
  linux:
    title: 公式リナックスパッケージ
    subtitle: 推奨されるインストール方法
    text: |
      これは、開始するための推奨される方法です。 Linuxパッケージは成熟しており、スケーラブルであり、現在 GitLab.com で使用されています。 さらなる柔軟性と回復性が必要な場合は、[リファレンスアーキテクチャのドキュメント]の説明に従ってGitLabをデプロイすることをお勧めします。(https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"}

      Linuxのインストールは、インストールが迅速で、アップグレードが簡単で、他の方法にはない信頼性を高める機能が含まれています。 GitLabの実行に必要なさまざまなサービスとツールをすべてバンドルした単一のパッケージ(オムニバスとも呼ばれます)を介してインストールします。 少なくとも 4 GB の RAM を推奨します ([最小要件](https://docs.gitlab.com/ee/install/requirements.html)(https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).
    cards:
      - title: ウブンツ
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu
          variant: marketing
          alt: Ubuntu Icon
          hex_color: '#E95420'
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#ubuntu
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: デビアン
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#debian
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: アルマリナックス8
        id: almalinux-8
        subtext: and RHEL, Oracle, Scientific
        icon:
          name: almalinux
          variant: marketing
          alt: Almalinux Icon
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#almalinux-8
        data_ga_name: almalinux 8 installation documentation
        data_ga_location: linux installation
      - title: セントOS 7
        id: centos-7
        subtext: and RHEL, Oracle, Scientific
        icon:
          name: centos
          variant: marketing
          alt: CentOs Icon
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#centos-7
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4 and SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          variant: marketing
          alt: OpenSuse Icon
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#opensuse-leap
        data_ga_name: opensuse leap installation documentation
        data_ga_location: linux installation
      - title: アマゾンリナックス2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#amazonlinux-2
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation
      - title: アマゾンリナックス2022
        id: amazonlinux-2022
        subtext:
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#amazonlinux-2022
        data_ga_name: amazonlinux 2022 installation documentation
        data_ga_location: linux installation

      - title: ラズベリーパイOS
        id: raspberry-pi-os
        subtext: Bullseye and Buster (32 bit)
        icon:
          name: raspberry-pi
          variant: marketing
          alt: Raspberry Pi Icon
          hex_color: '#000'
        link_text: インストール手順の表示 +
        link_url: /ja-jp/install/#raspberry-pi-os
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          Ubuntu 20.04および22.04では、「arm64」パッケージも利用可能であり、インストールにGitLabリポジトリを使用すると、そのプラットフォームで自動的に使用されます
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          Debian 10 では、'arm64' パッケージも利用可能であり、インストールに GitLab リポジトリを使用するときに、そのプラットフォームで自動的に使用されます
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux-8
        tip: |
          AlmaLinux および RedHat 8 では、'arm64' パッケージも利用可能であり、インストールに GitLab リポジトリを使用するときに、そのプラットフォームで自動的に使用されます。
        dependency_text: AlmaLinux 8 (および RedHat 8) では、以下のコマンドを実行すると、システムファイアウォールで HTTP、HTTPS、SSH アクセスも開かれます。 これはオプションの手順であり、ローカルネットワークからのみGitLabにアクセスする場合はスキップできます。
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # 有効になっていない場合はOpenSSHサーバデーモンを有効にする: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          #ファイアウォールを開く必要があるかどうかを確認します: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: アルマリナックス-8
      - id: centos-7
        dependency_text: CentOS 7(およびRedHat/Oracle/Scientific Linux 7)では、以下のコマンドでシステムファイアウォールでHTTP、HTTPS、SSHアクセスも開きます。 これはオプションの手順であり、ローカルネットワークからのみGitLabにアクセスする場合はスキップできます。
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
          # 有効になっていない場合はOpenSSHサーバデーモンを有効にする: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          #ファイアウォールを開く必要があるかどうかを確認します: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: セントス-7
      - id: opensuse-leap
        tip: |
          OpenSuse では、'arm64' パッケージも利用可能であり、インストールに GitLab リポジトリを使用するときに、そのプラットフォームで自動的に使用されます。
        dependency_text: OpenSUSEでは、以下のコマンドを実行すると、システムファイアウォールでHTTP、HTTPS、およびSSHアクセスも開きます。 これはオプションの手順であり、ローカルネットワークからのみGitLabにアクセスする場合はスキップできます。
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # 有効になっていない場合はOpenSSHサーバデーモンを有効にする: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          #ファイアウォールを開く必要があるかどうかを確認します: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2022
        tip: |
          Amazon Linux 2022の場合、「arm64」パッケージも利用可能であり、インストールにGitLabリポジトリを使用するときに、そのプラットフォームで自動的に使用されます。
        dependency_text: Amazon Linux 2022では、以下のコマンドを実行すると、システムファイアウォールでHTTP、HTTPS、およびSSHアクセスも開きます。 これはオプションの手順であり、ローカルネットワークからのみGitLabにアクセスする場合はスキップできます。
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # 有効になっていない場合はOpenSSHサーバデーモンを有効にする: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          #ファイアウォールを開く必要があるかどうかを確認します: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: アマゾンリナックス-2022

      - id: amazonlinux-2
        tip: |
          Amazon Linux 2 では、「arm64」パッケージも利用可能であり、インストールに GitLab リポジトリを使用するときに、そのプラットフォームで自動的に使用されます。
        dependency_text: Amazon Linux 2 では、以下のコマンドを実行すると、システムファイアウォールで HTTP、HTTPS、SSH アクセスも開かれます。 これはオプションの手順であり、ローカルネットワークからのみGitLabにアクセスする場合はスキップできます。
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # 有効になっていない場合はOpenSSHサーバデーモンを有効にする: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          #ファイアウォールを開く必要があるかどうかを確認します: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: アマゾンリナックス-2
      - id: raspberry-pi-os
        tip: |
          少なくとも4GBのラズベリーパイ4をお勧めします。 現時点では、32ビット(armhf)のみがサポートされています。 64ビット('arm64')が進行中です。
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Kubernetes のデプロイ
    text: |
      GitLabをKubernetesにインストールする場合、注意が必要なトレードオフがいくつかあります:

        - 管理とトラブルシューティングには Kubernetes の知識が必要
        - 小規模なインストールでは、より高価になる可能性があります。 既定のインストールでは、ほとんどのサービスが冗長な方法でデプロイされるため、単一ノードの Linux パッケージ展開よりも多くのリソースが必要です。
        - いくつかの機能 [注意すべき制限 があります.] (https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"}

      この方法は、インフラストラクチャが Kubernetes 上に構築されており、そのしくみに精通している場合に使用します。 管理、監視、および一部の概念の方法は、従来の展開とは異なります。 ヘルムチャート方式はVanilla Kubernetes デプロイメント用であり、GitLab オペレーターを使用して OpenShift クラスターに GitLab をデプロイできます。 GitLab Operator は、OpenShift とバニラの Kubernetes デプロイメントの両方で Day 2 の操作を自動化するために使用できます。
    cards:
      - title: ヘルムチャート
        subtext: ヘルムチャートを使用してGitLabをインストールする
        icon:
          name: kubernetes
          variant: marketing
          alt: Kubernetes Icon
          hex_color: '#336CE4'
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: GitLab オペレータ
        new_flag: true
        subtext: 演算子を使用してGitLabをインストールする
        icon:
          name: gitlab-operator
          variant: marketing
          alt: GitLab Operator Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: サポートされているクラウド
    text: |
      公式のLinuxパッケージを使用して、さまざまなクラウドプロバイダーにGitLabをインストールします
    cards:
      - title: アマゾン ウェブ サービス (AWS)
        subtext: GitLab を AWS にインストールする
        icon:
          name: aws
          variant: marketing
          alt: AWS Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud Platform (GCP)
        subtext: GitLab を GCP にインストールする
        icon:
          name: gcp
          variant: marketing
          alt: GCP Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: マイクロソフト アズ
        subtext: Azure に GitLab をインストールする
        icon:
          name: azure
          variant: marketing
          alt: Azure Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: サポートされているその他の公式のインストール方法
    cards:
      - title: 港湾労働者
        subtext: 公式GitLab Docker Images
        icon:
          name: docker
          variant: marketing
          alt: Docker Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: リファレンス・アーキテクチャ
        subtext: 推奨される GitLab デプロイ トポロジ
        icon:
          name: gitlab
          variant: marketing
          alt: GitLab Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: ソースからのインストール
        subtext: Debian/Ubuntu システムにソースファイルを使用して GitLab をインストールする
        icon:
          name: source
          variant: marketing
          alt: Source Icon
        link_text: インストール手順を表示する
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: GitLab Environment Toolkit (GET)
        subtext: Terraform と Ansible を使用した GitLab リファレンスアーキテクチャのデプロイの自動化
        icon:
          name: gitlab-environment-toolkit
          variant: marketing
          alt: GitLab Environment Toolkit Icon
        link_text: インストール手順を表示する
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: U非公式、サポートされていないインストール方法
    cards:
      - title: Debian ネイティブパッケージ
        subtext: by 海賊プラビーン
        icon:
          name: debian
          variant: marketing
          alt: Debian Icon
          hex_color: '#000'
        link_text: インストール手順を表示する
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: FreeBSD パッケージ
        subtext: トルステン・チュールスドルフ
        icon:
          name: freebsd
          variant: marketing
          alt: FreeBSD Icon
        link_text: インストール手順を表示する
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd installation
        data_ga_location: unofficial installation
      - title: アーチ Linux パッケージ
        subtext: by アーチ Linux コミュニティ
        icon:
          name: arch-linux
          variant: marketing
          alt: Arch Linux Icon
        link_text: インストール手順を表示する
        link_url: https://www.archlinux.org/packages/community/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: パペットモジュール
        subtext: に ヴォックスププリ
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          variant: marketing
          alt: Puppet Icon
        link_text: インストール手順を表示する
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: アンシブルプレイブック
        subtext: ジェフ・ギアリング
        icon:
          name: ansible
          variant: marketing
          alt: Ansible Icon
        link_text: インストール手順を表示する
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: GitLab Virtual appliance (KVM)
        subtext: に オープン星雲
        icon:
          name: open-nebula
          variant: marketing
          alt: Open Nebula Icon
        link_text: インストール手順を表示する
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab on Cloudron
        subtext: Cloudronアプリライブラリ経由
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          variant: marketing
          alt: Cloudron Icon
        link_text: インストール手順を表示する
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: GitLabをすでにインストールしていますか?
    cards:
      - title: 古いバージョンの GitLab からのアップデート
        text: GitLabのインストールを更新して、最新の機能を利用してください。 新機能を含むGitLabのバージョンは、毎月22日にリリースされます
        link_url: https://about.gitlab.com/update/
        link_text: GitLabの最新リリースにアップデートする
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: GitLabコミュニティエディションからのアップデート
        text: GitLab Enterprise Edition には、Community Edition では利用できない高度な機能が含まれています。
        link_url: https://about.gitlab.com/upgrade/
        link_text: エンタープライズ版へのアップグレード
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: 手動でインストールされたオムニバスパッケージからのアップグレード
        text: GitLab 7.10では、簡単なコマンドでGitLabをインストールできるGitLabのパッケージリポジトリを導入しました。
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: オムニバスパッケージリポジトリへのアップグレード
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: GitLabをサポートするサードパーティアプリケーション
        text: GitLabはコラボレーションにオープンであり、DevOpsエコシステムでテクノロジーパートナーシップを構築することに取り組んでいます。 GitLabテクノロジーパートナーになることの利点と要件の詳細をご覧ください。
        link_url: https://about.gitlab.com/partners/
        link_text: サードパーティ製アプリケーションを表示する
        data_ga_name: View third-party applications
        data_ga_location: update
