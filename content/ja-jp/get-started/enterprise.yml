---
title: 'エンタープライズを始めましょう'
description: 'このガイドは、Premiumランクでの自動ソフトウェア開発とデリバリーにおける必須項目をすばやくセットアップするのに役立ちます'
side_menu:
  anchors:
    text: 'このページの内容'
    data:
      - text: 'はじめに'
        href: '#getting-started'
        data_ga_name: getting-started
        data_ga_location: side-navigation
      - text: 'セットアップを行う'
        href: '#getting-setup'
        data_ga_name: getting-setup
        data_ga_location: side-navigation
      - text: 'GitLabを使用する'
        href: '#using-gitlab'
        data_ga_name: using-gitlab
        data_ga_location: side-navigation
  hyperlinks:
    text: ''
    data: []
hero:
  crumbs:
    - title: '今すぐ始める'
      href: /get-started/
      data_ga_name: Get Started
      data_ga_location: breadcrumb
    - title: 
  header_label: '所要時間は20分'
  title: 'エンタープライズを始めましょう'
  content: |
   競争力を維持するには、DevSecOpsを簡素化して拡張し、チームがセキュアなコードをより速く公開できるようにする方法が必要です。このガイドは、Ultimateランクのセキュリティ、コンプライアンス、プロジェクトプランニングを含むオプションを使用して、Premiumランクでの自動ソフトウェア開発とデリバリーにおける必需品をすばやくセットアップするのに役立ちます。
steps:
  text:
    show: すべて表示
    hide: すべて非表示
  groups:
    - header: 'はじめに'
      show: すべて表示
      hide: すべて非表示
      id: 'getting-started'
      items:
        - title: '自分に合ったサブスクリプションを決める'
          copies:
            - title: 'GitLab SaaSまたはGitLab自己管理'
              text: |
                <p>GitLabがGitLabプラットフォームを管理するようにしますか、それとも自分で管理しますか?</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                text: 'ちがいを見る'
                ga_name: GitLab Saas vs Self-Managed
                ga_location: body
        - title: '自分のニーズを満たすランクを決める'
          copies:
            - title: 'PremiumまたはUltimate'
              text: |
                <p>自分に合ったランクを決めるには、次の点を考慮してください。</p>
            - title: 必要なストレージ容量
              text: |
                <p>GitLab SaaSのPremiumランクの名前空間には50 GBのストレージ制限があり、Ultimateランクは最大250 GBになります。</p>
            - title: 希望するセキュリティとコンプライアンス
              text: |
                <p>*Premiumでは、シークレット検出、SAST、およびコンテナスキャンを使用できます。</p>
                <p>* Ultimateで は、DAST、依存関係、クラスタイメージ、IaC、API、ファズ <a href="https://docs.gitlab.com/ee/user/application_security/">などの追加スキャナ</a>を使用できます。</p>
                <p>*マージリクエストパイプラインとセキュリティダッシュボードに統合された実用的な調査結果には、脆弱性を管理するためにUltimateが必要です。</p>
                <p>*コンプライアンスパイプラインにはUltimateが必要です。</p>
                <p>*当社の <a href="https://docs.gitlab.com/ee/user/application_security/">セキュリティスキャナー</a>とコンプライアンス機能 <a href="https://docs.gitlab.com/ee/administration/compliance.html">についてお読みください</a>。
              link:
                href: /pricing/
                text: '詳しくは料金設定をご覧ください'
                ga_name: pricing
                ga_location: body
    - header: 'セットアップを行う'
      show: すべて表示
      hide: すべて非表示
      id: 'getting-setup'
      items:
        - title: 'SaaSサブスクリプションアカウントをセットアップする'
          copies:
            - title: '必要なシート数を決める'
              text: |
                GitLab SaaSサブスクリプションは、同時(シート)モデルを使用します。課金期間中にトップレベルグループまたはその子に割り当てる最大ユーザー数に応じて、サブスクリプションの支払いを行います。サブスクリプション期間中にユーザーを追加および削除することができます。ただし、合計ユーザー数がサブスクリプション数を超えていない場合に限ります。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                text: 詳細はこちら
                ga_name: Determine how many seats you want
                ga_location: body
            - title: 'SaaSサブスクリプションを入手'
              text: |
                <p>GitLab SaaSは、GitLab.comで入手可能なGitLabのサービスとしてのソフトウェアです。GitLab SaaSを使用するために何かをインストールする必要はなく、サインアップするだけで済みます。プライベートプロジェクトで利用する機能に応じて、サブスクリプションを決定します。パブリックオープンソースプロジェクトを持つ組織は、GitLab for Open Sourceに積極的に申し込むことができます。<p/> 

                <p>50,000個の分を含む<a href="/pricing/ultimate/">GitLab Ultimate</a>の機能は、<a href="/solutions/open-source/">GitLab for Open Source</a>プログラムを通じて資格のあるオープンソースプロジェクトには無料で提供されます。</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#view-your-gitlabcom-subscription
                text: 詳細はこちら
                ga_name: Obtain your SaaS subscription
                ga_location: body
            - title: 'Determine CI/CD共有ランナーに必要な時間を決定する'
              text: |
                <p><a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">共有ランナー</a>は、GitLabインスタンス内のすべてのプロジェクトとグループと共有されます。ジョブが共有ランナー上で実行される場合、分が使用されます。GitLab.comでは、各<a href="https://docs.gitlab.com/ee/user/group/index.html#namespaces" data-ga-name="namespaces" data-ga-location="body">名前空間</a>に分のクォータが設定され、ライセンスランク<a href="/pricing/" data-ga-name="Your license tier" data-ga-location="body">によって決定され ます。</a><p/>

                <p>毎月のクォータに加えて、GitLab.comでは、必要に応じて<a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes" data-ga-name="purchase additional compute minutes" data-ga-location="body">追加の分を 購入</a>できます。</p>
        - title: '自己管理サブスクリプションアカウントをセットアップする'
          copies:
            - title: '必要なシート数を決める'
              text: |
                GitLab SaaSサブスクリプションは、同時(シート)モデルを使用します。課金期間中にトップレベルグループまたはその子に割り当てる最大ユーザー数に応じて、サブスクリプションの支払いを行います。サブスクリプション期間中にユーザーを追加および削除することができます。ただし、合計ユーザー数がサブスクリプション数を超えていない場合に限ります。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                text: '詳細はこちら'
                ga_name: Determine how many seats you want
                ga_location: body
            - title: '自己管理サブスクリプションを入手する'
              text: |
                独自のGitLabインスタンスをインストール、管理、および維持できます。GitLabサブスクリプションの管理には、カスタマーポータルへのアクセスが必要です。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                text: 詳細はこちら
                ga_name: Determine how many seats you want
                ga_location: body
            - title: GitLabエンタープライズエディションをアクティベーション
              text: |
                ライセンスなしで新しいGitLabインスタンスをインストールすると、無料の機能のみが有効になります。GitLabエンタープライズエディション(EE)でより多くの機能を有効にするには、アクティベーションコードでインスタンスをアクティベートします。
              link:
                href: https://docs.gitlab.com/ee/user/admin_area/license.html
                text: 詳細はこちら
                ga_name: Activate GitLab Enterprise Edition
                ga_location: body
            - title: システム要件を確認
              text: |
                サポートされているオペレーティングシステムと、GitLabのインストールと使用に必要な最小要件の両方を確認します。
              link:
                href: https://docs.gitlab.com/ee/install/requirements.html
                text: 詳細はこちら
                ga_name: Review the system requirements
                ga_location: body
            - title: GitLabのインストール
              text: |
                <p><a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">インストール方法</a>を選択してください</p>
                <p><a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">クラウドプロバイダー</a>(該当する場合)にインストールします</p>
            - title: 'インスタンスを構成'
              link:
                href: https://docs.gitlab.com/ee/install/next_steps.html
                text: 詳細はこちら
                ga_name: Configure your instance
                ga_location: body
            - title: オフライン環境をセットアップ
              text: |
                パブリックインターネットからの隔離が必要な場合にオフライン環境を設定する(通常は規制された業界に適用されます)
              link:
                href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                text: 詳細はこちら
                ga_name: Set up off-line environment
                ga_location: body
            - title: CI/CD共有ランナーに許可されている制限時間を考慮する
              text: |
                GitLab自己管理インスタンスのリソース使用量を制御するには、管理者が各名前空間の分のクォータを設定できます。
              link:
                href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                text: 詳細はこちら
                ga_name: Consider limiting CI/CD shared runner minutes allowed
                ga_location: body
            - title: GitLabランナーのインストール
              text: |
                GitLabランナーは、GNU/Linux、macOS、FreeBSD、Windowsにインストールして使用できます。
              link:
                href: https://docs.gitlab.com/runner/install/
                text: 詳細はこちら
                ga_name: Install GitLab runner
                ga_location: body
            - title: GitLabランナーの構成(オプション)
              text: |
                GitLabランナーは、GNU/Linux、macOS、FreeBSD、Windowsにインストールして使用できます。
              link:
                href: https://docs.gitlab.com/runner/configuration/
                text: 詳細はこちら
                ga_name: Configure GitLab runner
                ga_location: body
            - title: 管理
              text: |
                自己管理にはGitLabが必要です
              link:
                href: https://docs.gitlab.com/ee/administration/
                text: 詳細はこちら
                ga_name: Self Administration
                ga_location: body
        - title: アプリケーションの統合(オプション)
          copies:
            - text: |
                シークレット管理や認証サービスなどの機能を追加したり、課題トラッカーなどの既存のアプリケーションを統合したりできます。
              link:
                href: https://docs.gitlab.com/ee/integration/
                text: 詳細はこちら
                ga_name: Integrate applications
                ga_location: body
    - header: GitLabを使用する
      show: すべて表示
      hide: すべて非表示
      id: 'using-gitlab'
      items:
        - title: 組織をセットアップ
          copies:
            - text: |
                組織とそのユーザーを構成します。ユーザーの役割を決定し、全員に必要なプロジェクトへのアクセスを付与します。
              link:
                href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                text: 詳細はこちら
                ga_name: Setup your organization
                ga_location: body
        - title: プロジェクトでの作業を管理
          copies:
            - text: |
                GitLabでは、コードベースをホストするプロジェクトを作成できます。また、プロジェクトを使用して問題を追跡し、作業を計画し、コードで連携し、内蔵のCI/CDを継続的にビルド、テスト、および使用してアプリをデプロイすることもできます。
              link:
                href: https://docs.gitlab.com/ee/user/project/index.html
                text: 詳細はこちら
                ga_name: Organize work with projects
                ga_location: body
        - title: 作業を計画して追跡する
          copies:
            - text: |
                要件、イシュー、エピックを作成して作業を計画します。マイルストーンで作業をスケジュールし、チームの時間を追跡します。クイックアクションで時間を節約する方法、GitLabがMarkdownテキストをどのようにレンダリングするか、Gitを使用してGitLabとやりとりする方法について説明します。
              link:
                href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                text: 詳細はこちら
                ga_name: Plan and track work
                ga_location: body
        - title: アプリケーションを構築する
          copies:
            - text: |
                リポジトリにソースコードを追加し、コードをチェックインするマージリクエストを作成し、CI/CDを使用してアプリケーションを生成します。
              link:
                href: https://docs.gitlab.com/ee/topics/build_your_application.html
                text: 詳細はこちら
                ga_name: Build your application
                ga_location: body
        - title: アプリケーションを安全に保護する
          copies:
            - title: 使用するスキャナを決定します(すべてデフォルトでオンになっています)
              text: |
                GitLabは、コンテナスキャンと依存関係スキャンの両方を提供して、これらの依存関係のすべてのタイプを確保します。できるだけ多くのリスク領域をカバーするために、すべてのセキュリティスキャナーを使用することをお勧めします。              link:
              link:
                href: https://docs.gitlab.com/ee/user/application_security/configuration/
                text: 詳細はこちら
                ga_name: Determine which scanners you’d like to use
                ga_location: body
            - title: セキュリティポリシーの構成
              text: |
                GitLabのポリシーは、プロジェクトパイプラインが指定された構成に従って実行されるときはいつでも、セキュリティチームに選択したスキャンを実行するように要求する方法を提供します。したがって、セキュリティチームは、設定したスキャンが変更、アラートの精製、または無効化されていないことを確信できます。
              link:
                href: https://docs.gitlab.com/ee/user/application_security/policies/
                text: 詳細はこちら
                ga_name: Configure your security policies
                ga_location: body
            - title: マージリクエストの承認ルールとセキュリティ認証を構成する
              text: |
                マージリクエストの承認ルールを使用すると、作業をプロジェクトにマージする前に必要な承認の最小数を設定できます。これらのルールを拡張して、どのタイプのユーザーが作業を承認できるかを定義することもできます。
              link:
                href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                text: 詳細はこちら
                ga_name: Configure MR approval rules and security approvals
                ga_location: body
        - title: アプリケーションをデプロイしてリリースする
          copies:
            - text: |
                アプリケーションを内部またはパブリックにデプロイします。フラグを使用して機能を段階的にリリースします。
              link:
                href: https://docs.gitlab.com/ee/topics/release_your_application.html
                text: 詳細はこちら
                ga_name: Deploy and release your application
                ga_location: body
        - title: アプリケーションのパフォーマンスをモニターする
          copies:
            - text: |
                GitLabは、アプリケーションの運用と保守を支援するさまざまなツールを提供しています。GitLab内では、チームにとって最も重要なメトリクスを追跡したり、パフォーマンスが低下したときに自動アラートを生成したり、これらのアラートを管理したりできます。
              link:
                href: https://docs.gitlab.com/ee/operations/index.html
                text: 詳細はこちら
                ga_name: Monitor application performance
                ga_location: body
        - title: ランナーのパフォーマンスをモニターする
          copies:
            - text: |
                GitLabには、「GitLab Performance Monitoring」と呼ばれる独自のアプリケーションパフォーマンス測定システムが付属しています。GitLab Performance Monitoringにより、さまざまな統計を測定できます
              link:
                href: https://docs.gitlab.com/runner/monitoring/index.html
                text: 詳細はこちら
                ga_name: Monitor runner performance
                ga_location: body
        - title: 'インフラストラクチャを管理する'
          copies:
            - text: |
                DevSecOpsとSREのアプローチの台頭により、インフラストラクチャ管理が体系化され、自動化されて、ソフトウェア開発のベストプラクティスもインフラストラクチャ管理まわりに位置づけられます。GitLabでは、インフラストラクチャ管理の実践をスピードアップし、簡素化するさまざまな機能を提供しています。
              link:
                href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                text: 詳細はこちら
                ga_name: Manage your infrastructure
                ga_location: body
        - title: GitLabの使用量を分析する
          copies:
            - text: |
                これは、どれくらいの頻度でエンドユーザーに価値を提供しているかを測定します。デプロイ頻度が多ければ多いほど、フィードバックをより早く取得し、迅速にイテレーションを行い、改善と機能を提供することができるます。
              link:
                href: https://docs.gitlab.com/ee/user/analytics/index.html
                text: 詳細はこちら
                ga_name: Analyze GitLab usage
                ga_location: body
next_steps:
  header: エンタープライズを次のステップに進めましょう
  cards:
    - title: 有料ランクをお持ちですか?
      text: テクニカルアカウントマネージャー(TAM)によるサポートを受けれます。
      avatar: /nuxt-images/icons/avatar_orange.png
      col_size: 4
      link:
        text: TAMからの連絡を希望する
        url: /sales/
        data_ga_name: Have my TAM contact me
        data_ga_location: body
    - title: 'お困りですか?'
      text: 'GitLabプロフェッショナルサービスは、使用の開始やサードパーティのアプリケーションとの統合などに役立ちます。'
      avatar: /nuxt-images/icons/avatar_pink.png
      col_size: 4
      link:
        text: 'PSからの連絡を希望する'
        url: /sales/
        data_ga_name: Have my PS contact me
        data_ga_location: body
    - title: 'チャネルパートナーとの連携をご希望ですか？'
      avatar: /nuxt-images/icons/avatar_blue.png
      col_size: 4
      link:
        text: チャンネルパートナーのディレクトリを表示
        url: /sales/
        data_ga_name: See channel partner directory
        data_ga_location: body
    - title: 'アップグレードを検討中ですか？'
      text: |
        [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"}と[Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"}の利点については、こちらをご覧ください。
      col_size: 6
      link:
        text: '各ランクの詳細を見る'
        url: /sales/
        data_ga_name: See tiering details
        data_ga_location: body
    - title: 'サードパーティの統合を検討していますか？'
      text: 'GitLabプロフェッショナルサービスは、使用の開始やサードパーティのアプリケーションとの統合などに役立ちます。'
      col_size: 6
      link:
        text: 'アライアンスとテクノロジーパートナーを確認する'
        url: /partners/
        data_ga_name: See our Alliance and Technology partners
        data_ga_location: body
