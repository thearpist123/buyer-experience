---
  title: スタートアップのためのGitLab
  description: スタートアップを加速する単一のアプリケーション。 私たちは、資格基準を満たすスタートアップに無料でトップティアを提供しています。 詳細情報！
  image_alt: スタートアップのためのGitLab
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  スタートアップのためのGitLab
        title: 成長を加速
        subtitle: DevSecOpsプラットフォームでセキュリティを強化しながら、ソフトウェアをより迅速にリリースする
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: 無料トライアルを開始
          data_ga_name: Start your free trial
          data_ga_location: header
        secondary_btn:
          url: /solutions/startups/join/
          text: スタートアップのためのGitLabプログラムに参加する
          data_ga_name: startup join
          data_ga_location: header
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/startups_hero.jpeg
          alt: "画像:オープンソースのためのgitlabは"
          bordered: true
    - name: customer-logos
      data:
        text_variant: body2
        companies:
          - image_url: "/nuxt-images/logos/chorus-color.svg"
            link_label: コーラスのお客様事例へのリンク
            alt: "Chorus logo"
            url: /customers/chorus/
          - image_url: "/nuxt-images/logos/zoopla-logo.png"
            link_label: ズープラのお客様事例へのリンク
            alt: "zoopla logo"
            url: /customers/zoopla/
          - image_url: "/nuxt-images/logos/zebra.svg"
            link_label: ゼブラのお客様事例へのリン
            alt: "the zebra logo"
            url: /customers/thezebra/
          - image_url: "/nuxt-images/logos/hackerone-logo.png"
            link_label: ハッカーワンのお客様のケーススタディへのリンク
            alt: "hackerone logo"
            url: /customers/hackerone/
          - image_url: "/nuxt-images/logos/weave_logo.svg"
            link_label: Weaveのお客様のケーススタディへのリンク
            alt: "weave logo"
            url: /customers/weave/
          - image_url: /nuxt-images/logos/inventx.png
            alt: "inventx logo"
            url: /customers/inventx/

    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: 利点
          href: '#benefits'
        - title: 顧客企業
          href: '#customers'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:

            - name: 'solutions-video-feature'
              data:
                header: 速度。 効率。 コントロール。
                description: スタートアップにとって、ソフトウェアをより迅速かつ効率的に展開することは、あれば便利ではありません。 これは、顧客ベースを拡大し、収益目標を達成し、市場で製品を差別化するかどうかの違いです。 GitLabは、成長を加速するために構築されたDevSecOpsプラットフォームです。
                video:
                  url: 'https://player.vimeo.com/video/784035583?h=d978ef89bf&color=7759C2&title=0&byline=0&portrait=0'

        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: スタートアップがGitLabを選ぶ理由
                light_background: false
                large_card_on_bottom: true
                cards:
                  - title: ライフサイクル全体に対応する1つのプラットフォーム
                    description: ソフトウェア配信ライフサイクル全体に対応する単一のアプリケーションにより、チームは多数のツールチェーン統合を維持することなく、優れたソフトウェアの出荷に集中できます。 コミュニケーション、エンドツーエンドの可視性、セキュリティ、サイクルタイム、オンボーディングなどが向上します。
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    cta: もっと詳しく知る
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: あらゆる環境やクラウドにデプロイ
                    description: GitLabはクラウドニュートラルであるため、好きな方法で好きな場所で自由に使用でき、成長に合わせてクラウドプロバイダーを変更および追加する柔軟性を提供します。 AWS、Google Cloud、Azureなどにシームレスにデプロイできます。
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: ビルトインセキュリティ
                    description: GitLabを使用すると、ソフトウェア配信ライフサイクルのすべての段階にセキュリティとコンプライアンスが組み込まれるため、チームは脆弱性を早期に特定し、開発をより迅速かつ効率的に行うことができます。 スタートアップが成長するにつれて、スピードを犠牲にすることなくリスクを管理できるようになります。
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    cta: もっと詳しく知る
                    icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                  - title: ソフトウェアの作成と展開を迅速化
                    description: GitLabを使用すると、サイクルタイムを短縮し、より少ない労力でより頻繁にデプロイできます。 新機能の計画から自動テスト、リリースオーケストレーションまで、すべてを 1 つのアプリケーションで実行できます。
                    href: /platform/
                    data_ga_name: platform
                    cta: もっと詳しく知る
                    icon:
                      name: speed-gauge
                      alt: speed-gauge Icon
                      variant: marketing
                  - title: より強力なコラボレーション
                    description: サイロを解消し、開発、セキュリティ、運用チームからビジネス チームや技術者以外の関係者まで、社内の全員がコードを中心にコラボレーションできるようにします。 DevSecOpsプラットフォームを使用すると、ライフサイクル全体にわたる可視性を高め、ソフトウェアプロジェクトを前進させるために協力するすべての人のコミュニケーションを向上させることができます。
                    icon:
                      name: collaboration-alt-4
                      alt: Collaboration Icon
                      variant: marketing
                  - title: オープンソースプラットフォーム
                    description: GitLabのオープンコアは、オープンソースソフトウェアの改善と改良に継続的に取り組んでいる世界中の何千人もの開発者のイノベーションを活用する機会など、オープンソースソフトウェアのすべての利点を提供します。
                    icon:
                      name: open-source
                      alt: open-source Icon
                      variant: marketing
                  - title: GitLab はあなたに合わせて拡張できます
                    description: シード資金調達からキャッシュフロープラス、大規模な多国籍企業(またはあなたが向かうところ)まで、GitLabはあなたと一緒に成長します。
                    href: /customers/
                    data_ga_name: customers
                    cta: GitLab の顧客の一部をご覧ください
                    icon:
                      name: auto-scale
                      alt: auto-scale Icon
                      variant: marketing
        - name: 'div'
          id: customers
          slot_enabled: true
          slot_content:
            - name: 'education-case-study-carousel'
              data:
                header: あなたのようなスタートアップをどのように支援したか
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: ストーリーをもっと見る
                  data_ga_name: customers
                case_studies:
                  - logo_url: "/nuxt-images/logos/chorus-color.svg"
                    institution_name: Chorus
                    quote:
                      img_url: /nuxt-images/blogimages/Chorus_case_study.png
                      quote_text: 製品エンジニアリングや製品エンジニアリングとやり取りしたい人にとっては、GitLabを介して作業できるため、生活がはるかに簡単になります。
                      author: ラッセル・レヴィ
                      author_title: 共同創業者兼CTO、Chorus.ai
                    case_study_url: /customers/chorus/
                    data_ga_name: chorus case study
                    data_ga_location: case study carousel
                  - logo_url:  "/nuxt-images/logos/hackerone-logo.png"
                    institution_name: hackerone
                    quote:
                      img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
                      quote_text: GitLabは、セキュリティ上の欠陥を早期に発見するのに役立ち、開発者のフローに統合されています。 エンジニアはコードをGitLab CIにプッシュし、多くのカスケード監査ステップの1つから即座にフィードバックを得て、そこにセキュリティの脆弱性が組み込まれているかどうかを確認し、非常に具体的なセキュリティ問題をテストする可能性のある独自の新しいステップを構築することもできます。
                      author: ミッチ・トラレ
                      author_title: インフラストラクチャ責任者、ハッカーワン
                    case_study_url: /customers/hackerone/
                    data_ga_name: hackerone case study
                    data_ga_location: case study carousel
                  - logo_url: /nuxt-images/logos/anchormen-logo.svg
                    institution_name: Anchormen
                    quote:
                      img_url: /nuxt-images/blogimages/anchormen.jpg
                      quote_text: あなたは本当に仕事に集中することができます。 GitLabをセットアップしたら、基本的にこのセーフティネットができあがるため、他のすべてのことについて考える必要はありません。 GitLabはあなたを保護し、あなたは本当にビジネスロジックに集中することができます。 間違いなく運用効率が向上すると思います。
                      author: イェロエン・ヴレク
                      author_title: CTO、アンカーマン
                    case_study_url: /customers/anchormen/
                    data_ga_name: anchormen case study
                    data_ga_location: case study carousel


    - name: cta-block
      data:
        cards:
          - header: スタートアップ向けGitLabプログラムがあなたに適しているかどうかを確認してください。
            icon: increase
            link:
              text: スタートアップのためのGitLabプログラム
              url: /solutions/startups/join/
              data_ga_name: startup join
          - header: 現在のツールチェーンにはどれくらいの費用がかかりますか?
            icon: piggy-bank-alt
            link:
              text: ROI計算機
              url: /calculator/roi/
              data_ga_name: roi calculator
