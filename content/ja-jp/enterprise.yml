---
  title: GitLab for Enterprise  - コラボレーションが簡単に
  description: GitLab DevSecOpsプラットフォームを使用してエンタープライズソフトウェアのデリバリーを加速し、開発コストを削減し、チームコラボレーションを合理化
  image_title: /nuxt-images/open-graph/open-graph-enterprise.png
  side_navigation_links:
    - title: 概要
      href: '#overview'
    - title: メリット
      href: '#benefits'
    - title: 機能
      href: '# capabilities'
    - title: ケーススタディ
      href: '#case-studies'
  solutions_hero:
    title: エンタープライズ向け GitLab
    subtitle: 計画から本番まで、最も包括的なDevSecOpsプラットフォーム。組織全体で連携し、安全なコードをより速く送信して、ビジネス結果を推進します。
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: 無料トライアルを開始
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: free trial
      data_ga_location: header
    secondary_btn:
      text: 専門家に相談
      url: /sales
      data_ga_name: talk to an expert
      data_ga_location: header
    image:
      image_url: /nuxt-images/enterprise/enterprise-header.jpeg
      alt: "車の積み込みボックス"
      rounded: true
  by_industry_intro:
    logos:
      - name: Siemens
        image: /nuxt-images/logos/logo_siemens_color.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/siemens/
        aria_label: Siemensお客様事例へのリンク
      - name: hilti
        image: /nuxt-images/customers/hilti-logo.png
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/hilti/
        aria_label: Hiltiお客様事例へのリンク
      - name: Bendigo
        image: /nuxt-images/customers/babl_logo.png
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/bab/
        aria_label: Bendigoお客様事例へのリンク
      - name: Radio France
        image: /nuxt-images/logos/logoradiofrance.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/radiofrance/
        aria_label: Radio Franceお客様事例へのリンク
      - name: Credit agricole
        image: /nuxt-images/customers/credit-agricole-logo-1.png
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/credit-agricole/
        aria_label: Credit Agricoleお客様事例へのリンク
      - name: Kiwi
        image: /nuxt-images/customers/kiwi.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/kiwi/
        aria_label: Kiwiお客様事例へのリンク
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: 企業はDevSecOpsを使用してイノベーション、モダナイゼーション、加速しています。
      description: 個々のプロジェクトでうまく機能したものを、企業全体でスケールするのは難しいでしょう。DevSecOpsソリューションでは、問題を生み出すよりも解決していかなければなりません。ポイントソリューション上に構築された脆弱なツールチェーンとは異なり、GitLabはチームがより速くイテレーションを行い、一緒に革新し、複雑さとリスクを排除して、より高品質でより安全なソフトウェアをより速く提供するために必要なものをすべて提供します。
  by_solution_benefits:
    title: 大規模なDevSecOps
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: "コラボ画像"
    items:
      - icon:
          name: increase
          alt: 増加アイコン
          variant: marketing
        header: 生産性の高いコラボレーション
        text: ClickOpsを削減し、クラウドネイティブの採用に不可欠なチェックとバランスを導入します。
      - icon:
          name: gitlab-release
          alt: GitLab リリース アイコン
          variant: marketing
        header: リスクとコストの削減
        text: より多くのテストを行い、エラーをすぐに検出し、リスクを減らします。
      - icon:
          name: collaboration
          alt: コラボレーション アイコン
          variant: marketing
        header: より優れたソフトウェアをより速く提供
        text: 繰り返しのタスクを最小限に抑え、価値を生み出すタスクに焦点を当てます。
      - icon:
          name: release
          alt: アジャイル アイコン
          variant: marketing
        header: DevSecOpsを簡素化
        text: すべてのDevSecOpsプロセスを一元管理することで、複雑さが増すことなく、成功を拡張できます。
  by_industry_solutions_block:
    subtitle: 公共部門向けの完全なDevSecOpsプラットフォーム
    sub_description: "安全で堅牢なソースコード管理(SCM)、継続的な統合(CI)、継続的なデリバリー(CD)、継続的なソフトウェアセキュリティとコンプライアンスを含む1つのDevSecOpsプラットフォームから始まり、GitLabは次のような独自のニーズに対応します。"
    white_bg: true
    sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec-ja.svg
    alt: 特典の画像
    solutions:
      - title: アジャイル計画
        description: イノベーションイニシアチブを計画、開始、優先順位付け、管理し、完全に可視化して完了した作業を接続します。
        icon:
          name: release
          alt: アジャイル アイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/agile-delivery
        data_ga_name: agile planning
        data_ga_location: body
      - title: 自動化されたソフトウェアデリバリー
        description: 既知の脆弱性を含む、使用されている依存関係に関する重要な詳細を記載したプロジェクトのソフトウェア部品表を確認します。
        icon:
          name: automated-code
          alt: 自動コード アイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/delivery-automation/
        data_ga_name: Automated Software Delivery
        data_ga_location: body
      - title: 継続的なセキュリティとコンプライアンス
        description: リスクと遅延を軽減するために、開発プロセス全体でセキュリティを左にシフトし、コンプライアンスを自動化します。
        icon:
          name: devsecops
          alt: DevSecOps アイコン
          variant: marketing
          link_text: 詳細はこちら
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: Continuous Security & Compliance
        data_ga_location: body
      - title: バリューストリーム管理
        description: 組織内のすべてのステークホルダーに実用的な洞察を提供し、アイデアと開発におけるすべての段階を可視化します。
        icon:
          name: visibility
          alt: 可視性アイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: /solutions/value-stream-management
        data_ga_name: Value Stream Management
        data_ga_location: body
      - title: 信頼度
        description: 地理的に分散しているチームはGeoを使用して、災害復旧戦略の一環として、ウォームスタンバイで世界中に迅速かつ効率的なエクスペリエンスを提供します。
        icon:
          name: remote-world
          alt: リモートワールドアイコン
          variant: marketing
      - title: 高可用性 - 大規模
        description: 50,000 人以上のユーザーの高可用性のための参照アーキテクチャ
        icon:
          name: auto-scale
          alt: 自動スケール アイコン
          variant: marketing
        link_text: 詳細はこちら
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/
        data_ga_name: High availability
        data_ga_location: body
  by_solution_value_prop:
    title: 開発、SEC、運用のために1つのプラットフォーム
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: 包括的
        description: 作業を行う同じシステム内でプラットフォーム全体の分析を使用して、DevSecOpsライフサイクル全体を視覚化して最適化します。
        icon:
          name: digital-transformation
          alt: デジタルトランスフォーメーション アイコン
          variant: marketing
      - title: DevSecOpsを簡素化
        description: ワークフローを混乱させる可能性のあるサードパーティのプラグインやAPIに依存することなく、チームやライフサイクルステージ間で共通のツールを使用できます。
        icon:
          name: devsecops
          alt: DevSecOps アイコン
          variant: marketing
      - title: 安全
        description: 各コミットの脆弱性とコンプライアンス違反をスキャンします。
        icon:
          name: eye-magnifying-glass
          alt: 目の拡大鏡アイコン
          variant: marketing
      - title: 透明性とコンプライアンス
        description: 計画からコードの変更、承認まで、すべてのアクションを自動的にキャプチャして関連付けることで、監査やレトロスペクティブ中に簡単にトレーサビリティを確保できます。
        icon:
          name: release
          alt: 盾のアイコン
          variant: marketing
      - title: 簡単にスケール
        description: リファレンスアーキテクチャは、50,000人以上のユーザーへのインストールの高可用性を拡張する方法を示しています。
        icon:
          name: monitor-web-app
          alt: Web アプリのアイコンを監視する
          variant: marketing
      - title: スケーラブル
        description: KubernetesクラスターにGitLabをデプロイし、水平にスケールします。アップグレードによるダウンタイムはありません。GitOpsワークフローまたはCI/CDワークフローを使用します。
        icon:
          name: auto-scale
          alt: 自動スケール アイコン
          variant: marketing
  by_industry_case_studies:
    title: お客様が気づいたメリット
    charcoal_bg: true
    link: 
      text: すべてのケーススタディ
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Siemens
        subtitle: SiemensがGitLabを使用してオープンソースのDevSecOpsカルチャーを作成した方法
        image:
          url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
          alt: 会社のミーティング
        button:
          href: /customers/siemens/
          text: 詳細はこちら
          data_ga_name: siemens learn more
          data_ga_location: body
      - title: Hilti
        subtitle: CI/CDと堅牢なセキュリティスキャンがHiltiのSDLCを高速化した方法
        image:
          url: /nuxt-images/blogimages/hilti_cover_image.jpg
          alt: 遠くからの建物
        button:
          href: /customers/hilti/
          text: 詳細はこちら
          data_ga_name: hilti learn more
          data_ga_location: body
      - title: Bendigo
        subtitle: GitLabがBendigoとAdelaide BankでDevSecOpsを加速する方法を学ぶ
        image:
          url: /nuxt-images/blogimages/bab_cover_image.jpg
          alt: ダウンタウン ビュー
        button:
          href: /customers/bab/
          text: 詳細はこちら
          data_ga_name: bendigo learn more
          data_ga_location: body
      - title: Radio France
        subtitle: Radio FranceはGitLab CI/CDを使用して5倍速くデプロイ
        image:
          url: /nuxt-images/blogimages/radio-france-cover-image.jpg
          alt: フランスの表紙
        button:
          href: /customers/radiofrance/
          text: 詳細はこちら
          data_ga_name: radio france learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: 資力
    link:
      text: すべてのリソースを表示
    cards:
      - icon:
          name: webcast
          alt: ウェブキャスト アイコン
          variant: marketing
        event_type: ウェビナー
        header: エンドツーエンドのDevOpsプラットフォームを使用して、問題を減らしてより多くの価値を提供
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_waves.png
        alt: 波
        href: https://www.youtube.com/watch?v=wChaqniv3HI
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: webcast
          alt: ウェブキャスト アイコン
          variant: marketing
        event_type: ウェビナー
        header: DevOpsプラットフォームのテクニカルデモ
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_webcast.png
        alt: カフェで働く
        href: https://youtu.be/Oei67XCnXMk
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: event
          alt: イベントアイコン
          variant: marketing
        event_type: バーチャルイベント
        header: GitLabによるNorthwestern Mutualのデジタルトランスフォーメーション
        link_text: 視聴する
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        alt: GitLab 無限ループ
        href: https://www.youtube.com/watch?v=o6EY_WwEFpE
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: event
          alt: イベントアイコン
          variant: marketing
        event_type: バーチャルイベント
        header: DevOpsの次のイテレーション(CEO基調講演)
        link_text: 視聴する
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: DevOps イメージ
        href: https://www.youtube.com/watch?v=Wx8tDVSeidk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: ケーススタディ アイコン
          variant: marketing
        event_type: ケーススタディ
        header: ゴールドマン・サックスは2週間に1つのビルドから1日に1,000個以上に改善
        link_text: 詳細はこちら
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: 上から見た木
        href: /customers/goldman-sachs/
        data_ga_name: Goldman Sachs
        data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
      - icon:
          name: video
          alt: ビデオ アイコン
          variant: marketing
        event_type: Video
        header: GitLabインフォマーシャル
        link_text: 視聴する
        image: /nuxt-images/features/resources/resources_golden_dog.png
        alt: 眠っている犬
        href: https://www.youtube.com/embed/gzYTZhJlHoI?
        aos_animation: fade-up
        aos_duration: 1400
