---
  title: Why GitLab Ultimate?
  description: Achieve organization wide security, compliance, and planning with GitLab Ultimate
  side_menu:
    anchors:
      text: On this page
      data:
      - text: Overview
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Summary
          href: "#wu-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Key solutions
          href: "#wu-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Customer case studies
          href: "#wu-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: ROI calculator
        href: "#wu-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Ultimate features
        href: "#wu-ultimate-features"
        data_ga_name: ultimate-features
        data_ga_location: side-navigation
        nodes:
        - text: Advanced security testing
          href: "#wu-advanced-security-testing"
          data_ga_name: advanced-security-testing
          data_ga_location: side-navigation
        - text: Security risk mitigation
          href: "#wu-security-risk-mitigation"
          data_ga_name: security-risk-mitigation
          data_ga_location: side-navigation
        - text: Compliance
          href: "#wu-compliance"
          data_ga_name: compliance
          data_ga_location: side-navigation
        - text: Portfolio management
          href: "#wu-portfolio-management"
          data_ga_name: portfolio-management
          data_ga_location: side-navigation
        - text: Value stream management
          href: "#wu-value-stream-management"
          data_ga_name: value-stream-management
          data_ga_location: side-navigation
        - text: Free guest users
          href: "#wu-free-guest-users"
          data_ga_name: free-guest-users
          data_ga_location: side-navigation
        - text: Other Ultimate features
          href: "#wu-other-ultimate-features"
          data_ga_name: other-ultimate-features
          data_ga_location: side-navigation
      - text: Get in touch
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Next Steps
      data:
      - text: Buy Ultimate now
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-ultimate-now
        data_ga_location: side-navigation
      - text: Learn about Premium
        href: /pricing/premium/
        variant: secondary
        icon: true
        data_ga_name: learn-about-premium
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wu-summary
      title: Why Ultimate?
      subtitle: GitLab Ultimate is ideal for organizations aiming to optimize and accelerate delivery while managing priorities, security, risk, and compliance.
      text: Available in both SaaS and self-managed deployment options, GitLab Ultimate adds **advanced security capabilities, security risk mitigation, compliance, portfolio management** and **value stream management**. In addition, GitLab Ultimate allows for free guest user licenses to improve your license usage for users with minimal interaction with the system. GitLab Ultimate also includes **priority support, live upgrade assistance**, and a **Technical Account Manager** for eligible customers.
      buttons:
        - variant: primary
          icon: false
          text: Buy Ultimate now
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: learn-about-ultimate
          data_ga_location: mobile-body
        - variant: secondary
          icon: true
          text: Learn about Premium
          url: /pricing/premium/
          data_ga_name: learn-about-premium
          data_ga_location: mobile-body
  - name: guest-calculator
    data:
      id: wu-guest-calculator
      title: Calculate the cost for your organization
      input_title: GitLab offers unlimited free guest users on Ultimate plans
      caption: |
        *All plans billed annually. The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller. See our [Pricing Page](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"} for more details.
      seats:
        title: Number of GitLab seats
        tooltip: GitLab seats are people with reporter, developer, maintainer or owner access. Guest users do not consume a seat in GitLab Ultimate. Learn more about seat usage <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined">here</a>.
      guests:
        title: Number of guest users
        tooltip: Guest users can create and assign issues, view certain analytics, optionally view code, and more. Learn more <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions">here</a>.
      premium:
        title: Premium monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Buy Premium
      ultimate:
        title: Ultimate monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Buy Ultimate

  - name: no-image-accordion
    data:
      id: wu-key-solutions
      title: GitLab Ultimate helps you
      is_accordion: true
      items:
        - icon:
            name: increase
            alt: Increase icon
            variant: marketing
          header: Increase Operational Efficiencies
          text:	GitLab Ultimate provides a single, scalable interface for organization wide DevSecOps, reducing handoffs across tools and teams - thereby improving efficiencies.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Speed Icon
          header: Deliver Better Products Faster
          text: With end to end Value Stream Management and Portfolio Management, GitLab Ultimate allow for greater visibility and transparency across projects - helping to eliminate bottlenecks and deliver products faster.
        - icon:
            name: lock-alt-5
            alt: Lock icon
            variant: marketing
          header: Reduce Security and Compliance Risk
          text: GitLab Ultimate introduces built-in security testing, compliance and preventive security for cloud native applications helping you manage security risk and achieve regulatory compliance.
  - name: case-study-carousel
    data:
      id: wu-customer-case-studies
      header: See how companies use GitLab Ultimate
      header_link:
        url: /customers/
        text: Read all case studies
        data_ga_name: Read all case studies
        data_ga_location: body
      case_studies:
        - title: HackerOne
          subtitle: HackerOne achieves 5x faster deployments with GitLab’s integrated security
          image:
            url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
            alt:
          button:
            href: /customers/hackerone/
            text: Learn more
            data_ga_name: visit HackerOne case study
            data_ga_location: body
        - title: The Zebra
          subtitle: How The Zebra achieved secure pipelines in black and white
          image:
            url: /nuxt-images/blogimages/thezebra_cover.jpg
            alt:
          button:
            href: /customers/thezebra/
            text: Learn more
            data_ga_name: visit The Zebra case study
            data_ga_location: body
        - title: Drupal Association
          subtitle: Drupal Association eases entry for new committers, speeds implementations
          image:
            url: /nuxt-images/blogimages/drupalassoc_cover.jpg
            alt:
          button:
            href: /customers/drupalassociation/
            text: Learn more
            data_ga_name: visit Drupal Association case study
            data_ga_location: body
        - title: Hilti
          subtitle: How CI/CD and robust security scanning accelerated Hilti’s SDLC
          image:
            url: /nuxt-images/blogimages/hilti_cover_image.jpg
            alt:
          button:
            href: /customers/hilti/
            text: Learn more
            data_ga_name: visit Hilti case study
            data_ga_location: body
        - title: Conversica
          subtitle: Conversica leads AI innovation with help from GitLab Ultimate
          image:
            url: /nuxt-images/blogimages/conversicaimage.jpg
            alt:
          button:
            href: /customers/conversica/
            text: Learn more
            data_ga_name: visit Conversica case study
            data_ga_location: body
        - title: US Army Cyber School
          subtitle: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
          image:
            url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
            alt:
          button:
            href: /customers/us_army_cyber_school/
            text: Learn more
            data_ga_name: visit US Army Cyber School case study
            data_ga_location: body
  - name: roi-calculator-block
    data:
      id: wu-roi-calculator
      header:
        gradient_line: true
        title:
          text: ROI calculator
          anchor: wu-roi-calculator
        subtitle: How much is your toolchain costing you?
      calc_data_src: calculator/roi/index
  features_block:
    id: wu-ultimate-features
    tier: ultimate
    header:
      gradient_line: true
      note: Please note this is not a comprehensive set of capabilities in GitLab Ultimate, visit [about.gitlab.com/features](https://about.gitlab.com/features){data-ga-name="features page" data-ga-location="body"} for the latest. GitLab continuously adds features every month and evaluates features that can be moved to lower tiers to benefit more users.
      title:
        text: Ultimate features
        anchor: wu-ultimate-features
        button:
          text: Compare all features
          data_ga_name: Compare all features
          data_ga_location: body
          url: /pricing/feature-comparison/
    pricing_themes:
      - id: wu-advanced-security-testing
        theme: Advanced security testing
        text: protects the integrity of your software supply chain with built in security testing.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/security-compliance/
      - id: wu-security-risk-mitigation
        theme: Security risk mitigation
        text: helps you manage your organization's security policies, alerts, and approval rules.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/security-compliance/
      - id: wu-compliance
        theme: Compliance
        text: ensures your code, deployments, and environments comply with changing regulations and emerging risks.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/compliance/
      - id: wu-portfolio-management
        theme: Portfolio management
        text: allows you to manage large scale organization wide projects.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/portfolio-management/
      - id: wu-value-stream-management
        theme: Value stream management
        text: measures and manages the business value of your DevSecOps lifecycle.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/value-stream-management/
      - id: wu-free-guest-users
        theme: Free guest users
      - id: wu-other-ultimate-features
        text: Ultimate features unrelated to a theme
