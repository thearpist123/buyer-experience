---
title: La plateforme DevSecOps
description: De la planification à la production, faites collaborer les équipes
  dans une seule application. Livrez du code sécurisé de manière plus efficace
  pour offrir votre valeur ajoutée plus rapidement.
schema_org: >
  {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab est la plateforme DevOps qui permet aux organisations d'optimiser le rendement global du développement logiciel en livrant des logiciels plus rapidement et plus efficacement, tout en renforçant la sécurité et la conformité. Grâce à GitLab, chaque équipe au sein de l'entreprise peut planifier, compiler, sécuriser et déployer des logiciels de manière collaborative et permettre ainsi à l'entreprise de prospérer plus rapidement, en toute transparence, en toute cohérence et avec une traçabilité complète. GitLab offre, au moyen d'une approche commerciale à noyau ouvert, une solution de bout en bout du cycle de vie du développement logiciel. GitLab compte 30 millions d'utilisateurs inscrits et plus d'un million d'utilisateurs actifs possédant une licence, et dispose d'une communauté dynamique de plus de 2 500 contributeurs. GitLab partage ouvertement plus d'informations que la plupart des entreprises, avec son approche publique par défaut. En d'autres termes, nos projets, notre stratégie, notre orientation et nos métriques sont discutés ouvertement et sont disponibles sur notre site Web. Nos valeurs sont la collaboration, les résultats, l'efficacité, la diversité, l'inclusion et l'appartenance, l'itération et la transparence (ou CREDIT en anglais) et celles-ci constituent notre culture.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Notre mission est de transformer tout travail créatif du mode lecture seule en mode lecture-écriture afin que tout le monde puisse contribuer.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Meilleure équipe d'ingénieurs 2021 selon Comparably, prix Challenger 2021 du Gartner Magic Quadrant pour les tests de sécurité des applications, prix DevOps Dozen du meilleur fournisseur de solutions DevOps pour 2019, prix 451 Firestarter de 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}
cta_block:
  badge:
      text: "Découvrez GitLab Duo : workflows alimentés par l'IA →"
      link: "/gitlab-duo/"
      data_ga_name: "gitlab duo"
      data_ga_location: "hero"
  title: Des logiciels. Plus rapidement.
  subtitle: GitLab est la plateforme DevSecOps <br>alimentée par l'IA la plus complète.
  primary_button:
    text: Commencer un essai gratuit
    link: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial
    data_ga_name: free trial
    data_ga_location: hero
  secondary_button:
    text: Qu'est-ce que GitLab ?
    modal:
      video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
      video_title: Qu'est-ce que GitLab ?
    data_ga_name: watch demo
    data_ga_location: hero
  image_copy: La plateforme DevSecOps
  images:
    - id: 1
      image: /nuxt-images/home/hero/developer-productivity-img.svg
      alt: Brand image of GitLab boards
    - id: 2
      image: /nuxt-images/home/hero/security-img.svg
      alt: Brand image of GitLab product
    - id: 3
      image: /nuxt-images/home/hero/value-stream-img.svg
      alt: Brand image of GitLab roadmap
customer_logos_block:
  text: Adoptée par
  showcased_enterprises:
    - image_url: /nuxt-images/home/logo_tmobile_mono.svg
      link_label: Lien vers la page d'accueil de T-Mobile et GitLab"
      alt: T-Mobile logo
      url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
    - image_url: /nuxt-images/home/logo_goldman_sachs_mono.svg
      link_label: Lien vers l'étude de cas de Goldman Sachs
      alt: Goldman Sachs logo
      url: /customers/goldman-sachs/
    - image_url: /nuxt-images/software-faster/airbus-logo.png
      link_label: Lien vers l'étude de cas d'Airbus
      alt: Airbus logo
      url: /customers/airbus/
    - image_url: /nuxt-images/logos/lockheed-martin.png
      link_label: Lien vers l'étude de cas de Lockheed Martin
      alt: Lockheed martin logo
      url: /customers/lockheed-martin/
    - image_url: /nuxt-images/home/logo_nvidia_mono.svg
      link_label: Lien vers l'étude de cas de Nvidia
      alt: Nvidia logo
      url: /customers/nvidia/
    - image_url: /nuxt-images/home/logo_ubs_mono.svg
      link_label: UBS a créé sa propre plateforme DevSecOps en utilisant GitLab
      alt: UBS logo
      url: /blog/2021/08/04/ubs-gitlab-devops-platform/
featured_content:
  cards:
    - header: Premiers pas avec GitLab
      description: Vous êtes nouveau sur GitLab et vous ne savez pas par où
        commencer ? Nous vous guiderons à travers les bases afin que vous
        sachiez à quoi vous attendre en cours de route.
      link:
        href: /get-started/
        text: Explorez les ressources
        data_ga_name: get started
        data_ga_location: home content block
      icon: cog-check
    - header: "Rapport mondial 2023 sur le DevSecOps : la sécurité sans sacrifices"
      description: Nous avons demandé à plus de 5 000 professionnels DevSecOps de nous
        faire part de leurs succès et de leurs défis lors de la création
        d'applications plus sécurisées.
      link:
        href: /developer-survey/
        text: Lire le rapport
        data_ga_name: 2023 global devsecops report
        data_ga_location: home content block
      icon: doc-pencil-alt
    - header: Des flux de travail assistés par l'IA pour soutenir chaque équipe
      description: Découvrez comment améliorer l'efficacité de vos processus et
        réduire les temps de cycle à l'aide de l'IA à chaque phase du cycle de
        vie du développement logiciel.
      link:
        href: /solutions/ai/
        text: En savoir plus
        data_ga_name: solutions ai
        data_ga_location: home content block
      icon: ai-code-suggestions
resource_card_block:
  column_size: 4
  header_text: Ressources
  header_cta_text: Consulter toutes les ressources
  header_cta_href: /resources/
  header_cta_ga_name: view all resources
  header_cta_ga_location: body
  cards:
    - icon:
        name: ebook-alt
        variant: marketing
        alt: Ebook Icon
      event_type: E-book
      header: "Guide DevOps pour débutant "
      link_text: En savoir plus
      href: https://page.gitlab.com/resources-ebook-beginners-guide-devops.html
      image: /nuxt-images/home/resources/Devops.png
      alt: Winding path
      data_ga_name: Beginner's Guide to DevOps
      data_ga_location: body
    - icon:
        name: topics
        variant: marketing
        alt: Topics Icon
      event_type: Thématiques
      header: Qu'est-ce que CI/CD ?
      link_text: En savoir plus
      href: /topics/ci-cd/
      image: /nuxt-images/resources/fallback/img-fallback-cards-cicd.png
      alt: Text reading CI/CD on a gradient
      data_ga_name: What is CI/CD?
      data_ga_location: body
    - icon:
        name: report-alt
        variant: marketing
        alt: Report Icon
      event_type: Rapports
      header: Édition 2023 de nos rapports mondiaux sur le DevSecOps
      link_text: En savoir plus
      href: /developer-survey/
      image: /nuxt-images/home/devsecops-survey-thumbnail.jpeg
      alt: A globe outlined with the DevSecOps loop with the text What's next in
        DevSecOps
      data_ga_name: 2023 DevSecOps Survey
      data_ga_location: body
    - icon:
        name: blog-alt
        variant: marketing
        alt: Blog Icon
      event_type: Articles de blog
      header: Série sur l'IA et le ML en DevSecOps
      description: Suivez notre série d'articles de blogs détaillant notre intégration
        de l'IA et de l'apprentissage automatique (ML) tout au long du cycle de
        vie du développement logiciel.
      link_text: En savoir plus
      href: https://about.gitlab.com/blog/2023/04/24/ai-ml-in-devsecops-series/
      image: /nuxt-images/home/resources/ai-experiment-stars.png
      alt: ai-experiment-stars
      data_ga_name: ai/ml in devsecops series
      data_ga_location: body
    - icon:
        name: partners
        variant: marketing
        alt: Partners Icon
      event_type: Partenaires
      header: Découvrez les avantages de GitLab sur AWS
      link_text: En savoir plus
      href: /partners/technology-partners/aws/
      image: /nuxt-images/home/resources/AWS.png
      alt: Text reading AWS on a gradient
      data_ga_name: Discover the benefits of GitLab on AWS
      data_ga_location: body
    - icon:
        name: announce-release
        variant: marketing
        alt: Announce Release Icon
        hex_color: "#FFF"
      event_type: Releases
      header: GitLab 16.1 est sorti avec une toute nouvelle navigation
      link_text: En savoir plus
      href: "/releases/2023/06/22/gitlab-16-1-released/"
      alt: GitLab version number on a gradient
      image: "/nuxt-images/home/resources/16_1.png"
      data_ga_name: GitLab 16.1 released with all new navigation
      data_ga_location: body
quotes_carousel_block:
  header: Les équipes sont plus performantes avec GitLab
  header_link:
    url: /customers/
    text: Lire nos études de cas
    data_ga_name: Read our case studies
    data_ga_location: body
  quotes:
    - main_img:
        url: /nuxt-images/home/HohnAlan.jpg
        alt: Picture of Alan Hohn
      quote: "En passant à GitLab et en automatisant le déploiement, les équipes de développement sont passées d'un rythme de livraisons mensuelles ou hebdomadaires à des livraisons quotidiennes ou à plusieurs livraisons quotidiennes."
      author: Alan Hohn
      logo: 
        url: /nuxt-images/logos/lockheed-martin.png
        alt: ''
      role: |
        Director of Software Strategy,
        Lockheed Martin
      statistic_samples:
        - data:
            highlight: 80x
            subtitle: faster CI pipeline builds
        - data:
            highlight: 90 %
            subtitle: de temps en moins consacré à la maintenance du système
      url: /customers/lockheed-martin/
      header: Lockheed Martin économise du temps, de l'argent et des efforts
        technologiques grâce à GitLab
      data_ga_name: lockheed martin case study
      data_ga_location: home case studies
      cta_text: Consulter l'étude de cas
    - main_img:
        url: /nuxt-images/home/JasonManoharan.png
        alt: Picture of Jason Monoharan
      quote: "La vision de GitLab, qui consiste à allier la stratégie à la portée du logiciel et au code est très puissante. J'apprécie les efforts majeurs que GitLab ne cesse de fournir dans sa plateforme."
      author: Jason Monoharan
      logo: 
        url: /nuxt-images/home/logo_iron_mountain_mono.svg
        alt: ''
      role: |
        VP of Technology,
        Iron Mountain
      statistic_samples:
        - data:
            highlight: 150 000 $
            subtitle: économies de coûts réalisées par an en moyenne
        - data:
            highlight: 20 heures
            subtitle: gain de temps d'intégration par projet
      url: customers/iron-mountain/
      header: Iron Mountain fait évoluer son DevOps avec GitLab Ultimate
      data_ga_name: iron mountain
      data_ga_location: home case studies
      cta_text: Consulter l'étude de cas
    - main_img:
        url: /nuxt-images/home/EvanO_Connor.png
        alt: Picture of Evan O’Connor
      quote: "L'engagement de GitLab envers sa communauté open source nous permet de travailler directement avec des ingénieurs pour résoudre des problèmes techniques complexes."
      author: Evan O’Connor
      logo: 
        url: /nuxt-images/home/havenTech.png
        alt: ''
      role: |
        Platform Engineering Manager,
        Haven Technologies
      statistic_samples:
        - data:
            highlight: 62 %
            subtitle: des utilisateurs mensuels ont exécuté des jobs de détection des
              secrets
        - data:
            highlight: 66 %
            subtitle: des utilisateurs mensuels ont exécuté des jobs de scanners sécurisés
      url: /customers/haven-technologies/
      header: Haven Technologies a migré vers Kubernetes avec GitLab
      data_ga_name: haven technology
      data_ga_location: home case studies
      cta_text: Consulter l'étude de cas
    - main_img:
        url: /nuxt-images/home/RickCarey.png
        alt: Picture of Rick Carey
      quote: "Nous avons une expression chez UBS : « Tous les développeurs attendent à la même vitesse ». Par conséquent, tout ce que nous pouvons faire pour réduire leur temps d'attente offre une valeur ajoutée. Et, grâce à GitLab, nous pouvons bénéficier de cette parfaite intégration."
      author: Rick Carey
      logo: 
        url: /nuxt-images/home/logo_ubs_mono.svg
        alt: ''
      role: |
        Group Chief Technology Officer,
        UBS
      statistic_samples:
        - data:
            highlight: 1 million
            subtitle: de compilations réussies dans les six premiers mois
        - data:
            highlight: 12 000
            subtitle: utilisateurs GitLab actifs
      url: /blog/2021/08/04/ubs-gitlab-devops-platform/
      header: UBS a créé sa propre plateforme DevOps à l'aide de GitLab
      data_ga_name: ubs
      data_ga_location: home case studies
      cta_text: Consulter l'étude de cas
    - main_img:
        url: /nuxt-images/home/LakshmiVenkatrama.png
        alt: Picture of Lakshmi Venkatraman
      quote: "GitLab nous permet une excellente collaboration avec les membres de l'équipe et entre les différentes équipes. En tant que chef de projet, la possibilité de suivre un projet ou la charge de travail d'un membre de l'équipe permet d'éviter les retards. Lorsque le projet est terminé, nous pouvons facilement automatiser le packaging des logiciels et fournir les solutions au client. Et avec GitLab, tout le processus se déroule en un seul endroit."
      author: Lakshmi Venkatraman
      logo: 
        url: /nuxt-images/home/singleron.svg
        alt: ''
      role: |
        Project Manager,
        Singleron Biotechnologies
      url: https://www.youtube.com/watch?v=22nmhrlL-FA
      header: Singleron utilise GitLab pour collaborer sur une plateforme unique afin
        d'améliorer les soins aux patients
      data_ga_name: singleron video
      data_ga_location: home case studies
      cta_text: Regarder la vidéo
solutions_block:
  title: Une plateforme, une équipe
  image: /nuxt-images/home/solutions/solutions-top-down.png
  alt: Top down image of office
  description: Que vous commenciez par intégrer quelques solutions ponctuelles ou
    par simplifier l'ensemble de votre chaîne d'outils, vous pouvez désormais le
    faire au sein d'une seule et même équipe, sur une seule et même plateforme.
    Collaborer de la planification jusqu'à la production sur une seule
    plateforme, avec une sécurité intégrée.
  subtitle: L'approche DevSecOps idéale
  sub_image: /nuxt-images/home/solutions/solutions.png
  solutions:
    - title: Accélérez votre transformation numérique
      description: Atteignez plus rapidement vos objectifs de transformation numérique
        avec une plateforme DevSecOps pour l'ensemble de votre organisation.
      icon:
        name: accelerate
        alt: Accelerate Icon
        variant: marketing
      link_text: En savoir plus
      link_url: /solutions/digital-transformation/
      data_ga_name: digital transformation
      data_ga_location: body
      image: /nuxt-images/home/solutions/acceler.png
      alt: Text bubbles of communicating teams
    - title: Livrez des logiciels plus rapidement
      description: Automatisez votre processus de livraison de logiciels afin d'en
        améliorer le flux de valeur plus rapidement avec un code de meilleure
        qualité.
      icon:
        name: deliver
        alt: Deliver Icon
        variant: marketing
      link_text: En savoir plus
      link_url: /solutions/delivery-automation/
      data_ga_name: delivery automation
      data_ga_location: body
      image: /nuxt-images/home/solutions/deliver.png
      alt: Text bubbles of communicating teams
    - title: Assurez la conformité
      description: Simplifiez le processus de conformité logicielle continue en
        définissant les règles d'utilisation, en les faisant respecter et en
        établissant des rapports de conformité, tout cela sur une seule
        plateforme.
      icon:
        name: ensure
        alt: Ensure Icon
        variant: marketing
      link_text: En savoir plus
      link_url: /solutions/continuous-software-compliance/
      data_ga_name: continuous software compliance
      data_ga_location: body
      image: /nuxt-images/home/solutions/ensure.png
      alt: Text bubbles of communicating teams
    - title: Intégrez la sécurité à votre processus
      description: Adoptez des pratiques DevSecOps avec une assurance continue de la
        sécurité des logiciels à chaque étape.
      icon:
        name: shield-check-light
        alt: Shield Check Icon
        variant: marketing
      link_text: En savoir plus
      link_url: /solutions/continuous-software-security-assurance/
      data_ga_name: continuous software security assurance
      data_ga_location: body
      image: /nuxt-images/home/solutions/build.png
      alt: Text bubbles of communicating teams
    - title: Améliorez le travail collaboratif et la visibilité des projets
      description: Offrez à chaque membre de votre équipe une plateforme unique pour
        collaborer et visualiser l'intégralité du processus, de la planification
        jusqu'à la production.
      icon:
        name: improve
        alt: Improve Icon
        variant: marketing
      link_text: En savoir plus
      link_url: /solutions/devops-platform/
      data_ga_name: devops platform
      data_ga_location: body
      image: /nuxt-images/home/solutions/improve.png
      alt: Text bubbles of communicating teams
badges:
  header: GitLab est la plateforme DevSecOps idéale
  tabs:
    - tab_name: Les leaders du DevSecOps
      tab_id: leaders
      tab_icon: /nuxt-images/icons/ribbon-check-transparent.svg
      copy: >
        **Nos utilisateurs en témoignent.**

        GitLab se classe parmi les leaders au Classement G2 dans les catégories DevSecOps
      cta:
        url: /analysts
        cta_text: En savoir plus
        data_ga_name: analysts page
        data_ga_location: leaders in devops tab - badge section
      badge_images:
        - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
          alt: G2 Entreprise Leader - Automne 2022
        - src: /nuxt-images/badges/midmarketleader_fall2022.svg
          alt: G2 Mid-Market Leader - Automne 2022
        - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
          alt: G2 PME Leader - Automne 2022
        - src: /nuxt-images/badges/bestresults_fall2022.svg
          alt: G2 Meilleurs résultats - Automne 2022
        - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
          alt: G2 Meilleur relationnel Entreprise - Automne 2022
        - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
          alt: G2 Meilleur relationnel Mid-Market - Automne 2022
        - src: /nuxt-images/badges/easyiesttodobusinesswith_fall2022.svg
          alt: G2 Meillleur fournisseur du Mid-Market - Automne 2022
        - src: /nuxt-images/badges/bestusability_fall2022.svg
          alt: G2 Meilleure convivialité - Automne 2022
    - tab_name: Rapports des analystes du secteur
      tab_id: research
      tab_icon: /nuxt-images/icons/doc-pencil-transparent.svg
      copy: |
        **Ce que les analystes du secteur pensent de GitLab**
      cta:
        url: /analysts
        cta_text: En savoir plus
        data_ga_name: analysts page
        data_ga_location: industry analyst research tab - badge section
      analysts:
        - logo: /nuxt-images/logos/forrester-logo.svg
          text: "Rapport Forrester Wave™ 2019 : outils d'intégration continue
            cloud-native"
          link:
            url: https://about.gitlab.com/analysts/forrester-cloudci19/
            data_ga_name: Forrester Cloud-Native Continuous Integration Tools
        - logo: /nuxt-images/logos/gartner-logo.svg
          text: Rapport Gartner® Magic Quadrant 2022 des outils de planification Agile
            d'entreprise
          link:
            url: https://about.gitlab.com/analysts/gartner-eapt21/
            data_ga_name: Gartner Magic Quadrant for Enterprise Agile Planning Tools
        - logo: /nuxt-images/logos/gartner-logo.svg
          text: Rapport Gartner® Magic Quadrant™ 2022 des tests de sécurité des
            applications
          link:
            url: https://about.gitlab.com/analysts/gartner-ast22/
            data_ga_name: Gartner Magic Quadrant for Application Security Testing
        - logo: /nuxt-images/logos/gartner-logo.svg
          text: Gartner® Market Guide 2021 des plateformes de livraison logicielle et
            chaîne de valeur
          link:
            url: https://about.gitlab.com/analysts/gartner-vsdp21/
            data_ga_name: Gartner Market Guide for Value Stream Delivery Platforms
top-banner:
  text: L'enquête DevSecOps 2022 est disponible, avec des insights provenant de
    5 000 experts DevOps.
  link:
    text: Découvrir l'enquête
    href: /developer-survey/
    ga_name: devsecops survey
    icon: gl-arrow-right
