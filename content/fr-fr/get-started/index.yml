---
title: "Découvrez GitLab"
description: "Comment voulez-vous commencer ?"
components:
    -
        name: "solutions-hero"
        data:
            title: "Découvrez GitLab"
            subtitle: "Vous êtes nouveau sur GitLab et vous ne savez pas par où commencer ? Nous vous présenterons les principes de base afin que vous sachiez à quoi vous attendre en cours de route."
            aos_animation: "fade-down"
            aos_duration: 500
            img_animation: zoom-out-left
            img_animation_duration: 1600
            primary_btn:
                text: "Commencez votre essai gratuit"
                url: "/free-trial/"
                data_ga_name: free trial
                data_ga_location: header
            secondary_btn:
                url: "/demo/"
                text: "Voir une démonstration"
                data_ga_name: demo
                data_ga_location: header
                variant: "tertiary"
                icon:
                    name: "play"
                    variant: "product"
            image:
                image_url: "/nuxt-images/get-started/working-person.jpeg"
                alt: "Image : deux mains travaillant sur un ordinateur portable"
                bordered: true
    -
        name: "get-started-menu"
        data:
            title: "Listes de contrôle pour l'installation rapide"
            block:
                -
                    name: "Priorité à l'entreprise"
                    links:
                        -
                            text: "Pour les petites entreprises"
                            href: "/get-started/small-business/"
                            data_ga_name: "for small business"
                            data_ga_location: "body"
                            icon:
                                name: "clipboard-checks"
                                alt: "Icône de presse-papier"
                        -
                            text: "Pour les entreprises"
                            href: "/get-started/enterprise/"
                            data_ga_name: "for enterprise"
                            data_ga_location: "body"
                            icon:
                                name: "clipboard-checks"
                                alt: "Icône de presse-papier"
                        -
                            text: "Pour construire votre dossier d'entreprise"
                            href: "/get-started/build-business-case/"
                            data_ga_name: "for building your business case"
                            data_ga_location: "corps"
                            icon:
                                name: "clipboard-checks"
                                alt: "Icône de presse-papier"
                        -
                            text: "Pourquoi GitLab\_?"
                            href: "/why-gitlab/"
                            data_ga_name: "simplify software development with The One DevOps Platform"
                            data_ga_location: "body"
                            icon:
                                name: "docs-alt"
                                alt: "Icône Docs"
                -
                    name: "Tirer le meilleur parti de GitLab"
                    links:
                        -
                            text: "Pour augmenter votre utilisation de GitLab"
                            href: "https://docs.gitlab.com/ee/development/scalability"
                            data_ga_name: "for scaling your gitlab usage"
                            data_ga_location: "body"
                            icon:
                                name: "increase"
                                alt: "Icône d'augmentation"
                        -   text: Démarrer avec l'intégration continue
                            href: /get-started/continuous-integration/
                            data_ga_name: get started with continuous integration
                            data_ga_location: body
                            icon:
                                name: cog
                                alt: cog Icon
                        -
                            text: "Pour la mise en place de la sécurité et de la conformité"
                            href: "/solutions/continuous-software-security-assurance/"
                            data_ga_name: "for setting up security and compliance"
                            data_ga_location: "body"
                            icon:
                                name: "cog-code"
                                alt: "Icône de code"
                        -
                            text: "Pour l'installation de versions plus récentes"
                            href: "https://docs.gitlab.com/ee/update/"
                            data_ga_name: "for installing newer versions"
                            data_ga_location: "body"
                            icon:
                                name: "cog-check"
                                alt: "Vérifier l'icône du rouage"
                        -
                            text: "Pour la mise à niveau"
                            href: "/pricing/"
                            data_ga_name: "for up-tiering"
                            data_ga_location: "body"
                            icon:
                                name: "increase"
                                alt: "Icône d'augmentation"
                        -
                            text: "Pour utiliser plus de capacités"
                            href: "/features/"
                            data_ga_name: for using more capabilities
                            data_ga_location: "body"
                            icon:
                                name: "cogs"
                                alt: "Icône des rouages"
                -
                    name: "Assurer une transition en douceur"
                    links:
                        -
                            text: "Pour la transition depuis GitHub"
                            href: "https://docs.gitlab.com/ee/user/project/import/github.html"
                            data_ga_name: "for transitioning from github"
                            data_ga_location: "body"
                            icon:
                                name: "digital-transformation"
                                alt: "Transformation numérique Icône"
                        -
                            text: "Pour importer les problèmes de votre projet depuis Jira"
                            href: "https://docs.gitlab.com/ee/user/project/import/jira.html"
                            data_ga_name: "For importing your project issues from Jira"
                            data_ga_location: "corps"
                            icon:
                                name: "digital-transformation"
                                alt: "Transformation numérique Icône"
                        -
                            text: "Pour migrer d'autres projets vers GitLab"
                            href: "https://docs.gitlab.com/ee/user/project/import/"
                            data_ga_name: "for migrating other projects to GitLab"
                            data_ga_location: "corps"
                            icon:
                                name: "digital-transformation"
                                alt: "Transformation numérique Icône"
    -
        name: "get-started-resources"
        data:
            title: "Plus de ressources"
            cards:
                -
                    title: "GitLab Docs"
                    icon:
                        name: "docs-alt"
                        alt: "Icône Gitlab Docs"
                    description: "Documentation pour GitLab Community Edition, GitLab Enterprise Edition, Omnibus GitLab et GitLab Runner."
                    link:
                        text: "Visiter les docs"
                        url: "https://docs.gitlab.com/"
                -
                    title: "Portail des développeurs"
                    icon:
                        name: "code"
                        alt: "Icône du portail des développeurs"
                    description: "Documentation pour les contributeurs au projet GitLab - informations sur notre base de code, nos API, nos webhooks, notre système de conception, notre cadre d'interface utilisateur, et plus encore !"
                    link:
                        text: "Voir le portail des développeurs"
                        url: "https://developer.gitlab.com/"
                -
                    title: "Blog"
                    icon:
                        name: "doc-pencil-alt"
                        alt: "Icône de blog"
                    description: "Visitez le blog de GitLab pour en savoir plus sur les versions, les applications, les contributions, les nouvelles, les événements, et plus encore."
                    link:
                        text: "Lire le blog"
                        url: "/blog/"
