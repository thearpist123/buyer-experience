---
  title:  Software. Schneller.
  description: GitLab vereinfacht Ihre DevSecOps, damit Sie sich auf das Wesentliche konzentrieren können. Mehr erfahren!
  image_title: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      Der schnellere Weg von der Idee zur Software
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: Software-schnelleres hero Bild
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&badge=0&autopause=0&player_id=0&app_id=58479
      text: Was ist GitLab?
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link zur Siemens Kunden-Fallstudie
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: Link to UBS customer case study
  featured_content:
    col_size: 4
    header: "Gemeinsam besser: Kunden stellen Software mit GitLab schneller bereit"
    case_studies:
      - header: "Nasdaq: Schneller, nahtloser Übergang zur Cloud"
        description: |
          Nasdaq hat die Vision, zu 100 % in die Cloud zu wechseln. Um dieses Ziel zu erreichen, arbeiten sie mit GitLab zusammen.
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: Video ansehen
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: Erzielen einer 5 x schnelleren Bereitstellungszeit
        description: |
          Hackerone hat mit GitLab Ultimate die Pipeline-Zeit, die Bereitstellungsgeschwindigkeit und die Effizienz der Entwickler verbessert.
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Person, die an einem Computer mit Code arbeitet - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: Mehr erfahren
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: Release-Funktionen wurden 144 x schneller
        description: |
          Airbus Intelligence verbesserte seine Arbeitsabläufe und Codequalität mit Single Application CI.
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Flugzeugflügel im Flug - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: Geschichte lesen
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: Alle wichtigen DevSecOps-Tools in einer umfassenden Plattform
    link:
      text: Mehr erfahren
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: Bessere Einblicke
        description: End-to-End-Transparenz über den gesamten Lebenszyklus der Softwareentwicklung.
        icon: case-study-alt
      - header: Höhere Effizienz
        description: Eingebaute Unterstützung für Automatisierung und Integration mit Drittanbieterdiensten.
        icon: principles
      - header: Verbesserte Zusammenarbeit
        description: Ein Workflow, der Entwickler-, Sicherheits- und Betriebsteams zusammenführt.
        icon: roles
      - header: Schnellere Amortisation
        description: Kontinuierliche Verbesserung durch beschleunigte Feedbackschleifen.
        icon: verification
  by_industry_case_studies:
    title: DevSecOps-Ressourcen erkunden
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2023 Global DevSecOps Berichtreihe
        subtitle: Lesen Sie, was wir von mehr als 5.000 DevSecOps-Experten über den aktuellen Stand der Softwareentwicklung, -sicherheit und -abläufe erfahren haben.
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: Bericht lesen
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: 'Rhythmus ist alles: 10x Ingenieurbüros für 10x Ingenieure.'
        subtitle: Der CEO und Mitbegründer von GitLab, Sid Sijbrandij, über die Bedeutung des Rhythmus in technischen Unternehmen.
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: Bild von Menschen, die Marathon laufen
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: Blogbeitrag lesen
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon