title: "Die DevSecOps-Plattform"
description: "Von der Planung bis zur Produktion: Bringen Sie Teams in einer einzigen Anwendung zusammen. Stellen Sie sicheren Code effizienter bereit, um schneller Mehrwert zu liefern."
schema_org: >
  "{"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab ist die DevOps-Plattform, die es Unternehmen ermöglicht, die Gesamtrendite aus der Softwareentwicklung zu maximieren, indem sie Software schneller und effizienter bereitstellen und gleichzeitig Sicherheit und Konformität stärken. Mit GitLab kann jedes Team in Ihrem Unternehmen gemeinsam Software planen, erstellen, sichern und bereitstellen, um Geschäftsergebnisse schneller, mit vollständiger Transparenz, Konsistenz und Rückverfolgbarkeit zu erzielen. GitLab ist ein offenes Kernunternehmen, das Software für den Softwareentwicklungslebenszyklus mit 30 Millionen geschätzten registrierten Benutzern und mehr als 1 Million aktiven Lizenzbenutzern entwickelt und über eine aktive Community von mehr als 2.500 Mitwirkenden verfügt. GitLab teilt offen mehr Informationen als die meisten Unternehmen und ist standardmäßig öffentlich. Dies bedeutet, dass unsere Projekte, Strategie, Richtung und Metriken offen diskutiert werden und auf unserer Website zu finden sind. Unsere Werte sind Collaboration (Zusammenarbeit), Results (Ergebnisse), Efficiency (Effizienz), Diversity (Vielfalt), Inclusion (Inklusion) & Belonging (Zugehörigkeit), Iteration und Transparancy (Transparenz) (CREDIT). Diese Werte prägen unsere Kultur.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Unsere Mission ist es, alle kreativen Arbeiten vom Read-Only zum Read-Write zu transformieren, damit jeder etwas beitragen kann.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparably, Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing – Challenger, DevOps Dozen Award für Best DevOps Solution Provider 2019, 451 Firestarter Award von 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}"
cta_block: 
    # Remove comments and add content to add pill badge to Homepage hero
    badge:
      text: "Lernen Sie GitLab Duo kennen: KI-gestützte Workflows"
      link: "/gitlab-duo/"
      data_ga_name: "gitlab duo pill"
      data_ga_location: "hero"
    title: "Software. Schneller."
    subtitle: "GitLab ist die umfassendste <br>KI-gestützte DevSecOps-Plattform."
    primary_button:
      text: "Kostenlose Testversion anfordern"
      link: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
      data_ga_name: "free trial"
      data_ga_location: "hero"
    secondary_button:
      text: "Was ist GitLab?"
      modal:
        video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
        video_title: Was ist GitLab?
      data_ga_name: "watch demo"
      data_ga_location: "hero"
    image_copy: "DevSecOps-Plattform"
    images: 
        - id: 1
          image: "/nuxt-images/home/hero/developer-productivity-img.svg"
          alt: "Markenimage von GitLab-Boards"
        - id: 2
          image: "/nuxt-images/home/hero/security-img.svg"
          alt: "Markenimage des GitLab-Produkts"
        - id: 3
          image: "/nuxt-images/home/hero/value-stream-img.svg"
          alt: "Markenimage der GitLab-Roadmap"
customer_logos_block:
    text: "Diese Unternehmen vertrauen uns"
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: "Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: "Link to Goldman Sachs customer case study"
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/software-faster/airbus-logo.png"
        link_label: "Link to Airbus customer case study"
        alt: "Airbus logo"
        url: "/customers/airbus/"
      - image_url: "/nuxt-images/logos/lockheed-martin.png"
        link_label: "Link to Lockheed Martin customer case study"
        alt: "Lockheed martin logo"
        url: /customers/lockheed-martin/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: "Link to Nvidia customer case study"
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: "Link to blogpost How UBS created their own DevSecOps platform using GitLab"
        alt: "UBS logo"
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
featured_content:
    cards:
      - header: "Erste Schritte mit GitLab"
        description: "Sie sind neu bei GitLab und wissen nicht, wo Sie anfangen sollen? Wir führen Sie durch die Grundlagen, damit Sie sehen, was Sie erwartet."
        link:
          href: /get-started/
          text: "Ressourcen erkunden"
          data_ga_name: "get started"
          data_ga_location: "home content block"
        icon: cog-check
      - header: "Globaler DevSecOps-Bericht 2023: Sicherheit ohne Opfer"
        description: "Wir haben mehr als 5.000 DevSecOps-Experten gebeten, ihre Erfolge und Herausforderungen bei der Erstellung sichererer Anwendungen zu teilen."
        link:
          href: "/developer-survey/"
          text: "Bericht lesen"
          data_ga_name: "2023 global devsecops report"
          data_ga_location: "home content block"
        icon: doc-pencil-alt
      - header: "Die KI-gestützten Codevorschläge von GitLab"
        description: "Reduzieren Sie die kognitive Belastung und steigern Sie die Effizienz mithilfe generativer KI, die während der Eingabe Code vorschlägt."
        link:
          href: "/solutions/code-suggestions/"
          text: "Mehr erfahren"
          data_ga_name: "code suggestions"
          data_ga_location: home content block
        icon: ai-code-suggestions
resource_card_block:
    column_size: 4
    header_text: Ressourcen
    header_cta_text: Alle Ressourcen anzeigen
    header_cta_href: /resources/
    header_cta_ga_name: view all resources
    header_cta_ga_location: body
    cards:
      - icon:
          name: ebook-alt
          variant: marketing
          alt: Ebook Icon
        event_type: "E-Book"
        header: "Erste Schritte mit DevOps"
        link_text: "Mehr lesen"
        href: "https://page.gitlab.com/resources-ebook-beginners-guide-devops.html"
        image: "/nuxt-images/home/resources/Devops.png"
        alt: Winding path
        data_ga_name: "Beginner's Guide to DevOps"
        data_ga_location: "body"
      - icon:
          name: topics
          variant: marketing
          alt: Topics Icon
        event_type: "Themen"
        header: "Was ist CI/CD?"
        link_text: "Mehr lesen"
        href: "/topics/ci-cd/"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
        alt: Text reading CI/CD on a gradient
        data_ga_name: "What is CI/CD?"
        data_ga_location: "body"
      - icon:
          name: report-alt
          variant: marketing
          alt: Report Icon
        event_type: "Bericht"
        header: "2023 Global DevSecOps Berichtreihe"
        link_text: "Mehr lesen"
        href: "/developer-survey/"
        image: "/nuxt-images/home/devsecops-survey-thumbnail.jpeg"
        alt: A globe outlined with the DevSecOps loop with the text What's next in DevSecOps
        data_ga_name: "2023 DevSecOps Survey"
        data_ga_location: "body"
      - icon:
          name: blog-alt
          variant: marketing
          alt: Blog Icon
        event_type: "Blogbeitrag"
        header: "Reihe: KI/ML für DevSecOps"
        description: Lesen Sie unsere Blog-Reihe, in der wir unsere Integration von KI/ML während des gesamten Softwareentwicklungslebenszyklus detailliert beschreiben.
        link_text: "Mehr lesen"
        href: "https://about.gitlab.com/blog/2023/04/24/ai-ml-in-devsecops-series/"
        image: "/nuxt-images/home/resources/ai-experiment-stars.png"
        alt: ai-experiment-stars
        data_ga_name: "ai/ml in devsecops series"
        data_ga_location: "body"
      - icon:
          name: partners
          variant: marketing
          alt: Partners Icon
        event_type: "Partner"
        header: "Entdecken Sie die Vorteile von GitLab auf AWS"
        link_text: "Mehr lesen"
        href: "/partners/technology-partners/aws/"
        image: "/nuxt-images/home/resources/AWS.png"
        alt: "Text reading AWS on a gradient"
        data_ga_name: "Discover the benefits of GitLab on AWS"
        data_ga_location: "body"
      - icon:
          name: announce-release
          variant: marketing
          alt: Announce Release Icon
          hex_color: '#FFF'
        event_type: "Release"
        header: "GitLab 16.1 mit völlig neuer Navigation veröffentlicht"
        link_text: "Mehr lesen"
        href: "/releases/2023/06/22/gitlab-16-1-released/"
        alt: "GitLab version number on a gradient"
        image: "/nuxt-images/home/resources/16_1.png"
        data_ga_name: "GitLab 16.1 released with all new navigation"
        data_ga_location: "body"

quotes_carousel_block:
    header: "Teams werden besser durch GitLab"
    header_link:
      url: "/customers/"
      text: "Lesen Sie unsere Fallstudien"
      data_ga_name: "Read our case studies"
      data_ga_location: body
    quotes:
      - main_img:
          url: "/nuxt-images/home/HohnAlan.jpg"
          alt: "Picture of Alan Hohn"
        quote: "Durch den Wechsel zu GitLab und die Automatisierung der Bereitstellung können unsere Teams statt monatlichen oder wöchentlichen Lieferungen jetzt täglich oder mehrmals täglich Ergebnisse liefern."
        author: "Alan Hohn"
        logo: 
          url: /nuxt-images/logos/lockheed-martin.png
          alt: ''
        role: "Director of Software Strategy, Lockheed Martin"
        statistic_samples:
          - data:
              highlight: "80x"
              subtitle: "schnellere CI-Pipeline-Builds"
          - data:
              highlight: "90%"
              subtitle: "weniger Zeitaufwand für die Systemwartung"
        url: /customers/lockheed-martin/
        header: "Lockheed Martin spart mit GitLab Zeit, Geld und Technik"
        data_ga_name: "lockheed martin case study"
        data_ga_location: "home case studies"
        cta_text: "Fallstudie ansehen"
      - main_img:
          url: "/nuxt-images/home/JasonManoharan.png"
          alt: "Picture of Jason Monoharan"
        quote: "Die Vision, die GitLab in Bezug auf die Verknüpfung von Strategie mit Umfang und Code hat, ist sehr mächtig. Ich schätze die kontinuierlichen Investitionen in die Plattform."
        author: "Jason Monoharan"
        logo: 
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: ''
        role: "VP of Technology, Iron Mountain"
        statistic_samples:
          - data:
              highlight: "150Tsd.$"
              subtitle: "ungefähre Kosteneinsparung pro Jahr"
          - data:
              highlight: "20Stunden"
              subtitle: "eingesparte Einarbeitungszeit pro Projekt"
        url: "customers/iron-mountain/"
        header: "Iron Mountain treibt die DevOps-Entwicklung mit GitLab Ultimate voran"
        data_ga_name: iron mountain
        data_ga_location: home case studies
        cta_text: "Fallstudie ansehen"
      - main_img:
          url: "/nuxt-images/home/EvanO_Connor.png"
          alt: "Picture of Evan O’Connor"
        quote: "GitLabs Engagement für eine Open Source-Community bedeutete, dass wir direkt mit Ingenieuren zusammenarbeiten konnten, um schwierige technische Probleme zu lösen."
        author: "Evan O’Connor"
        logo: 
          url: "/nuxt-images/home/havenTech.png"
          alt: ''
        role:  "Platform Engineering Manager, Haven Technologies"
        statistic_samples:
          - data:
              highlight: "62%"
              subtitle: "der monatlichen Benutzer(innen) haben Jobs zur Geheimniserkennung ausgeführt"
          - data:
              highlight: "66%"
              subtitle: "der monatlichen Benutzer(innen) haben sichere Scanner-Jobs ausgeführt"
        url: /customers/haven-technologies/
        header: "Haven Technologies ist mit GitLab zu Kubernetes migriert"
        data_ga_name: "haven technology"
        data_ga_location: home case studies
        cta_text: "Fallstudie ansehen"
      - main_img:
          url: /nuxt-images/home/RickCarey.png
          alt: "Picture of Rick Carey"
        quote: "Wir haben bei UBS den Ausdruck „alle Entwickler warten mit der gleichen Geschwindigkeit“. Alles, was ihre Wartezeit verkürzt, ist ein Mehrwert. GitLab ermöglicht es uns, diese integrierte Erfahrung zu haben."
        author: "Rick Carey"
        logo: 
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: ''
        role: "Group Chief Technology Officer, UBS"
        statistic_samples:
          - data:
              highlight: "1 Million"
              subtitle: "erfolgreiche Builds in den ersten sechs Monaten"
          - data:
              highlight: "12.000"
              subtitle: aktive GitLab-Benutzer(innen)
        url: "/blog/2021/08/04/ubs-gitlab-devops-platform/"
        header: "UBS hat mithilfe von GitLab eine eigene DevOps-Plattform erstellt"
        data_ga_name: ubs
        data_ga_location: home case studies
        cta_text: "Fallstudie ansehen"
      - main_img:
          url: /nuxt-images/home/LakshmiVenkatrama.png
          alt: "Picture of Lakshmi Venkatraman"
        quote: "Durch GitLab können Teammitglieder auch zwischen verschiedenen Teams optimal zusammenzuarbeiten. Wenn man als Projektmanager ein Projekt oder die Arbeitsbelastung eines Teammitglieds verfolgen kann, lassen sich Verzögerungen bei einem Projekt leichter vermeiden. Wenn ein Projekt abgeschlossen ist, können wir einfach einen Verpackungsprozess automatisieren und die Ergebnisse an den Kunden senden. Mit GitLab befindet sich alles an einem Ort."
        author: Lakshmi Venkatraman
        logo: 
          url: /nuxt-images/home/singleron.svg
          alt: ''
        role: "Projektleiterin, Singleron Biotechnologies"
        url: https://www.youtube.com/watch?v=22nmhrlL-FA
        header: "Dank GitLab kann Singleron auf einer einzigen Plattform zusammenzuarbeiten, um die Patientenversorgung zu verbessern"
        data_ga_name: singleron video
        data_ga_location: home case studies
        cta_text: "Video ansehen"
solutions_block:
    title: Eine Plattform, ein Team
    image: /nuxt-images/home/solutions/solutions-top-down.png
    alt: "Top down image of office"
    description: "Egal, ob Sie mit der Integration weniger Punktlösungen beginnen oder Ihre gesamte Toolchain vereinfachen, jetzt können Sie es als ein Team auf einer Plattform tun. Zusammenarbeit von der Planung bis zur Produktion über eine einzige Plattform mit integrierter Sicherheit."
    subtitle: DevSecOps, wie es sein sollte
    sub_image: /nuxt-images/home/solutions/solutions.png
    solutions:
      - title: Ihre digitale Transformation beschleunigen
        description: Erreichen Sie die Ziele Ihrer digitalen Transformation schneller mit einer DevSecOps-Plattform für Ihr gesamtes Unternehmen.
        icon:
          name: accelerate
          alt: Accelerate Icon
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/digital-transformation/
        data_ga_name: digital transformation
        data_ga_location: body
        image: /nuxt-images/home/solutions/accelerate.png
        alt: "Text bubbles of communicating teams"
      - title: Software schneller bereitstellen
        description: Automatisieren Sie Ihren Softwarebereitstellungsprozess, damit Sie schneller Werte und häufiger Code mit hoher Qualität bereitstellen können.
        icon:
          name: deliver
          alt: Deliver Icon
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/delivery-automation/
        data_ga_name: delivery automation
        data_ga_location: body
        image: /nuxt-images/home/solutions/deliver.png
        alt: "Text bubbles of communicating teams"
      - title: Konformität sicherstellen
        description: Sie vereinfachen die kontinuierliche Konformität Ihrer Software, indem Sie sie auf einer Plattform definieren, erzwingen und Berichte dazu erstellen.
        icon:
          name: ensure
          alt: Ensure Icon
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: continuous software compliance
        data_ga_location: body
        image: /nuxt-images/home/solutions/ensure.png
        alt: "Text bubbles of communicating teams"
      - title: Sicherheit integrieren
        description: Übernehmen Sie DevSecOps-Praktiken mit kontinuierlicher Gewährleistung der Sicherheit Ihrer Software in jeder Phase.
        icon:
          name: shield-check-light
          alt: Shield Check Icon
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/continuous-software-security-assurance/
        data_ga_name: continuous software security assurance
        data_ga_location: body
        image: /nuxt-images/home/solutions/build.png
        alt: "Text bubbles of communicating teams"
      - title: Zusammenarbeit und Sichtbarkeit verbessern
        description: Indem Sie allen eine einzige Plattform für die Zusammenarbeit zur Verfügung stellen, haben Sie Einblick in alles, von der Planung bis zur Produktion.
        icon:
          name: improve
          alt: Improve Icon
          variant: marketing
        link_text: Mehr erfahren
        link_url: /solutions/devops-platform/
        data_ga_name: devops platform
        data_ga_location: body
        image: /nuxt-images/home/solutions/improve.png
        alt: "Text bubbles of communicating teams"
badges:
    header: GitLab ist die DevSecOps-Plattform
    tabs :
      - tab_name: Führend in DevSecOps
        tab_id: leaders
        tab_icon:  /nuxt-images/icons/ribbon-check-transparent.svg
        copy: |
          **Unsere Benutzer(innen) haben gesprochen.**
          GitLab rangiert in allen DevSecOps-Kategorien als G2-Leader
        cta:
          url: /analysts
          cta_text: Mehr erfahren
          data_ga_name: analysts page
          data_ga_location: leaders in devops tab - badge section
        badge_images:
          - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
            alt: G2 Enterprise Leader – Herbst 2022
          - src: /nuxt-images/badges/midmarketleader_fall2022.svg
            alt: G2 Mid-Market Leader – Herbst 2022
          - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
            alt: G2 Small Business Leader – Herbst 2022
          - src: /nuxt-images/badges/bestresults_fall2022.svg
            alt: G2 Best Results – Herbst 2022
          - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
            alt: G2 Best Relationship Enterprise – Herbst 2022
          - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
            alt: G2 Best Relationship Mid-Market – Herbst 2022
          - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
            alt: G2 Easiest To Do Business With Mid-Market – Herbst 2022
          - src: /nuxt-images/badges/bestusability_fall2022.svg
            alt: G2 Best Usability – Herbst 2022
      - tab_name: Was Branchenanalysten sagen
        tab_id: research
        tab_icon: /nuxt-images/icons/doc-pencil-transparent.svg
        copy: |
          **Was Branchenanalysten über GitLab sagen**
        cta:
          url: /analysts
          cta_text: Mehr erfahren
          data_ga_name: analysts page
          data_ga_location: industry analyst research tab - badge section
        analysts:
          - logo: /nuxt-images/logos/forrester-logo.svg
            text: 'Die Forrester Wave™ 2019: Cloud-native Tools zur kontinuierlichen Integration'
            link:
              url: https://about.gitlab.com/analysts/forrester-cloudci19/
              data_ga_name: Forrester Cloud-Native Continuous Integration Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant für agile Planungstools für Unternehmen'
            link:
              url: https://about.gitlab.com/analysts/gartner-eapt21/
              data_ga_name: Gartner Magic Quadrant for Enterprise Agile Planning Tools
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2022 Gartner® Magic Quadrant™ für Anwendungssicherheitstests'
            link:
              url: https://about.gitlab.com/analysts/gartner-ast22/
              data_ga_name: Gartner Magic Quadrant for Application Security Testing
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: '2021 Gartner® Market Guide für Value Stream-Delivery-Plattformen'
            link:
              url: https://about.gitlab.com/analysts/gartner-vsdp21/
              data_ga_name: Gartner Market Guide for Value Stream Delivery Platforms
top-banner:
    text: Die DevSecOps-Umfrage 2022 mit Erkenntnissen von 5.000 DevOps-Profis ist da.
    link:
      text: Umfrage ansehen
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right