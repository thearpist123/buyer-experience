---
title: Handling Sensitive Information With GitLab Support
description: Some common questions and answers about how customers and GitLab support should handle sensitive information
support-hero:
  data:
    title: Handling Sensitive Information With GitLab Support
side_menu:
  anchors:
    text: 'On this page'
    data:
      - text: 'What is sensitive information?'
        href: '#what-is-sensitive-information'                               
      - text: "Where do I need to be careful about divulging sensitive information in interacting with GitLab Support?"
        href: "#where-do-i-need-to-be-careful-about-divulging-sensitive-information-in-interacting-with-gitlab-support"
      - text: "How do I scrub sensitive information?"
        href: "#how-do-i-scrub-sensitive-information"
      - text: "What do I do if I suspect that I have divulged sensitive information in a Support Ticket?"
        href: "#what-do-i-do-if-i-suspect-that-i-have-divulged-sensitive-information-in-a-support-ticket"                                          
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - subtitle:
        text: What is sensitive information?
        id: what-is-sensitive-information
      text: |
        Sensitive information is any information that your organization deems sensitive. Commonly:

        - credentials
        - passwords
        - tokens
        - keys
        - secrets

        These would all be generally be considered sensitive, but your organization likely has its own data classification policies. Your security team might also consider hostnames, IP addresses or other environmental information to be sensitive.
    - subtitle:
        text: Where do I need to be careful about divulging sensitive information in interacting with GitLab Support?
        id: where-do-i-need-to-be-careful-about-divulging-sensitive-information-in-interacting-with-gitlab-support
      text: |
        Depending on your configuration, this type of sensitive information can occasionally be in GitLab's configuration files and its components.
        Specifically (but not exhaustively):
        
          - `gitlab.rb` for the GitLab application
          - `config.toml` for GitLab Runners
          - `.gitlab-ci.yml` for GitLab projects

        All *could* contain information your organization might consider sensitive.
    - subtitle:
        text: How do I scrub sensitive information? 
        id: how-do-i-scrub-sensitive-information
      text: |
        Please do not submit or share files without verifying that the contents have been scrubbed of secrets and private information.
        If you or your company/organization is not comfortable sharing it, do not submit it - please.
        To sanitize a `gitlab.rb` file, review and redact secrets manually, or use a command line utility to automate this process, for example [our `gitlab.rb` sanitizer tool](https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer).
        You can also scrub sensitive data using the grep command line utility.

        > `grep -Ev "password|_key|token|secret|app_id|bind_dn|^$|^\s*#" \`
        > `/etc/gitlab/gitlab.rb > /tmp/_clean.gitlab.rb`

        In addition to configuration files, [log files](https://docs.gitlab.com/ee/administration/logs.html) and traces may include details like internal IP addresses and URLs.
        To redact details in certain log files, we suggest replacing the sensitive information with placeholder text. There are a number of ways to quickly do a find and replace operation, including common command line utilities like `sed`:

        > `sed -i 's/secret-project/project1/g' attachment.log`
        > `sed -i 's/private.com/app1.com/g' attachment.log`
        > `sed -i 's/10.10.0.2/ip1/g' attachment.log`

    - subtitle:
        text: What do I do if I suspect that I have divulged sensitive information in a Support Ticket?
        id: what-do-i-do-if-i-suspect-that-i-have-divulged-sensitive-information-in-a-support-ticket
      text: |
        If secrets were accidentally shared or attached to a Support ticket, please notify GitLab Support immediately to ensure this data is redacted and deleted.
        Conversely, if a Support Engineer suspects that secrets were accidentally submitted to a Support ticket, we will bring this to your attention and take action to [remove any sensitive information](/handbook/support/providing_excellent_customer_service.html#removing-information-from-tickets).
