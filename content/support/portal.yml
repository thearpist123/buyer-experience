---
title: Support Portal
description: Support Portal
support-hero:
  data:
    title: Support Portal
side_menu:
  anchors:
    text: 'On this page'
    data:
    - text: 'Account management'
      href: '#account-management'
      nodes:
      - text: 'Creating a support portal account'
        href: '#creating-a-support-portal-account'  
      - text: 'Logging into your support portal account'
        href: '#logging-into-your-support-portal-account'
      - text: 'Logging out of your support portal account'
        href: '#logging-out-of-your-support-portal-account'
      - text: 'Reset your password'
        href: '#reset-your-password'
      - text: 'Change your password'
        href: '#change-your-password'
      - text: 'Change your name'
        href: '#change-your-name'
      - text: 'Change your profile picture'
        href: '#change-your-profile-picture'
      - text: 'Change your email'
        href: '#change-your-email'
    - text: 'Ticket management'
      href: '#ticket-management'
      nodes:
      - text: 'Creating a ticket'
        href: '#creating-a-ticket'
        nodes:
        - text: 'Creating emergency tickets'
          href: '#creating-emergency-tickets'
      - text: 'Viewing your tickets'
        href: '#viewing-your-tickets'
        nodes:
        - text: 'Filtering your tickets'
          href: '#filtering-your-tickets'
      - text: "Viewing tickets you are CC'd on"
        href: '#viewing-tickets-you-are-ccd-on'
      - text: "Viewing your organization's tickets"
        href: '#Viewing-your-organizations-tickets'
        nodes:
        - text: "Getting notified about your organization's tickets"
          href: '#getting-notified-about-your-organizations-tickets'
      - text: 'Updating a ticket'
        href: '#updating-a-ticket'
        nodes:
        - text: 'Adding a CC on your ticket'
          href: '#adding-a-cc-on-your-ticket'
        - text: 'Creating a follow-up ticket'
          href: '#creating-a-follow-up-ticket'
      - text: 'Solving a ticket'
        href: '#solving-a-ticket'
    - text: 'Information on ticket flow'
      href: '#information-on-ticket-flow'
      nodes:
      - text: 'We use auto-responders for efficiency'
        href: '#we-use-auto-responders-for-efficiency'
      - text: 'We automatically modify the ticket status based on unresponsiveness timelines'
        href: '#we-automatically-modify-the-ticket-status-based-on-unresponsiveness-timelines'
    - text: 'Troubleshooting support portal issues'
      href: '#troubleshooting-support-portal-issues'
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - header: Account Management
      id: account-management
      text:
    - subtitle:
        text: Creating a support portal account
        id: creating-a-support-portal-account
      text: |
        **NOTE** If you have filed a ticket without signing into a support portal account previously or another associated contact had requested you be added to your organization, an account was already created for you. You will need to reset your password first to be able to login. See [reset your password](#reset-your-password) for more details.

        <br />

        To create a support portal account, you can do it one of three ways:

        1. Submit a ticket without logging into the support portal. Doing so will result in an account being automatically created for you. You will need to reset your password first to be able to login if your account is created in this way. See [reset your password](#reset-your-password) for more details.
        1. Have an associated contact request you be added to your organization (via a Support Operations ticket). This will result in an account being automatically created for you. You will need to reset your password first to be able to login if your account is created in this way. See [reset your password](#reset-your-password) for more details.
        1. Manually create the account. You do this by:
           1. Click the [Sign in](https://gitlab.zendesk.com/auth/v2/login/signin) button at the top right of the page
           1. Click the [Sign up](https://gitlab.zendesk.com/auth/v2/login/registration) link towards the bottom left of the page.
           1. Enter your full name and email address, then click the blue `Sign up` button.

        If doing this manually (option 3 from above), there are a few results that can occur:

        * An account already exists with the email you tried to use. In this case, a password reset email is sent to the account's email address.
        * A new account is created for you. An email will be sent to your account's email for setting up your password and confirming your accounts' email.

        Both results will appear the _exact_ same on the portal, so you will need to check your account's email to be sure which result occurred.
    - subtitle:
        text: Logging into your support portal account
        id: logging-into-your-support-portal-account
      text: |
        1. Click the [Sign in](https://gitlab.zendesk.com/auth/v2/login/signin) button at the top right of the page
        1. Determine the method to use for signing in:
           * If you have filed a ticket without signing into a support portal account previously or another associated contact had requested you be added to your organization, an account was already created for you. You will need to reset your password first to be able to login. See [reset your password](#reset-your-password) for more details.
           * If you have a Google or Twitter account using the same email as your support portal account, you can click the corresponding Oauth button on the left side.
           * Otherwise, you'd enter the email and password for your account on the right side login box and then click the blue `Sign in` button.
    - subtitle:
        text: Logging out of your support portal account
        id: logging-out-of-your-support-portal-account
      text: |
        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the [Sign out](https://support.gitlab.com/access/logout) link
    - subtitle:
        text: Reset your password
        id: reset-your-password
      text: |
        To reset your support portal account's password:

        1. Click the [Sign in](https://gitlab.zendesk.com/auth/v2/login/signin) button at the top right of the page
        1. Click the [Forgot password?](https://gitlab.zendesk.com/auth/v2/login/password_reset) link on the right side of the page (below the Email/Password input boxes)
        1. Enter your support portal account's email address and click the blue `Submit` button.

        This will send password reset instructions to your email inbox.
    - subtitle:
        text: Change your password
        id: change-your-password
      text: |
        To change the email used for your support portal account:

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the `Change password` link (some browsers may not show it as a link). This will cause a modal to popup showing your profile information.
        1. Enter the password you used to login under `Current password`.
        1. Enter your the new password you wish to use under `New password`.
        1. Re-enter your the new password you wish to use under `Confirm password`.
        1. Click the black `Save` button. (**NOTE** this will sign you out of the account).

        An alterative to doing this is using the information within [Reset your password](#reset-your-password), which will accomplish the same end-result.
    - subtitle:
        text: Change your name
        id: change-your-name
      text: |
        To change the name used for your support portal account:

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the `Profile` link (some browsers may not show it as a link). This will cause a modal to popup showing your profile information.
        1. Change the name showing in the modal and click the black `OK` button.
    - subtitle:
        text: Change your profile picture
        id: change-your-profile-picture
      text: |
        To change the profile picture used for your support portal account:

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the `Profile` link (some browsers may not show it as a link). This will cause a modal to popup showing your profile information.
        1. Click the `Change photo` button (below the Name input box). This will open the file browser your for browser, allowing you to select an image file to use for your profile picture.
        1. Wait for the image to render on the preview bubble (to the left of the `Change photo` button).
        1. Click the black `OK` button.
    - subtitle:
        text: Change your email
        id: change-your-email
      text: |
        **NOTE** Changing your email might result in you being de-associated from your organization depending on your organization's setup. You should speak with your organization to ensure that changing your email is acceptable and will not cause issues.

        <br />

        To change the email used for your support portal account:

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the `Profile` link (some browsers may not show it as a link). This will cause a modal to popup showing your profile information.
        1. Change the email showing in the modal and click the black `OK` button.

        This will only work if the new email is available for use. If another account is using the new email address, you will see an error message of `This email address is already in use by another profile.` after clicking the black `OK` button.
    - header: Ticket management
      id: ticket-management
      text:
    - subtitle:
        text: Creating a ticket
        id: creating-a-ticket
      text: |
        **NOTE** This requires using the support portal to do. We do not accept tickets created via email, with the exception of billing, account receivable, and emergency emails.

        <br />

        To create a ticket via the support portal, you will want to navigate to the [Submit a ticket](https://support.gitlab.com/hc/en-us/requests/new) page. The location of the button for this can be found in multiple locations:

        * At the top right on all support portal pages
        * Below the search bar on the [main page](https://support.gitlab.com/hc/en-us)
        * As one of the 6 block items on the [main page](https://support.gitlab.com/hc/en-us) (the 6th one on the bottom-right)

        This page will then ask you to choose the reason you are reaching our to us. The answer on this question will determine the _form_ that is used for your ticket submission, which will be used to route your ticket to the correct team.

        This is a brief overview of the options and what they mean:

        | Form | Team | What it is for |
        |------|------|----------------|
        | Support for GitLab.com (SaaS) | Support | Technical support tickets for customers using gitlab.com (not relating to user account specific issues) |
        | GitLab.com (SaaS) user accounts and login issues | Support | Technical support tickets for customers using gitlab.com relating to user account specific issues |
        | Support for a self-managed GitLab instance | Support | Technical support tickets for customers using self-managed instances |
        | Support for GitLab Dedicated instances | Support | Technical support tickets for customers using GitLab Dedicated |
        | Licensing and renewals problems | Support | For matters relating to Customers Portal, subscriptions, license issues, credit card errors, etc. |
        | Billing inquiries/refunds | Billing | For matters relating to refunds, invoices, etc. |
        | Level Up | Learning and Development | For tickets relating to [GitLab LevelUp](https://levelup.gitlab.com) |
        | Support for open partners | Support | For Open partners to file technical tickets |
        | Support for select partners | Support | For Select partners to file technical tickets |
        | Support for alliance partners | Support | For Alliance partners to file technical tickets |
        | Support portal related matters | Support Operations | For issues relating to ticket management, contact management, support portal usage, etc. |
        | Support for JiHu | JiHu team | For JiHu specific customers |

        On each form, you will want to fill out as much informaiton as is possible. The more detailed and thorough of a description given to the various GitLab teams, the quicker and more efficient the ticket will be resolved.

        <br />

        **NOTE** If you are logged into the support portal, you can add a CC on your ticket at the time of submission. The option will not be available if you not signed into the support portal.
    - subtext:
        text: Creating emergency tickets
        id: creating-emergency-tickets
      text: |
        To create an emergency ticket, you must create a **brand new email** to the emergency email address. Please avoid adding any CC's, BCC's, or other emails in the `To` field of your email client. It should be a brand new, clean email to _only_ the emergency address. Creating an email that contains any of the aforementioned items could result in the emergency not filing correctly.

        If you do not have the emergency email address, please consider one of the following routes to obtain it:

        * Speak to your account manager
        * File a [support operations ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001801419&tf_subject=Please%20provide%20me%20the%20emergency%20email%20address&tf_360020614540=support_ops_problem_type_other).  Our team would be more than happy to provide it to you (pending you are associated to an organization with an active, paid subscription).
    - subtitle:
       text: Viewing your tickets
       id: viewing-your-tickets
      text: |
        **NOTE** This requires logging into the support portal to do.

        <br />

        To view your tickets:

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the [Requests](https://support.gitlab.com/hc/en-us/requests) link

        This will bring up a list of your requests. You can also just use the [Requests link](https://support.gitlab.com/hc/en-us/requests) to get to the same page.
    - subtext:
        text: Filtering your tickets
        id: filtering-your-tickets
      text: |
        You can use various filters to limit the number of tickets showing. This is done by using the search bar or dropdown menu (to the right of the searchbar).

        Some common filters you might find useful:

        * [Only show tickets waiting on an agent to reply](https://support.gitlab.com/hc/en-us/requests?utf8=%E2%9C%93&query=&status=open)
        * [Only show tickets waiting for your reply](https://support.gitlab.com/hc/en-us/requests?utf8=%E2%9C%93&query=&status=answered)
        * [Only show solved tickets](https://support.gitlab.com/hc/en-us/requests?utf8=%E2%9C%93&query=&status=solved)
    - subtitle:
       text: Viewing tickets you are CC'd on
       id: viewing-tickets-you-are-ccd-on
      text: |
        **NOTE** This requires logging into the support portal to do.

        <br />

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the [Requests](https://support.gitlab.com/hc/en-us/requests) link
        1. Click the [Requests I'm CC'd on](https://support.gitlab.com/hc/en-us/requests/ccd) link

        This will bring up a list of your requests you are CC'd on. You can also just use the [Requests I'm CC'd on link](https://support.gitlab.com/hc/en-us/requests/ccd) to get to the same page.
    - subtitle:
       text: Viewing your organization's tickets
       id: viewing-your-organizations-tickets
      text: |
        **NOTE** This requires logging into the support portal to do. It also requires a [shared organization](managing-support-contacts#shared-organizations) has been setup for your organization. If that has not been done, the link will not appear in the support portal.

        <br />

        1. [Sign in](#logging-into-your-support-portal-account) to your account.
        1. Click your profile link in the top right of the page (shows your profile picture and account name).
        1. Click the [Requests](https://support.gitlab.com/hc/en-us/requests) link
        1. Click the [Organization requests](https://support.gitlab.com/hc/en-us/requests/organization) link

        This will bring up a list of your requests you are CC'd on. You can also just use the [Organization requests link](https://support.gitlab.com/hc/en-us/requests/organization) to get to the same page.
    - subtext:
        text: Getting notified about your organization's tickets
        id: getting-notified-about-your-organizations-tickets
      text: |
        If you wish to be notified about your organization's tickets (and your organization has a [shared organization](managing-support-contacts#shared-organizations) setup, you can click the `Follow` button to the right of the search bar on the [Organization requests page](https://support.gitlab.com/hc/en-us/requests/organization).

        To turn this off, simple click the `Unfollow` button to the right of the search bar on the [Organization requests page](https://support.gitlab.com/hc/en-us/requests/organization).
    - subtitle:
        text: Updating a ticket
        id: updating-a-ticket
      text: |
        **NOTE** The support portal displays the first comment of the ticket at the top of the page, and the most recent comment at the bottom of the page.

        <br />

        There are currently two ways to update a _non-closed_ ticket:

        * Via email
          * This is done by replying to the latest notification email you received for
            the ticket.
        * Via the support portal
          * This is done by navigating to the ticket in question via the support portal,
            going to the very bottom of the page, clicking the `Add to conversation`
            field, typing out your response, and then clicking the `Submit` button.
    - subtext:
        text: Adding additional participants (CCs) to your ticket
        id: adding-a-cc-on-your-ticket
      text: |
        **NOTE** This requires logging into the support portal to do. It cannot be done via email. Requests to add additional participants made through the ticket interaction will not be actioned on.

        <br />

        When updating a ticket, you can add additional participants (CCs) on your ticket. This will ensure the CC'd person gets update notifications for the ticket (and can also reply on said ticket).

        <br />

        To do this, you will have to post an update on the ticket itself (meaning a comment must be made). Right above the comment text box is the CC box, which will allow you to type out email addresses to be CC'd on the ticket. To enter the email, the system waits for a space delimeter. So add the emails of a@abc.com, b@abc.com, and c@abc.com, you would enter

        <br />

        `a@abc.com b@abc.com c@abc.com`

        <br />

        into the field. You can remove an email before you submit the update by clicking the `x` to the right of the email address.

        <br />

        Once a CC is added, it cannot be removed via the support portal. You will need to make a comment on the ticket requesting the GitLab agent completely remove the email as a CC on the ticket.
    - subtext:
        text: Creating a follow-up ticket
        id: creating-a-follow-up-ticket
      text: |
        **NOTE** This requires logging into the support portal to do.

        <br />

        To create a follow-up ticket for a previously solved/closed ticket, navigate to the ticket in question, scroll to the last comment on the ticket, and click the `create a follow-up` link. This will send you to the ticket creation page, copying over the fields from the original ticket. It will also pre-populate the ticket's description with a message about it being a follow-up to the original ticket.
    - subtitle:
        text: Solving a ticket
        id: solving-a-ticket
      text: |
        **NOTE** This requires logging into the support portal to do.

        <br />

        In very specific situations, you are able to mark your ticket as solved. To be able to do this:

        * The ticket must be assigned to a GitLab Agent
        * The ticket's type may not be `problem`
        * You must be the requester on the ticket (i.e. you cannot mark other members of your organization's tickets as solved).

        If all of these criteria are hit, you can mark the ticket as solved. To do this:

        1. Navigate the to ticket in question
        1. Go to the bottom of the page for your ticket
        1. Click the white `Mark as solved` button

        If you wish to mark the ticket as solved _and_ update the ticket with a reply, you would type out your reply in the text box (bottom of the ticket) and then click the white `Mark as solved & Submit` button.
    - header: Information on ticket flow
      id: information-on-ticket-flow
      text: When it comes to the flow of your ticket, there are some things to keep in mind.
    - subtitle:
        text: We use auto-responders for efficiency
        id: we-use-auto-responders-for-efficiency
      text: |
        Where possible, for the sake of efficiency, we use auto-responders from a bot account we control. The goal of these is to try to solve common issues that can be easily solved with a quick reply.

        <br />

        We utilize these to help ensure the fastest resolution to your tickets whenever possible.
    - subtitle:
        text: We automatically modify the ticket status based on unresponsiveness timelines
        id: we-automatically-modify-the-ticket-status-based-on-unresponsiveness-timelines
      text: |
        To ensure tickets do not stay in a pending (awaiting your response) forever, we have setup automated triggers to change the status based on unresponsiveness timelines:

        * If a ticket is pending (awaiting your response) for 7 days, a reminder notification is sent on your ticket reminding you of the current state of the ticket.
        * If a ticket is pending (awaiting your response) for 14 days, we change the  ticket's status to solved (and send out a notification email). At this point, you can still update the ticket to get it moved out of the solved state.
        * If a ticket is in a solved state for 7 days, we automatically close the ticket. At this point, the ticket can no longer be updated. The only way to resume a conversation about the ticket is to [create a follow-up ticket](#creating-a-follow-up-ticket).
    - subtitle:
        text: Ticket responses can be in other languages
        id: language-support
      text: |
        Ticket support is available in the following languages:

        * Chinese (Simplified)
        * English
        * French
        * German
        * Japanese
        * Korean
        * Portuguese
        * Spanish

        While we do not currently provide translated interfaces for our [Support Portal](https://support.gitlab.com/), you can write in your preferred language and we will respond accordingly.

        <br />

        Should you be offered a call, only English is available.

        <br />

        **NOTE:** Any attached media used for ticket resolution must be sent in English.
    - header: Troubleshooting support portal issues
      id: troubleshooting-support-portal-issues
      text: |
        Occasionally, you may find the [Support Portal](https://support.gitlab.com/) not acting as expected. This is often caused by the user’s setup. When encountering this, we recommended the following course of action:

        1. Ensure your browser is allowing third party cookies. These are often vital for the system to work. A general list to allow would be:
           * `[*.]zendesk.com`
           * `[*.]zdassets.com`
           * `[*.]gitlab.com`
        1. Disable all plugins/extensions/addons on the browser.
        1. Disable any themes on the browser.
        1. Clear all cookies and cache on the browser.
        1. Try logging in again to the the Support Portal.
        1. If you are still having issues, obtain the following:
           * the browser’s version
           * the browser's type
           * your operating system (and distro)
           * any other identifying information
           * the complete contents of your Javascript console for your browser
        1. Send all of that to support. If you are unable to create a ticket, then communicate with your Account Manager. The next best place to send the information is via a GitLab.com issue.
