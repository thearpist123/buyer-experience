---
  title: Download and install GitLab
  description: Download, install and maintain your own GitLab instance with various installation packages and downloads for Linux, Kubernetes, Docker, Google Cloud and more.
  side_menu:
    anchors:
      text: "On this page"
      data:
      - text: Official Linux package
        href: "#official-linux-package"
        data_ga_name: official linux package
        data_ga_location: side menu
      - text: Kubernetes Deployments
        href: "#kubernetes-deployments"
        data_ga_name: kubernetes deployments
        data_ga_location: side menu
      - text: Supported cloud
        href: "#supported-cloud"
        data_ga_name: supported cloud
        data_ga_location: side menu
      - text: Other official methods
        href: "#other-official-methods"
        data_ga_name: other official methods
        data_ga_location: side menu
      - text: Community-contributed
        href: "#community-contributed"
        data_ga_name: community contributed
        data_ga_location: side menu
      - text: Already installed?
        href: "#already-installed"
        data_ga_name: already installed
        data_ga_location: side menu
    hyperlinks:
      text: "More on this topic"
      data:
        - text: "Get Free Ultimate Trial"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "side-navigation"
        - text: "Install self-managed"
          href: "/free-trial/"
          data_ga_name: "self managed trial"
          data_ga_location: "side-navigation"
        - text: "Get started with SaaS"
          href: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "saas get started"
          data_ga_location: "side-navigation"
        - text: "Purchase from Marketplaces"
          href: "https://page.gitlab.com/cloud-partner-marketplaces.html"
          data_ga_name: "partner marketplace"
          data_ga_location: "side-navigation"
  header:
    title: Install self-managed GitLab
    subtitle: Try GitLab today. Download, install and maintain your own GitLab instance.
    text: |
      [Trial](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="free trial" data-ga-location="header"} – start your Free Ultimate Trial today

      [Self-managed](/free-trial/){data-ga-name="self managed trial" data-ga-location="header"} – install on your own infrastructure

      [SaaS](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial){data-ga-name="saas get started" data-ga-location="header"}  – get started with our SaaS offering

      [Marketplace](https://page.gitlab.com/cloud-partner-marketplaces.html){data-ga-name="partner marketplace" data-ga-location="header"}  – purchase seamlessly using the cloud marketplace of your choice
  linux:
    title: Official Linux package
    subtitle: Recommended installation method
    text: |
      This is the recommended method for getting started. The Linux packages are mature, scalable, and are used today on GitLab.com. If you need additional flexibility and resilience, we recommend deploying GitLab as described in the [reference architecture documentation.](https://docs.gitlab.com/ee/administration/reference_architectures/index.html){data-ga-name="reference architecture documentation" data-ga-location="linux installation"}

      Linux installation is quicker to install, easier to upgrade and contains features to enhance reliability not found in other methods. Install via a single package (also known as Omnibus) that bundles all the different services and tools required to run GitLab. At least 4 GB of RAM is recommended ([minimum requirements](https://docs.gitlab.com/ee/install/requirements.html){data-ga-name="installation requirements documentation" data-ga-location="linux installation"}).

      Please refer to our Packages repository ([GitLab-ee](https://packages.gitlab.com/gitlab/gitlab-ee) or [GitLab-ce](https://packages.gitlab.com/gitlab/gitlab-ce)) to ensure that the required GitLab version is available for the host OS version.
    cards:
      - title: Ubuntu
        id: ubuntu
        subtext: 18.04 LTS, 20.04 LTS, 22.04 LTS
        icon:
          name: ubuntu-purple
          alt: Ubuntu Icon
        link_text: View install instructions +
        link_url: /install/#ubuntu
        data_ga_name: ubuntu installation documentation
        data_ga_location: linux installation
      - title: Debian
        id: debian
        subtext: 10, 11
        icon:
          name: debian
          alt: Debian Icon
        link_text: View install instructions +
        link_url: /install/#debian
        data_ga_name: debian installation documentation
        data_ga_location: linux installation
      - title: AlmaLinux 8
        id: almalinux-8
        subtext: and RHEL, Oracle, Scientific
        icon:
          name: almalinux
          alt: Almalinux Icon
        link_text: View install instructions +
        link_url: /install/#almalinux-8
        data_ga_name: almalinux 8 installation documentation
        data_ga_location: linux installation
      - title: CentOS 7
        id: centos-7
        subtext: and RHEL, Oracle, Scientific
        icon:
          name: centos
          alt: CentOs Icon
        link_text: View install instructions +
        link_url: /install/#centos-7
        data_ga_name: centos 7 installation documentation
        data_ga_location: linux installation
      - title: OpenSUSE Leap
        id: opensuse-leap
        subtext: OpenSUSE Leap 15.4 and SUSE Linux Enterprise Server 12.2, 12.5
        icon:
          name: opensuse
          alt: OpenSuse Icon
        link_text: View install instructions +
        link_url: /install/#opensuse-leap
        data_ga_name: opensuse leap installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2
        id: amazonlinux-2
        subtext:
        icon:
          name: aws
          alt: AWS Icon
        link_text: View install instructions +
        link_url: /install/#amazonlinux-2
        data_ga_name: amazonlinux 2 installation documentation
        data_ga_location: linux installation
      - title: Amazon Linux 2022
        id: amazonlinux-2022
        subtext:
        icon:
          name: aws
          alt: AWS Icon
        link_text: View install instructions +
        link_url: /install/#amazonlinux-2022
        data_ga_name: amazonlinux 2022 installation documentation
        data_ga_location: linux installation

      - title: Raspberry Pi OS
        id: raspberry-pi-os
        subtext: Bullseye and Buster (32 bit)
        icon:
          name: raspberry-pi
          alt: Raspberry Pi Icon
        link_text: View install instructions +
        link_url: /install/#raspberry-pi-os
        data_ga_name: raspberry pi os installation documentation
        data_ga_location: linux installation
    dropdowns:
      - id: ubuntu
        tip: |
          For Ubuntu 20.04 and 22.04, `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        first_step: 1. Install and configure the necessary dependencies
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
          ```
        postfix_command: |
          ```
           sudo apt-get install -y postfix
          ```
        download_command: |
          ```
           curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
           sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        link_back: ubuntu
      - id: debian
        tip: |
          For Debian 10, `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        dependency_command: |
          ```
          sudo apt-get update
          sudo apt-get install -y curl openssh-server ca-certificates perl
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
            curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
          ```
        line_back: debian
      - id: almalinux-8
        tip: |
          For AlmaLinux and RedHat 8, `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        dependency_text: On AlmaLinux 8 (and RedHat 8), the commands below will also open HTTP, HTTPS and SSH access in the system firewall. This is an optional step, and you can skip it if you intend to access GitLab only from your local network.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: almalinux-8
      - id: centos-7
        dependency_text: On CentOS 7 (and RedHat/Oracle/Scientific Linux 7), the commands below will also open HTTP, HTTPS and SSH access in the system firewall. This is an optional step, and you can skip it if you intend to access GitLab only from your local network.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: centos-7
      - id: opensuse-leap
        tip: |
          For OpenSuse, `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        dependency_text: On OpenSUSE, the commands below will also open HTTP, HTTPS and SSH access in the system firewall. This is an optional step, and you can skip it if you intend to access GitLab only from your local network.
        dependency_command: |
          ```
          sudo zypper install curl openssh perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo zypper install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash```
        install_command: |
          ```sudo EXTERNAL_URL="https://gitlab.example.com" zypper install gitlab-ee```
        line_back: opensuse-leap

      - id: amazonlinux-2022
        tip: |
          For Amazon Linux 2022 `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        dependency_text: On Amazon Linux 2022, the commands below will also open HTTP, HTTPS and SSH access in the system firewall. This is an optional step, and you can skip it if you intend to access GitLab only from your local network.
        dependency_command: |
          ```
          sudo dnf install -y curl policycoreutils openssh-server perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo dnf install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" dnf install -y gitlab-ee
          ```
        line_back: amazonlinux-2022

      - id: amazonlinux-2
        tip: |
          For Amazon Linux 2, `arm64` packages are also available and will be automatically used on that platform when using the GitLab repository for installation.
        dependency_text: On Amazon Linux 2, the commands below will also open HTTP, HTTPS and SSH access in the system firewall. This is an optional step, and you can skip it if you intend to access GitLab only from your local network.
        dependency_command: |
          ```
          sudo yum install -y curl policycoreutils-python openssh-server openssh-clients perl
          # Enable OpenSSH server daemon if not enabled: sudo systemctl status sshd
          sudo systemctl enable sshd
          sudo systemctl start sshd
          # Check if opening the firewall is needed with: sudo systemctl status firewalld
          sudo firewall-cmd --permanent --add-service=http
          sudo firewall-cmd --permanent --add-service=https
          sudo systemctl reload firewalld
          ```
        postfix_command: |
          ```
          sudo yum install postfix
          sudo systemctl enable postfix
          sudo systemctl start postfix
          ```
        download_command: |
          ```
          curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" yum install -y gitlab-ee
          ```
        line_back: amazonlinux-2
      - id: raspberry-pi-os
        tip: |
          Raspberry Pi 4 with at least 4GB is recommended. Only 32bit (armhf) is supported at this point. 64 bit (`arm64`) is on its way.
        dependency_command: |
          ```
          sudo apt-get install curl openssh-server ca-certificates apt-transport-https perl
          curl https://packages.gitlab.com/gpg.key | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
          ```
        postfix_command: |
          ```
          sudo apt-get install -y postfix
          ```
        download_command: |
          ```
          sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash
          ```
        install_command: |
          ```
          sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
          ```
  kubernetes:
    title: Kubernetes Deployments
    text: |
      When installing GitLab on Kubernetes, there are some trade-offs that you need to be aware of:

        - Administration and troubleshooting requires Kubernetes knowledge
        - It can be more expensive for smaller installations. The default installation requires more resources than a single node Linux package deployment, as most services are deployed in a redundant fashion.
        - There are some feature [limitations to be aware of.](https://docs.gitlab.com/charts/#limitations){data-ga-name="chart limitations" data-ga-location="kubernetes installation"}

      Use this method if your infrastructure is built on Kubernetes and you’re familiar with how it works. The methods for management, observability, and some concepts are different than traditional deployments. The helm chart method is for Vanilla Kubernetes deployments and the GitLab Operator can be used to deploy GitLab on an OpenShift cluster. The GitLab Operator can be used to automate Day 2 operations in both OpenShift and vanilla Kubernetes deployments.
    cards:
      - title: Helm Chart
        subtext: Install GitLab using Helm charts
        icon:
          name: kubernetes-purple
          alt: Kubernetes Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/charts/
        data_ga_name: helm charts
        data_ga_location: kubernetes installation
      - title: GitLab Operator
        new_flag: true
        subtext: Install GitLab using the Operator
        icon:
          name: gitlab-operator
          alt: GitLab Operator Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/charts/installation/operator.html
        data_ga_name: gitlab operator
        data_ga_location: kubernetes installation
  supported_cloud:
    title: Supported cloud
    text: |
      Use the official Linux package to install GitLab in various cloud providers
    cards:
      - title: Amazon Web Services (AWS)
        subtext: Install GitLab on AWS
        icon:
          name: aws
          alt: AWS Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/install/aws/
        data_ga_name: aws install documentation
        data_ga_location: supported cloud
      - title: Google Cloud Platform (GCP)
        subtext: Install GitLab on GCP
        icon:
          name: gcp
          alt: GCP Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/install/google_cloud_platform/
        data_ga_name: gcp install documentation
        data_ga_location: supported cloud
      - title: Microsoft Azure
        subtext: Install GitLab on Azure
        icon:
          name: azure
          alt: Azure Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/install/azure/
        data_ga_name: azure install documentation
        data_ga_location: supported cloud
  official_methods:
    title: Other official, supported installation methods
    cards:
      - title: Docker
        subtext: Official GitLab Docker Images
        icon:
          name: docker
          alt: Docker Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/install/docker.html
        data_ga_name: docker install documentation
        data_ga_location: official installation
      - title: Reference Architectures
        subtext: Recommended GitLab deployment topologies
        icon:
          name: gitlab-tanuki
          alt: GitLab Icon
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
        data_ga_name: reference architectures install documentation
        data_ga_location: official installation
      - title: Installation from source
        subtext: Install GitLab using the source files on a Debian/Ubuntu system
        icon:
          name: source
          alt: Source Icon
          variant: marketing
          hex_color: '#336CE4'          
        link_text: View install instructions
        link_url: https://docs.gitlab.com/ee/install/installation.html
        data_ga_name: installation from source
        data_ga_location: official installation
      - title: GitLab Environment Toolkit (GET)
        subtext: Automation for deploying GitLab Reference Architectures using Terraform and Ansible
        icon:
          name: gitlab-environment-toolkit
          alt: GitLab Environment Toolkit Icon
          variant: marketing
        link_text: View install instructions
        link_url: https://gitlab.com/gitlab-org/gitlab-environment-toolkit
        data_ga_name: gitlab environment toolkit installation
        data_ga_location: official installation
  unofficial_methods:
    title: Unofficial, unsupported installation methods
    cards:
      - title: Debian native package
        subtext: by Pirate Praveen
        icon:
          name: debian
          alt: Debian Icon
        link_text: View install instructions
        link_url: https://wiki.debian.org/gitlab/
        data_ga_name: debian native installation
        data_ga_location: unofficial installation
      - title: FreeBSD package
        subtext: by Torsten Zühlsdorff
        icon:
          name: freebsd
          alt: FreeBSD Icon
        link_text: View install instructions
        link_url: http://www.freshports.org/www/gitlab-ce
        data_ga_name: freebsd installation
        data_ga_location: unofficial installation
      - title: Arch Linux package
        subtext: by the Arch Linux community
        icon:
          name: arch-linux
          alt: Arch Linux Icon
        link_text: View install instructions
        link_url: https://archlinux.org/packages/extra/x86_64/gitlab/
        data_ga_name: arch linux installation
        data_ga_location: unofficial installation
      - title: Puppet module
        subtext: by Vox Pupuli
        img_src: /nuxt-images/install/puppet-logo.svg
        icon:
          name: puppet
          alt: Puppet Icon
        link_text: View install instructions
        link_url: https://forge.puppet.com/puppet/gitlab
        data_ga_name: puppet module installation
        data_ga_location: unofficial installation
      - title: Ansible playbook
        subtext: by Jeff Geerling
        icon:
          name: ansible-purple
          alt: Ansible Icon
        link_text: View install instructions
        link_url: https://github.com/geerlingguy/ansible-role-gitlab
        data_ga_name: ansible installation
        data_ga_location: unofficial installation
      - title: GitLab virtual appliance (KVM)
        subtext: by OpenNebula
        icon:
          name: open-nebula
          alt: Open Nebula Icon
        link_text: View install instructions
        link_url: https://marketplace.opennebula.io/appliance/6b54a412-03a5-11e9-8652-f0def1753696
        data_ga_name: opennebula installation
        data_ga_location: unofficial installation
      - title: GitLab on Cloudron
        subtext: via Cloudron App Library
        img_src: /nuxt-images/install/cloudron-logo.svg
        icon:
          name: cloudron
          alt: Cloudron Icon
        link_text: View install instructions
        link_url: https://cloudron.io/store/com.gitlab.cloudronapp.html
        data_ga_name: cloudron installation
        data_ga_location: unofficial installation
  updates:
    title: Already Installed GitLab?
    cards:
      - title: Update from an old version of GitLab
        text: Update your GitLab installation to take advantage of the latest features. Versions of GitLab which include new functionality are released every month on the 22nd.
        link_url: https://about.gitlab.com/update/
        link_text: Update to the latest release of GitLab
        data_ga_name: Update to the latest relase of GitLab
        data_ga_location: update
      - title: Update from GitLab Community Edition
        text: GitLab Enterprise Edition includes advanced features and functionality not available in the Community Edition.
        link_url: https://about.gitlab.com/upgrade/
        link_text: Upgrade to Enterprise Edition
        data_ga_name: Upgrade to Enterprise Edition
        data_ga_location: update
      - title: Upgrade from manually installed Omnibus package
        text: With GitLab 7.10 we have introduced package repositories for GitLab, that allow you to install GitLab with a simple command.
        link_url: https://about.gitlab.com/upgrade-to-package-repository/
        link_text: Upgrade to Omnibus package repository
        data_ga_name: Upgrade to Omnibus package repository
        data_ga_location: update
      - title: Third-party applications that support GitLab
        text: GitLab is open to collaboration and committed to building technology partnerships in the DevOps ecosystem. Learn more about the benefits and requirements of becoming a GitLab Technology Partner.
        link_url: https://about.gitlab.com/partners/
        link_text: View third-party applications
        data_ga_name: View third-party applications
        data_ga_location: update
