---
  title: Platform
  description: Learn more about how the GitLab platform can help teams collaborate and build software faster.

  header: The DevSecOps Platform
  subcopy: Deliver better software faster with one platform for your entire software delivery lifecycle
  img:
    url: /nuxt-images/platform/devops-infinity.png
    alt: The DevOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield.
  cta_primary:
    text: Get free trial
    href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial
    data_ga_name: free trial
    data_ga_location: hero
  cta_secondary:
    text:  Talk to an expert
    href: /sales/
    data_ga_name: sales
    data_ga_location: hero
  cards:
    - icon: ebook
      type: Ebook
      text: Starting and Scaling DevOps in the Enterprise
      link:
        href: https://about.gitlab.com/resources/scaling-enterprise-devops/
        data_ga_name: starting and scaling devops in the enterprise ebook
        data_ga_location: platform cards
    - icon: blog
      type: Blog Post
      text: Making the case for a DevOps platform
      link:
        href: https://about.gitlab.com/blog/2021/09/08/making-the-case-for-a-devops-platform-what-data-and-customers-say/
        data_ga_name: making the case for a devops platform blog post
        data_ga_location: platform cards
    - icon: whitepapers
      type: White Paper
      text: Manage your toolchain before it manages you
      link:
        href: https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/
        data_ga_name: manage your toolchain before it manages you white paper
        data_ga_location: platform cards

  platform_features:
    table_label: GitLab features that fall within each stage of the software development lifecycle. These stages are grouped into phases of software innovation.

  report_cta:
    layout: "dark"
    title: Analyst reports
    reports:
    - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: GitLab recognized as a Leader in the 2023 Gartner Magic Quadrant" for DevOps Platforms
      url: /gartner-magic-quadrant/
