---
  title: DORA Metrics
  description: Accelerating DevOps with DORA Metrics and Value Stream Analytics Management.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note: 
          -  A continuous journey needs continuous improvement
        title: DORA Metrics
        subtitle: Accelerating DevOps with DORA Metrics and Value Stream Analytics Management.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Learn about pricing
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/compliance/compliance-overview.png
          image_url_mobile: /nuxt-images/gitlab-security-and-governance/gitlab-security-and-governance.jpg
          alt: "Image: gitlab for public sector"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Trusted by
        logos:
          - name: UBS Logo
            image: "/nuxt-images/home/logo_ubs_mono.svg"
            url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
            aria_label: Link to UBS customer case study
          - name: Hackerone Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: https://about.gitlab.com/customers/hackerone/
            aria_label: Link to Hackerone customer case study
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: https://about.gitlab.com/customers/thezebra/
            aria_label: Link to The Zebra customer case study
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: https://about.gitlab.com/customers/hilti/
            aria_label: Link to Hilti customer case study
          - name: Conversica Logo
            image: /nuxt-images/logos/conversica.svg
            url: https://about.gitlab.com/customers/conversica/
            aria_label: Link to Conversica customer case study
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: https://about.gitlab.com/customers/bab/
            aria_label: Link to Bendigo and Adelaide Bank customer case study
          - name: Glympse Logo
            image: /nuxt-images/logos/glympse-logo-mono.svg
            url: https://about.gitlab.com/customers/glympse/
            aria_label: Link to Glympse customer case study
    - name: 'side-navigation-variant'
      links:
        - title: Overview
          href: '#overview'
        - title: Benefits
          href: '#benefits'
        - title: Capabilities
          href: '#capabilities'
        - title: Customers
          href: '#customers'
        - title: Resources
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-intro'
              data:
                aos_animation: fade-up
                aos_duration: 800
                header: DevOps maturity and benchmarks.
                text:
                  description: "After eight years of data collection and research, DORA's Accelerate State of DevOps research program has developed and validated four elements that measure software delivery velocity and a fifth one for stability: (1) deployment frequency, (2) lead time for changes, (3) mean time to restore  and (4) change failure rate and (5) reliability. The outcomes from the report help teams measure and improve their DevOps performance. GitLab offers out-of-the- box DORA metrics visibility for teams to measure current state, deliver visibility across the value chain, streamline with business objectives, and promote a collaborative culture"
            - name: 'by-solution-benefits'
              data:
                title: Track and manage the flow of software development
                is_accordion: false
                items:
                  - icon:
                      name: visibility
                      alt: visibility Icon
                      variant: marketing
                    header: Measure and identify inefficiencies in your SDLC
                    text: Accurate out-of-the-box DORA metrics to benchmark your engineering teams.
                  - icon:
                      name: automated-code
                      alt: automated-code Icon
                      variant: marketing
                    header: Automate to deliver better products faster
                    text: Bring in data hygiene with a single platform to keep up with the accelerating speed of business
                  - icon:
                      name: gitlab-monitor-alt
                      alt: monitor Icon
                      variant: marketing
                    header: Prioritize delivery and operational excellence
                    text: DORA metrics coupled with Value Stream Analytics helps draw insights to make data-driven decisions.
            - name: 'solutions-video-feature'
              data:
                video: 
                  url: 'https://www.youtube.com/embed/1BrcMV6rCDw'
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Why GitLab for DORA? 
                cards:
                  - title: DORA Custom Reporting
                    description: DORA Insights allows users to create custom reports to explore data and track metrics improvements, understand patterns in their metrics trends, and compare performance between groups and projects.
                    data_ga_name: dora report
                    data_ga_location: benefits
                    icon:
                      name: devsecops
                      alt: DevSecOps Icon
                      variant: marketing
                    href: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#insights-custom-dora-reporting
                  - title: DORA Metrics API
                    description: GitLab enables retrieval and usage of the DORA metrics data via GraphQL and REST APIs for analytics and reporting best suited for your team. You can empower your business teams to utilize metrics data through APIs, without technical barriers.
                    data_ga_name: dora metrics
                    data_ga_location: benefits
                    icon:
                      name: lock-cog
                      alt: lock-cog Icon
                      variant: marketing
                    href: https://docs.gitlab.com/ee/api/graphql/reference/index.html
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Optimize engineering velocity 
                sub_description: ''
                white_bg: true
                markdown: true
                sub_image: /nuxt-images/solutions/dev-sec-ops/dev-sec-ops.png
                solutions:
                  - title: Deployment Frequency
                    description: | 
                      The number of times code or software is deployed to production or “shipped”. You can evaluate the needs of the business and ensure that the velocity matches business needs.
                      
                      **Insights driven through Value Stream Analytics**
                    list: 
                      - "Scope for automation to improve processes"
                      - "Benchmark against target business goals"
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#deployment-frequency
                    data_ga_name: dora metrics
                    data_ga_location: solutions block
                  - title: Lead time for change
                    description: | 
                      The time from when development teams start working on a feature to the time the feature gets deployed. Understanding the pace of delivery and aiming for smaller, frequent deployments can help you get quicker feedback.
                      
                      **Insights driven through Value Stream Analytics**
                    list: 
                      - "Breakdown release process based on time spent in different tasks"
                      - "Identify and fix bottlenecks in release process leading to delays"
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#lead-time-for-changes
                    data_ga_name: lead time for change
                    data_ga_location: solutions block
                  - title: Time to Restore Service 
                    description: | 
                      The time it takes to restore a failure in production, where a failure can be an unplanned outage or a service failure. Service failures and outages can be of different types and severity, which can make it tricky to measure.
                      
                      **Insights driven through Value Stream Analytics**
                    list:
                      - Shift-left opportunities to minimize service failure and related impacts
                      - Drill down to the specific apps which respond poorly to failure/outages
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#time-to-restore-service
                    data_ga_name: time to restore service
                    data_ga_location: solutions block
                  - title: Change failure rate
                    description: |
                      The percentage of deployment causing a failure in production. It is the measure of the number of times “a hotfix, a rollback, a fix-forward, or a patch” is required after a deployment. You can assess code quality, and testing procedures to understand failure rates.
                      
                      **Insights driven through Value Stream Analytics**
                    list:
                      - Deeper understanding of risk factors resulting in failure
                      - Process inefficiencies to address to minimize risks
                    link_text: Learn More
                    link_url: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#change-failure-rate
                    data_ga_name: change failure rate
                    data_ga_location: solutions block
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Companies deriving value from DORA metrics with Value Stream Analytics
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/zoopla-logo.png
                      alt: zoopla Logo
                    quote: “Without GitLab, we wouldn’t be able to measure them. So this would be the key point, and that’s also why I really like the way the API is designed. There’s a lot more that we can be doing with just the API, because without that, we wouldn’t be able to measure all of that stuff, because we wouldn’t know.”
                    author: Gustaw Fit
                    position: Engineering Lead, Zoopla
                    ga_location: customers
                    url: /customers/zoopla/
                  - title_img:
                      url: /nuxt-images/logos/axway-logo.svg
                      alt: Axway Logo
                    quote: “If you want to speed up the delivery cycle, you need to simplify your ecosystem. And we've been doing that with GitLab along the way, it's critical for developers to have one single point of contact and one simple interface to increase the speed of delivery.”
                    author: Eric Labourdette
                    position: CP Cloud Operations, Axway
                    ga_location: customers
                    ga_carousel: Axway testimonial
                    url: /customers/axway-devops/
                  - title_img:
                      url: /nuxt-images/logos/logoradiofrance.svg
                      alt: Radio France Logo
                    quote: '"We knew that using a combination of various tools would be very wasteful, so we looked for a tool that integrated the elements we needed at the time:  issue management, progress management, code management, CI, and CD."'
                    author: Julien Vey
                    position: Operations Excellence Manager, Radio France
                    ga_location: customers
                    ga_carousel: radio france testimonial
                    url: /customers/radiofrance/
                  - title_img:
                      url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
                      alt: Fujitsu Logo
                    quote: “We were at 10 per day before the migration. Now with GitLab, we do 50 deployments per day in production, which is way more efficient than when we had to switch between GitLab and Jenkins.”
                    author: Yuichi Saotome
                    position: Principal Engineer, Cloud Infra Division, Fujitsu Cloud Technologies
                    ga_carousel: Fujitsu testimonial
                    ga_location: customers
                    url: /customers/fujitsu/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: |
                      "GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running."
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_carousel: hilti testimonial
                    ga_location: customers
                    url: /customers/hilti/
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Related Resources
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: "DORA Metrics: Speed Run"
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/DORA/video1.jpg"
                  href: https://www.youtube.com/embed/wQU-mWvNSiI
                  data_ga_name: "DORA Metrics: Speed Run"
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: "DORA: Change Failure Rate"
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/DORA/video2.jpg"
                  href: https://www.youtube.com/embed/_EKW0eg3n4E
                  data_ga_name: "DORA: Change Failure Rate"
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: "DORA: Mean time to Restore"
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/DORA/video3.jpg"
                  href: https://www.youtube.com/embed/r3fyRtxFFUk
                  data_ga_name: "DORA: Mean time to Restore"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header:  How the DORA metrics can help DevOps team performance
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/04/20/how-the-dora-metrics-can-help-devops-team-performance/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name:  How the DORA metrics can help DevOps team performance
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header:  Break the black box of software delivery with GitLab Value Stream Management and DORA Metrics
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/06/20/gitlab-value-stream-management-and-dora/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name:  Break the black box of software delivery with GitLab Value Stream Management and DORA Metrics
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: Elite team strategies to secure the software supply chain
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/01/06/elite-team-strategies-to-secure-software-supply-chains/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: Elite team strategies to secure the software supply chain
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: GitLab DevSecOps survey
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_11.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: GitLab DevSecOps survey
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: DORA metrics in GitLab One DevOps Platform
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/DORA/video4.jpg"
                  href: https://www.youtube.com/embed/1BrcMV6rCDw
                  data_ga_name: DORA metrics in GitLab One DevOps Platform
                  data_ga_location: resource cards
                - icon:
                    name: docs
                    variant: marketing
                    alt: doc Icon
                  event_type: "Doc"
                  header: "DevOps Research and Assessment (DORA) metrics | GitLab"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://docs.gitlab.com/ee/user/analytics/dora_metrics.html#devops-research-and-assessment-dora-metrics
                  data_ga_name: "DevOps Research and Assessment (DORA) metrics | GitLab"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "GitLab DevSecOps survey"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Do more with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: all solutions
          data_ga_location: do more with gitlab
        cards:
          - title: Continuous Software Compliance
            description: Integrating security into your DevOps lifecycle is easy with GitLab.
            icon:
              name: continuous-integration
              alt: Continuous Integration Icon
              variant: marketing
            href: /solutions/compliance/
            data_ga_name: compliance
            data_ga_location: do more with gitlab
          - title: Software Supply Chain Security
            description: Ensure your software supply chain is secure and compliant.
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: supply chain
            data_ga_location: do more with gitlab
          - title: Continuous Integration and Delivery
            description: Make software delivery repeatable and on-demand
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /features/continuous-integration/
            data_ga_name: ci
            data_ga_location: do more with gitlab

                  