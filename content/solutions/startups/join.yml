---
  title: GitLab for Startups
  description: The single application to accelerate your startup. We are offering our top tiers for free to startups that meet the eligibility criteria. Learn more!
  image_alt: GitLab for Startups
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          - GitLab for Startups Program
        title: Software. Faster.
        subtitle: Decrease cycle times and deploy more frequently with less effort. With a single application for the entire software delivery lifecycle, your teams can focus on shipping better software, faster.
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application-form'
          text: Apply Now
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/startups_join.jpeg
          alt: "Image: gitlab for open source"
          bordered: true
          position: left
    - name: 'startups-intro'
      data:
        title: "Now available to even more startups, a program built to help accelerate your business."
        subtitle: |

          Startups need to build for exponential growth while doing more with less. With GitLab you can build innovative products to reach new customers and achieve your revenue goals.

          **Qualifying\* startups get GitLab Ultimate free for one year.**

    - name: 'education-case-study-carousel'
      data:
        customers_cta: true
        cta:
          href: '/customers/'
          text: View more stories
          data_ga_name: customers
        logo_in_card: true
        case_studies:
          - logo_url: "/nuxt-images/logos/chorus-color.svg"
            institution_name: Te Herenga Waka—Victoria University of Wellington
            quote:
              img_url: /nuxt-images/blogimages/Chorus_case_study.png
              quote_text: Life is just so much easier for product engineering and anyone who wants to interact with product engineering because they can just do it through GitLab
              author: Russell Levy
              author_title: Co-founder and CTO, Chorus.ai
            case_study_url: /customers/chorus/
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel

    - name: startups-overview
      data:
        header: Who qualifies for GitLab for Startups Program?
        description: GitLab for Startups Tiers, tailored for your phase of growth.
        blocks:
          - header: Seed Stage Startup
            offers:
              - list:
                - |
                  Pre-Seed or Seed Stage; up to
                  $5 million in funding.
                - Funding must have been raised externally.
                - New customers only.
              - title: "During Year One, you'll receive:"
                list:
                  - Free Ultimate licence for a year (SaaS or self-managed) up to 20 seats.
                  - Support not included. That's okay, GitLab is designed to be super easy to use.
              - title: "During Year Two, you'll receive:"
                list:
                  - 50% discount on any tier; up to 20 Users.
          - header: Early Stage Startup
            offers:
              - list:
                - |
                  Currently series A or Series B; up to
                  $20 million in funding.
                - Funding must have been raised externally.
                - New customers only.
              - title: "During Year One, you'll receive:"
                list:
                  - 50% discount on any tier; up to 20 Users.
              - title: "During Year Two, you'll receive:"
                list:
                  - 25% discount on any tier; up to 20 Users.
        information:
          header: Additional Requirements and Information
          list:
            - Once you apply, you may be asked to provide additional information in order to verify eligibity.
            - Upon acceptance to the GitLab for StartUps Program, all program members are subject to the [GitLab Subscription Agreement](/handbook/legal/subscription-agreement/). 
            - For self-hosted instances, Users must enable usage and analytics features.
            - The GitLab for Startups Program is offered at the sole discretion of GitLab and is subject to change.
            - Questions? Reach us at [startups@gitlab.com](mailto:startups@gitlab.com).
    - name: 'open-source-form-section'
      data:
        id: application-form
        title: Application form
        blocks:
          - content: |
              Apply today to join the GitLab for Startups program. Our team will reach out to you and guide you in getting started with GitLab. We are excited to see what innovative products you build with our support.
        form:
          form_id: 2154
          form_header: ''
          datalayer: startups
    - name: 'startups-link'
      data:
        header: Why join GitLab for Startups?
        description: From seed funding to cash flow positive to large multinational enterprise, whatever your ambitions, GitLab is ready to scale with you as your business grows.
        button:
          href: /solutions/startups
          text: Learn more
        image: /nuxt-images/solutions/startups_hero.jpeg
        alt: crowd during a presentation
        icon: calendar-alt-2
    - name: faq
      data:
        header: Frequently asked questions
        show_all_button: true
        background: true
        groups:
        - header: ""
          questions:
            - question: Can self-funded startups apply?
              answer: >-
                The program is currently available for externally funded companies only, with the given qualification parameters. On subsequent iterations, we will consider assessing other portfolios.
            - question: How is the external funding validated?
              answer: >-
                Application data will be validated by GitLab to determine eligibility. Applicants must provide a Crunchbase, Pitchbook proifle, financial statements or any other reasonable sustantiation to prove funding status.
            - question: What will happen after one year?
              answer: |
                Please refer to where your Startup falls in the funding parameters to ensure you know the 2nd year pricing structure. A team member will reach out well in advance to provide guidance and time to prepare.
            - question: Who gets counted in the subscription?
              answer: >-
                Please see the detailed response in our [licensing and subscription FAQ](/pricing/licensing-faq/#who-gets-counted-in-the-subscription)
            - question: Are we allowed to increase the number of seats in the future?
              answer: >-
                If you want to increase the number of seats on your existing license, please email [startups@gitlab.com](mailto:startups@gitlab.com), and we’ll prepare an add-on quote for additional seats.
