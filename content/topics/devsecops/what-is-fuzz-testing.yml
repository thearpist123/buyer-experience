---
  title: What is fuzz testing?
  description: Fuzz testing, also called fuzzing, is a way to find bugs other software testing methodologies can’t."
  date_published: 2023-04-14
  date_modified: 2023-04-14
  topics_header:
    data:
      title: What is fuzz testing?
      block:
        - metadata:
            id_tag: what-is-fuzz-testing
          text: |
            How can you find vulnerabilities when you don’t know exactly what you’re looking for? Learn how fuzz testing, or fuzzing, can help to detect zero-day vulnerabilities.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevSecOps
      href: /topics/devsecops/
      data-ga-name: devsecops
      data_ga_location: breadcrumb
    - title: What is fuzz testing?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Overview
          href: "#overview"
          data_ga_name: overview
          data_ga_location: side-navigation
        - text: History of fuzzing
          href: "#history-of-fuzzing"
          data_ga_name: history of fuzzing
          data_ga_location: side-navigation
        - text: Two types of fuzzing
          href: "#two-types-of-fuzzing"
          data_ga_name: two types of fuzzing
          data_ga_location: side-navigation
        - text: Benefits of fuzzing
          href: "#benefits-of-fuzzing"
          data_ga_name: benefits of fuzzing
          data_ga_location: side-navigation
        - text: Challenges of fuzzing
          href: "#challenges-of-fuzzing"
          data_ga_name: challenges of fuzzing
          data_ga_location: side-navigation
        - text: Find bugs with with coverage-guided fuzzing
          href: "#find-bugs-with-coverage-guided-fuzzing"
          data_ga_name: find bugs with coverage guided fuzzing
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: Overview
          column_size: 10
          blocks:
              - text: |
                  Fuzz testing, or application fuzzing, is a software testing technique that allows teams to discover security vulnerabilities or bugs in the source code of software applications. Unlike traditional software testing methodologies – SAST, DAST, or IAST – fuzzing essentially “pings” code with random inputs in an effort to crash it and thus identify faults that would otherwise not be apparent. Those code faults (or problems with business logic) represent areas that are potentially at high risk for security threats.

                  When a fault or vulnerability is found, a fuzzer — a tool that identifies the potential causes of the crash — can be used to zero in on specific vulnerabilities in the source code. Fuzzers are most effective at uncovering vulnerabilities that can be exploited by attacks such as SQL injection and cross-site scripting, where hackers disable security to steal information or take down a system. Fuzzers are less effective at identifying vulnerabilities that are unrelated to system crashes, such as spyware or Trojans.

                  Fuzz testing’s supporters praise it for being fully automated and able to find obscure weaknesses, while its detractors complain it can be difficult to set up and prone to deliver unreliable results.

      - name: 'topics-copy-block'
        data:
          header: History of fuzzing
          column_size: 10
          blocks:
            - text: |
                Fuzz testing stands out in another way as well: there’s actually a story about how the concept was discovered. In 1988, University of Wisconsin – Madison Professor Barton Miller was trying to access code remotely using a dial-up system, but feedback from a thunderstorm kept causing the program to crash. The idea that external “noise” couldn’t be tolerated by code became the inspiration for Miller and his student’s work. They discovered that Unix, Mac, and Windows programs would routinely crash when pinged by random unexpected inputs. Miller is one of the authors of [Fuzzing for Software Security Testing and Quality Assurance](https://www.amazon.com/Fuzzing-Software-Security-Assurance-Information/dp/1596932147/ref=sr_1_2){data-ga-name="fuzzing for softwware security testing and quality assurance book" data-ga-location="body"}.
      - name: 'topics-copy-block'
        data:
            header: Two types of fuzzing
            column_size: 10
            blocks:
              - text: |
                  There are two main types of fuzzing: coverage-guided and behavioral.

                  [Coverage-guided fuzzing](https://about.gitlab.com/blog/2020/10/01/fuzzing-with-gitlab/){data-ga-name="fuzzing with gitlab" data-ga-location="body"} focuses on the source code while the app is running, probing it with random input in an effort to uncover bugs. New tests are constantly being generated and the goal is to get the app to crash. A crash means a potential problem, and data from the coverage-guided fuzz testing process will allow a tester to reproduce the crash, which is helpful when trying to identify at-risk code.

                  Behavioral fuzzing works differently. Using specs to show how an application should work, it uses random inputs to judge how the app really works; the difference between the expected and the reality is generally where bugs or other potential security risks can be found.
      - name: 'topics-copy-block'
        data:
            header: Benefits of fuzzing
            column_size: 10
            blocks:
              - text: |
                  Why is fuzz testing important for [DevSecOps](https://about.gitlab.com/topics/devsecops/){data-ga-name="devsecops topics" data-ga-location="body"}? Because of the random nature of fuzz testing, experts say it’s the methodology most likely to find bugs missed by other tests. It’s also seen as an incredibly low-effort testing methodology, or what some like to call “set it and forget it.” Once the test harness is created fuzz testing is fully automated and will run indefinitely. It can be scaled easily by spinning up more machines and is a good choice for regression testing. Fuzzing also speeds up the development process by maximizing code coverage — how much of the code has been executed by the fuzzer — without introducing false positives. Higher code coverage means more thorough testing.

                  Fuzz testing is also ideal to work alongside a manual testing team, as both sets of inputs will educate the other.
      - name: 'topics-copy-block'
        data:
          header: Challenges of fuzzing
          column_size: 10
          blocks:
            - text: |
                Two main challenges face practitioners looking to implement fuzz testing: setup and data analysis. Fuzz testing isn’t necessarily easy to set up; it requires complex testing “harnesses” that can be even more tricky to create if the fuzz testing isn’t actually located within an existing toolchain. Plus, fuzz testing can generate a lot of data, including potentially false positives. So it’s critical to ensure that a testing team is prepared to deal with the onslaught of information.

                Also, while less easy to document, negative attitudes toward the “vague” nature of fuzz testing do persist in the QA community.
      - name: 'topics-copy-block'
        data:
          header: Find bugs with coverage-guided fuzzing
          column_size: 10
          blocks:
            - video:
                video_url: 'https://www.youtube.com/embed/4ROYvNfRZVU'
              text: |

  components:
    - name: solutions-resource-cards
      data:
        title: Learn more about DevSecOps
        grouped: true
        column_size: 4
        cards:
          - icon:
              name: video
              variant: marketing
              alt:
            event_type: "Video"
            header: Watch a video about fuzz testing
            link_text: "Watch now"
            image: "/nuxt-images/topics/fuzz-testing.jpeg"
            href: https://www.youtube.com/embed/4ROYvNfRZVU
            data_ga_name: fuzz testing video
            data_ga_location: resource cards
          - icon:
              name: articles
              alt:
              variant: marketing
            event_type: "Article"
            header: Technical documentation on GitLab's fuzz testing
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
            data_ga_name: gitlab fuzz testing docs
            data_ga_location: resource cards
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt:
            event_type: "Blog"
            header: How developer-centric AppSec testing can dramatically change your DevOps team
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: /blog/2020/08/21/align-engineering-security-appsec-tests-in-ci/
            data_ga_name: how developer-centric appsec testing can dramatically change your devops team blog
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt:
            event_type: "Blog"
            header: How recent acquisitions introduce fuzz-testing to GitLab
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: /blog/2020/07/17/fuzz-testing/
            data_ga_name: how recent acquisitions introduce fuzz-testing to GitLab
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt:
            event_type: "Blog"
            header: Find Bugs with Coverage-Guided Fuzz Testing
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: /blog/2020/10/01/fuzzing-with-gitlab/
            data_ga_name: find bugs with coverage-guided fuzz testing
            data_ga_location: resource cards
