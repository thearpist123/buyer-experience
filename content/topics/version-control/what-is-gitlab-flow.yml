---
  title: What is GitLab Flow?
  description: Code reviews ensure developers ship the highest quality code through systematic assessments designed to identify bugs.
  topics_header:
    data:
      title: What is GitLab Flow?
      block:
        - metadata:
            id_tag: what-is-gitlab-flow
          text: GitLab Flow prevents the overhead of releasing, tagging, and merging to streamline development.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Version Control
      href: /topics/version-control/
      data-ga-name: version-control
      data_ga_location: breadcrumb
    - title: What is GitLab Flow?
  side_menu:
    anchors:
      text: "On this page"
      # data filled automatically by template
    hyperlinks:
      text: ''
      data: []
    content:   
      - name: topics-copy-block
        data:
          no_header: true
          no_decoration: true
          column_size: 11
          blocks: 
            - text: |
                Git simplifies branching and merging, leading software development teams to move away from other source control tools, like SVN, and adopt a workflow to simplify development. Organizations moving to Git from other [version control](https://about.gitlab.com/topics/version-control/) systems may have difficulty identifying an effective workflow. GitLab Flow creates a seamless approach to software development by integrating a Git workflow with an issue tracking system.
      - name: topics-copy-block
        data:
          header: What is GitLab Flow?
          column_size: 11
          blocks:
            - text: |
                GitLab Flow is a simpler alternative to [GitFlow](https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/) and combines feature driven development and feature branches with issue tracking. With GitLab Flow, all features and fixes go to the `main` branch while enabling `production` and `stable` branches. GitLab Flow includes a set of [best practices](/topics/version-control/what-are-gitlab-flow-best-practices/) and guidelines to ensure software development teams follow a smooth process to ship features collaboratively.
      - name: topics-copy-block
        data:
          header: How does GitLab Flow work?
          column_size: 11
          blocks:
            - text: |
                With GitFlow, developers create a `develop` branch and make that the default, while GitLab Flow works with the ‘main’ branch right away. GitLab Flow incorporates a pre-production branch to make bug fixes before merging changes back to `main` before going to production. Teams can add as many pre-production branches as needed — for example, from `main` to test, from test to acceptance, and from acceptance to production.

                Essentially, teams practice feature branching, while also maintaining a separate production branch. Whenever the ‘main’ branch is ready to be deployed, users merge it into the production branch and release. GitLab Flow is often used with release branches. Teams that require a public API may need to maintain different versions. Using GitLab Flow, teams can make a `v1` branch and a `v2` branch that can be maintained individually, which can be helpful if the team identifies a bug during [code reviews](/topics/version-control/what-is-code-review/) that goes back to `v1.`
      - name: topics-copy-block
        data:
          header: What are the benefits of GitLab Flow?
          column_size: 11
          blocks:
            - text: |
                GitLab Flow offers a simple, transparent, and effective way to work with Git. Using [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html), developers can collaborate on and maintain several versions of software in different environments. GitLab Flow decreases the overhead of releasing, tagging, and merging, which is a common challenge encountered with other types of Git [workflows](https://git-scm.com/), to create an easier way to deploy code. Commits flow downstream to ensure that every line of code is tested in all environments. Teams of any size can use GitLab FLow, and it has the flexibility to adapt to various needs and challenges.
  components:
    - name: topics-cta
      data:
        subtitle: Discover how GitLab streamlines the code review process
        text: GitLab streamlines software development with comprehensive version control and collaboration.
        column_size: 10
        cta_one:
          text: Learn More
          link: /stages-devops-lifecycle/source-code-management/
          data_ga_name: Learn More
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Ready to learn more about Git and version control?
        column_size: 4
        cards:
          - icon:
              name: webcast
              variant: marketing
            event_type: "Webcast"
            header: Learn how to deliver faster with GitLab
            image: "/nuxt-images/resources/resources_10.jpeg"
            link_text: "Learn more"
            href: /webcast/collaboration-without-boundaries/
            data_ga_name: learn how to deliver faster with GitLab
            data_ga_location: resources
          - icon:
              name: book
              variant: marketing
            event_type: "Books"
            header: Download the Git branching strategies eBook to simplify your workflow
            image: "/nuxt-images/resources/resources_1.jpeg"
            link_text: "Learn more"
            href: /resources/ebook-git-branching-strategies/
            data_ga_name: download the Git branching strategies eBook to simplify your workflow
            data_ga_location: body
