---
  title: What are Git version control best practices?
  description: Making the most of Git involves learning best practices to streamline workflows and ensure consistency across a codebase.
  topics_header:
    data:
      title: What are Git version control best practices?
      block:
        - metadata:
            id_tag: version-control-best-practices
          text: Making the most of Git involves learning best practices to streamline workflows and ensure consistency across a codebase.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Version Control
      href: /topics/version-control/
      data-ga-name: version-control
      data_ga_location: breadcrumb
    - title: What are Git version control best practices?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: 'The importance of Git version control best practices'
          href: "#the-importance-of-git-version-control-best-practices"
          data_ga_name: the importance of Git version control best practices
          data_ga_location: side-navigation
          variant: primary
        - text: 'Make incremental, small changes'
          href: "#make-incremental-small-changes"
          data_ga_name: make incremental, small changes
          data_ga_location: side-navigation
        - text: 'Keep commits atomic'
          href: "#keep-commits-atomic"
          data_ga_name: "keep commits atomic"
          data_ga_location: side-navigation
        - text: 'Develop using branches'
          href: "#develop-using-branches"
          data_ga_name: "develop using branches"
          data_ga_location: side-navigation
        - text: 'Write descriptive commit messages'
          href: "#write-descriptive-commit-messages"
          data_ga_name: "write descriptive commit messages"
          data_ga_location: side-navigation
        - text: 'Obtain feedback through code reviews'
          href: "#obtain-feedback-through-code-reviews"
          data_ga_name: "obtain feedback through code reviews"
          data_ga_location: side-navigation
        - text: 'Identify a branching strategy'
          href: "#identify-a-branching-strategy"
          data_ga_name: "identify a branching strategy"
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: topics-copy-block
        data:
          header: The importance of Git version control best practices
          column_size: 11
          blocks:
            - text: |
                [Git version control](/topics/version-control/){data-ga-name="git version control" data-ga-location="body"} best practices help software development teams meet the demands of rapid changes in the industry combined with increasing customer demand for new features. The speed at which teams must work can lead teams to silos, which slows down velocity. Software development teams turn to version control to [streamline collaboration](/topics/version-control/software-team-collaboration/){data-ga-name="streamline collaboration" data-ga-location="body"} and break down information silos.


                Using [Git best practices](/images/press/git-cheat-sheet.pdf){data-ga-name="git best practices" data-ga-location="body"}, teams can coordinate all changes in a software project and utilize fast branching to help teams quickly collaborate and share feedback, leading to immediate, actionable changes.

      - name: topics-copy-block
        data:
          header: Make incremental, small changes
          column_size: 11
          blocks:
            - text: |
                Write the smallest amount of code possible to solve a problem. After identifying a problem or enhancement, the best way to try something new and untested is to divide the update into small batches of value that can easily and rapidly be tested with the end user to prove the validity of the proposed solution and to roll back in case it doesn't work without deprecating the whole new functionality.


                Committing code in small batches decreases the likelihood of integration conflicts, because the longer a branch lives separated from the main branch or codeline, the longer other developers are merging changes to the main branch, so integration conflicts will likely arise when merging. Frequent, small commits solves this problem. Incremental changes also help team members easily revert if merge conflicts happen, especially when those changes have been properly documented in the form of descriptive commit messages.


      - name: topics-copy-block
        data:
          header: Keep commits atomic
          column_size: 11
          blocks:
            - text: |
                Related to making small changes, atomic commits are a single unit of work, involving only one task or one fix (e.g. upgrade, bug fix, refactor). Atomic commits make code reviews faster and reverts easier, since they can be applied or reverted without any unintended side effects.


                The goal of atomic commits isn't to create hundreds of commits but to group commits by context. For example, if a developer needs to refactor code and add a new feature, she would create two separate commits rather than create a monolithic commit which includes changes with different purposes.


      - name: topics-copy-block
        data:
          header: Develop using branches
          column_size: 11
          blocks:
            - text: |
                Using branches, software development teams can make changes without affecting the main codeline. The running history of changes are tracked in a branch, and when the code is ready, it's merged into the main branch.


                Branching organizes development and separates work in progress from stable, tested code in the main branch. Developing in branches ensures that bugs and vulnerabilities don't work their way into the source code and impact users, since testing and finding those in a branch is easier.


      - name: topics-copy-block
        data:
          header: Write descriptive commit messages
          column_size: 11
          blocks:
            - text: |
                  Descriptive commit messages are as important as a change itself. Write descriptive commit messages starting with a verb in present tense in imperative mood to indicate the purpose of each commit in a clear and concise manner. Each commit should only have a single purpose explained in detail in the commit message. The [Git documentation](https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?id=HEAD#n133) provides guidance on how to write descriptive commit messages:


                  > Describe your changes in imperative mood, e.g. “make xyzzy do frotz” instead of “\[This patch] makes xyzzy do frotz” or “\[I] changed xyzzy to do frotz,” as if you are giving orders to the codebase to change its behavior. Try to make sure your explanation can be understood without external resources. Instead of giving a URL to a mailing list archive, summarize the relevant points of the discussion.


                  Writing commit messages in this way forces software teams to understand the value an add or fix makes to the existing code line. If teams find it impossible to find the value and describe it, then it might be worth reassessing the motivations behind the commit. There's always time to commit later, as long changes are stashed and there's consistency in commits.

      - name: topics-copy-block
        data:
          header: Obtain feedback through code reviews
          column_size: 11
          blocks:
            - text: |
                Requesting feedback from others is an excellent way to ensure code quality. [Code reviews](/topics/version-control/what-is-code-review/){data-ga-name="code reviews" data-ga-location="body"} are an effective method to identify whether a proposal solves a problem in the most effective way possible. Asking individuals from other teams to review code is important, because some areas of the code base might include specific domain knowledge or even security implications beyond the individual contributor's attributions.


                Bringing in a specific stakeholder to the conversation is a good practice and creates a faster feedback loop that prevents problems later in the software development lifecycle. This is especially important for junior developers, because through code review, senior developers can transfer knowledge in a very practical, hands on manner.
      - name: topics-copy-block
        data:
          header: Identify a branching strategy
          column_size: 11
          blocks:
            - text: |
                Software development teams include professionals with diverse experiences and background, which can potentially cause conflicting workflows. Determining a single branching strategy is the solution to a chaotic development experience.


                While there are several approaches to development, the most common are:


                * Centralized workflow: Teams use only a single repository and commit directly to the main branch.

                * Feature branching: Teams use a new branch for each feature and don't commit directly to the main branch.

                * GitFlow: An extreme version of feature branching in which development occurs on the develop branch, moves to a release branch, and merges into the main branch.

                * Personal branching: Similar to feature branching, but rather than develop on a branch per feature, it's per developer. Every user merges to the main branch when they complete their work.


                Many teams decide to follow an established workflow, but others create a customized approach based on specific needs. Regardless of the strategy, it's important to communicate the decision and workflow logistics to team members and provide training if the approach is new to some members.
  components:
    - name: topics-cta
      data:
        subtitle: Discover how GitLab helps teams create high quality code
        text: GitLab streamlines software development with comprehensive version control and collaboration.
        column_size: 10
        cta_one:
          text: Learn More
          link: /stages-devops-lifecycle/source-code-management/
          data_ga_name: Learn More
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Want to learn more about Git and best practices?
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
            event_type: "Case Study"
            header: Learn how Worldline uses GitLab to improve code reviews
            image: "/nuxt-images/resources/resources_10.jpeg"
            link_text: "Learn more"
            href: /customers/worldline/
            data_ga_name: dLearn how Worldline uses GitLab to improve code reviews
            data_ga_location: resources
          - icon:
              name: book
              variant: marketing
            event_type: "Books"
            header: Discover a Git branching strategy to simplify software development
            image: "/nuxt-images/resources/resources_1.jpeg"
            link_text: "Learn more"
            href: /resources/ebook-git-branching-strategies/
            data_ga_name: Discover a Git branching strategy to simplify software development
            data_ga_location: body
