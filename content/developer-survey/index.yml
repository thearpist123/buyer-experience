---
title: GitLab 2023 Global DevSecOps Report Series
og_title: GitLab 2023 Global DevSecOps Report Series
description: "GitLab surveyed over 5,000 development, security, and operations professionals about what's next in DevSecOps. Check out the report series to learn more!"
twitter_description: "GitLab surveyed over 5,000 development, security, and operations professionals about what's next in DevSecOps. Check out the report series to learn more!"
og_description: "GitLab surveyed over 5,000 development, security, and operations professionals about what's next in DevSecOps. Check out the report series to learn more!"
og_image: /nuxt-images/developer-survey/2023-devsecops-survey-landing-page-opengraph.jpg
twitter_image: /nuxt-images/developer-survey/2023-devsecops-survey-landing-page-opengraph.jpg
hero:
  note:
    text: 2023 Global DevSecOps Report Series
    animation_duration: .8s
  title:
    text: What’s next <br> in DevSecOps
    animation_duration: .9s
  description:
    text: Dive into the data for a complete picture of the state of DevSecOps in 2023.
    animation_duration: 1s
  display_size: true
  primary_btn:
    text: Get access to the full report series
    url: "#download"
    data_ga_name: get access to the full report series
    data_ga_location: hero
    animation_duration: 1.2s
    icon:
      name: docs
      variant: marketing
  parallax: true
intro:
  variant: dark
  components:
    - name: 'column-group'
      data:
        variant: dark
        align: top
        aos_animation: fade-up
        aos_duration: 600
        columns:
          - size: 7
            blocks:
              - text: Dev, Sec, and Ops weigh in on what’s working and what’s changing in software development.
                typography: heading2-bold
          - size: 5
            blocks:
              - text: <p>In March 2023, we asked more than 5,000 DevSecOps professionals to share their opinions on the current state of software development, operations, and security. What are teams prioritizing in 2023? Where are things improving, and where are teams still running into roadblocks? What’s the latest on hot topics such as security, artificial intelligence and machine learning, and cloud computing? These are just a few of the big questions we set out to answer this year.</p><p>Read on for our snapshot of DevSecOps in 2023.</p>
                typography: body1
survey_tabs:
    variant: dark
    tabs:
      - header: Security & Compliance
        components:
          - name: 'survey-hero'
            data:
              title:
                text: Security without sacrifices
              description:
                text: 'In the first installment of our 2023 Global DevSecOps Report Series, we examine where organizations are in their efforts to shift security left, or move security earlier in the software development lifecycle. What’s top of mind for DevSecOps teams when it comes to creating more secure applications? Where are teams seeing the biggest wins, and what work is left to be done?<br><br><a href="#download" data-ga-name="download the full report" data-ga-location="body">Download the full report</a> to explore all of the findings, or read on for the highlights.'
              aos_animation: fade-up
              aos_duration: 600
              left_padding: true
              background: /nuxt-images/developer-survey/Orb_1.png
              graphic: 'globe-graphic'
          - name: 'survey-stat-bars-section'
            data:
              title: 'AI: The new security status quo'
              description: Among devs who use AI/ML, more said that they use AI/ML to check code, that they use bots for testing, and that they use AI/ML for code review.
              aos_animation: fade-up
              aos_duration: 600
              blocks:
                - description: We use AI/ML to check code (separate from testing)
                  aos_animation: fade-right
                  aos_duration: 600
                  stats:
                    - legend: 2023
                      percentage: 62
                      color: green
                    - legend: 2022
                      percentage: 51
                      color: orange
                - description: We use bots in our testing process
                  aos_animation: fade-left
                  aos_duration: 600
                  stats:
                    - legend: 2023
                      percentage: 53
                      color: green
                    - legend: 2022
                      percentage: 39
                      color: orange
                - description: An AI/ML tool reviews code before we see it
                  aos_animation: fade-right
                  aos_duration: 600
                  stats:
                    - legend: 2023
                      percentage: 36
                      color: green
                    - legend: 2022
                      percentage: 31
                      color: orange
                - description: We don't use AI/ML
                  aos_animation: fade-left
                  aos_duration: 600
                  stats:
                    - legend: 2023
                      percentage: 5
                      color: green
                    - legend: 2022
                      percentage: 5
                      color: orange
          - name: 'column-group'
            data:
              variant: dark
              spacing: large
              background: /nuxt-images/developer-survey/Orb_2.png
              columns:
                - size: 7
                  aos_animation: fade-right
                  aos_duration: 600
                  blocks:
                  - title: Feeling the toolchain pressure
                    text: More than half of security respondents said they use 6 or more tools. This year there was also a drop in the number of security respondents who said they use 2-5 tools, and a corresponding increase in the number who said they use 6-10 tools.
                - size: 5
                  aos_animation: fade-left
                  aos_duration: 600
                  blocks:
                  - graphic:
                      name: 'globe-graphic'
                    stat:
                      percent: 57
                      description: of security respondents said they use 6 or more tools, compared to 48% of devs and 50% of ops.
                      delay: 500
          - name: 'column-group'
            data:
              variant: dark
              spacing: large
              columns:
                - size: 12
                  aos_animation: fade-right
                  aos_duration: 600
                  blocks:
                  - graphic:
                      name: 'word-cloud'
                      data:
                        - CI/CD 
                        - Easier automation
                        - Better security
                        - A more efficient DevOps practice
                        - Cost and time savings
                - size: 12
                  aos_animation: fade-left
                  aos_duration: 600
                  blocks:
                  - title: 'Teams want it all: Security and efficiency'
                    text: Better security was one of the top benefits of a DevSecOps platform, according to respondents, along with a more efficient DevOps practice, easier automation, cost and time savings, and better collaboration.
          - name: 'survey-quote-carousel'
            data:
              background:
                src: /nuxt-images/developer-survey/Orb_3.png
              graphic:
                src: /nuxt-images/developer-survey/half-globe.png
              quotes:
                - text: “There’s too much focus from Product on pushing out new features without taking the time to keep an eye on <b>security, code quality, and code rot.</b>”
                  author: Site Reliability Engineer, Media & Entertainment
                - text: “<b>Security is becoming more important</b> and quickly shows the gaps between traditional development methodologies such as waterfall and newer, product based organizations. In some ways, I see the gap between mature, capable teams and less mature teams as growing rather than closing.”
                  author: DevOps Leader, Business Services/Consulting
                - text: “There are an <b>overwhelming amount of vulnerabilities</b> to triage and resolve.”
                  author: Security Engineer, Computer Hardware/Services/Software/SaaS
          - name:  'survey-banner'
            variant: dark
            data:
              aos_animation: fade-up
              aos_duration: 600
              note: Explore the survey data
              title: Who took the survey

              description: |
                In March 2023, our Global DevSecOps Survey received over 5,000 responses. This year’s survey respondents come from organizations of all sizes and represent a wide variety of countries, industries, and job roles. Here’s a snapshot of who took the survey — get <a href="#download" data-ga-name="access to the full report series" data-ga-location="body">access to the full report series</a> to see the complete breakdown.
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Top 5 industries
                  aos_animation: fade-left
                  aos_duration: 600
                  stats:
                    - legend: Computer Hardware / Services / Software / SaaS
                      percentage: 39
                      color: green
                    - legend: Telecommunications
                      percentage: 6
                      color: red
                    - legend: Banking/Financial Services
                      percentage: 5
                      color: orange
                    - legend: Industrial Manufacturing
                      percentage: 5
                      color: purple
                    - legend: Retail
                      percentage: 5
                      color: blue
                - title: Top 5 job roles
                  aos_animation: fade-right
                  aos_duration: 600
                  stats:
                    - legend: Software Developer / Engineer
                      percentage: 19
                      color: green
                    - legend: Development / Engineering Manager / Director
                      percentage: 13
                      color: red
                    - legend: Technology Executive (CIO/CISO/CTO/VP)
                      percentage: 10
                      color: orange
                    - legend: DevOps Manager / Director
                      percentage: 6
                      color: purple
                    - legend: DevOps Engineer
                      percentage: 5
                      color: blue
          - name : 'survey-role-bubbles'
            data:
              background: /nuxt-images/developer-survey/Orb_4.png
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Functional area
              blocks:
                - percentage: 39
                  label: Software development
                  color: red
                - percentage: 32
                  label: IT operations
                  color: green
                - percentage: 29
                  label: IT security
                  color: yellow
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Organization size (employee headcount)
                  scale: 100
                  size: 12
                  aos_animation: fade-up
                  aos_duration: 600
                  stats:
                    - legend: 24 or fewer
                      percentage: 3
                      color: green
                    - legend: 25–49
                      percentage: 11
                      color: red
                    - legend: 50–99
                      percentage: 12
                      color: orange
                    - legend: 100–249
                      percentage: 23
                      color: purple
                    - legend: 250–499
                      percentage: 14
                      color: green
                    - legend: 500–999
                      percentage: 13
                      color: red
                    - legend: 1,000–2,499
                      percentage: 9
                      color: orange
                    - legend: 2,500–4,99
                      percentage: 5
                      color: purple
                    - legend: 5,000+
                      percentage: 10
                      color: blue
          - name: 'survey-region-bubbles'
            data:
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Region
              blocks:
                - percentage: 11
                  label: Europe
                  color: purple
                - percentage: 15
                  label: Asia
                  color: green
                - percentage: 71
                  label: Americas
                  color: yellow
                - percentage: 3
                  label: Australia & New Zealand
                  color: red
      - header: Productivity & Efficiency
        components:
          - name: 'survey-hero'
            data:
              title:
                text: Productivity & efficiency within reach
              description:
                text: 'In the second installment of our 2023 Global DevSecOps Report Series, we’ll dig into the current state of productivity, efficiency, and development velocity for organizations — and what organizations are doing to improve the way their teams and software delivery processes work. <br><br><p><a href="#download">Download the full report </a>to explore all of the findings, or read on for the highlights.</p>'
              aos_animation: fade-up
              aos_duration: 600
              cols: 6
              background: /nuxt-images/developer-survey/Orb_1.png
              graphic: 'globe-graphic'
              asset: half-globe
          - name:  'survey-banner'
            data:
              variant: dark
              aos_animation: fade-up
              title_tag: heading3-bold
              description_tag: heading5-bold
              padding_bottom_size: 64
              aos_duration: 600
              title: Productivity and efficiency on the roadmap
              description: |
                Many organizations have plans to improve their productivity and efficiency <i>this year</i>.
          - name: 'stats-grid'
            data:
              variant: dark
              aos_animation: fade-up
              aos_duration: 600
              blocks:
                - percent: 46
                  description: of all respondents said they are planning DevOps process optimizations this year.
                  delay: 1000
                - percent: 41
                  description: plan to introduce new analytics tools and dashboards to improve delivery efficiency this year.
                  delay: 1000
          - name: 'survey-stat-bars-section'
            data:
              title: 'The hiring and retention struggle is real'
              description: Nearly half of respondents (46%) said it’s somewhat or very difficult for their organization to attract, hire, and retain developers — and that only makes it more difficult to improve productivity and efficiency.
              aos_animation: fade-up
              aos_duration: 600
              scale: 25
              size: 12
              blocks:
                - description: How easy or difficult is it for your organization to attract, hire, and retain developers?
                  aos_animation: fade-right
                  aos_duration: 600
                  size: 12
                  stats:
                    - legend: Very difficult
                      percentage: 9
                      color: green
                    - legend: Somewhat difficult
                      percentage: 37
                      color: red
                    - legend: Neither easy nor difficult
                      percentage: 29
                      color: orange
                    - legend: Somewhat easy
                      percentage: 20
                      color: purple
                    - legend: Very easy
                      percentage: 5
                      color: green
          - name: 'column-group'
            data:
              variant: dark
              spacing: medium
              columns:
                - size: 12
                  aos_animation: fade-left
                  aos_duration: 600
                  blocks:
                  - title: 'What successful organizations are doing differently'
                    text: We dug into the data to surface what successful organizations are doing differently that may be helping them improve their team’s productivity and efficiency. Here are a few of the top factors.
          - name: 'survey-glow-pills'
            data:
              pills:
                - title: 'CI/CD'
                  description: Respondents using CI/CD were 2x more likely to deploy multiple times per day.
                  width: 592
                  color: green
                - title: 'AI/ML'
                  color: red
                - title: 'DORA and other metrics'
                  color: yellow
                - title: 'DevSecOps platform'
                  description: Respondents who use a DevSecOps platform were 1.7x more likely to onboard new developers in less than 4 weeks.
                  width: 670
                  color: purple
                - title: 'Deploying to the cloud'
                  color: green
          - name:  'survey-banner'
            data:
              variant: dark
              aos_animation: fade-up
              aos_duration: 600
              note: Explore the survey data
              title: Who took the survey

              description: |
                In March 2023, our Global DevSecOps Survey received over 5,000 responses. This year’s survey respondents come from organizations of all sizes and represent a wide variety of countries, industries, and job roles. Here’s a snapshot of who took the survey — get <a href="#download" data-ga-name="access to the full report series" data-ga-location="body">access to the full report series</a> to see the complete breakdown.
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Top 5 industries
                  aos_animation: fade-left
                  aos_duration: 600
                  stats:
                    - legend: Computer Hardware / Services / Software / SaaS
                      percentage: 39
                      color: green
                    - legend: Telecommunications
                      percentage: 6
                      color: red
                    - legend: Banking/Financial Services
                      percentage: 5
                      color: orange
                    - legend: Industrial Manufacturing
                      percentage: 5
                      color: purple
                    - legend: Retail
                      percentage: 5
                      color: blue
                - title: Top 5 job roles
                  aos_animation: fade-right
                  aos_duration: 600
                  stats:
                    - legend: Software Developer / Engineer
                      percentage: 19
                      color: green
                    - legend: Development / Engineering Manager / Director
                      percentage: 13
                      color: red
                    - legend: Technology Executive (CIO/CISO/CTO/VP)
                      percentage: 10
                      color: orange
                    - legend: DevOps Manager / Director
                      percentage: 6
                      color: purple
                    - legend: DevOps Engineer
                      percentage: 5
                      color: blue
          - name : 'survey-role-bubbles'
            data:
              background: /nuxt-images/developer-survey/Orb_4.png
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Functional area
              blocks:
                - percentage: 39
                  label: Software development
                  color: red
                - percentage: 32
                  label: IT operations
                  color: green
                - percentage: 29
                  label: IT security
                  color: yellow
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Organization size (employee headcount)
                  scale: 25
                  size: 12
                  aos_animation: fade-up
                  aos_duration: 600
                  stats:
                    - legend: 24 or fewer
                      percentage: 3
                      color: blue
                    - legend: 25–49
                      percentage: 11
                      color: purple
                    - legend: 50–99
                      percentage: 12
                      color: orange
                    - legend: 100–249
                      percentage: 23
                      color: red
                    - legend: 250–499
                      percentage: 14
                      color: green
                    - legend: 500–999
                      percentage: 13
                      color: purple
                    - legend: 1,000–2,499
                      percentage: 9
                      color: orange
                    - legend: 2,500–4,99
                      percentage: 5
                      color: red
                    - legend: 5,000+
                      percentage: 10
                      color: green
          - name: 'survey-region-bubbles'
            data:
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Region
              blocks:
                - percentage: 11
                  label: Europe
                  color: purple
                - percentage: 15
                  label: Asia
                  color: green
                - percentage: 71
                  label: Americas
                  color: yellow
                - percentage: 3
                  label: Australia & New Zealand
                  color: red
      - header: Artificial Intelligence
        components:
          - name: 'survey-hero'
            data:
              title:
                text: Artificial Intelligence
              description:
                text: Check back soon to learn how DevSecOps teams feel about the rise of AI — where AI is helping, where it's creating new challenges, and where respondents want to integrate AI into the software development process.
              aos_animation: fade-up
              aos_duration: 600
              background: /nuxt-images/developer-survey/Orb_1.png
              graphic: 'globe-graphic'
              asset: half-globe
              width: '1250'
              height: '837'
              position: bottom
              note:
                text: Coming soon
                variant: heading5-bold
              primary_btn:
                text: Sign up to be notified
                url: "#download"
                data_ga_name: sign up tp be notified
                data_ga_location: survey hero
                animation_duration: 1.2s
                icon:
                  name: docs
                  variant: marketing
          - name:  'survey-banner'
            data:
              variant: dark
              aos_animation: fade-up
              aos_duration: 600
              note: Explore the survey data
              title: Who took the survey
              padding_top_size: 96

              description: |
                In March 2023, our Global DevSecOps Survey received over 5,000 responses. This year’s survey respondents come from organizations of all sizes and represent a wide variety of countries, industries, and job roles. Here’s a snapshot of who took the survey — get <a href="#download" data-ga-name="access to the full report series" data-ga-location="body">access to the full report series</a> to see the complete breakdown.
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Top 5 industries
                  aos_animation: fade-left
                  aos_duration: 600
                  stats:
                    - legend: Computer Hardware / Services / Software / SaaS
                      percentage: 39
                      color: green
                    - legend: Telecommunications
                      percentage: 6
                      color: red
                    - legend: Banking/Financial Services
                      percentage: 5
                      color: orange
                    - legend: Industrial Manufacturing
                      percentage: 5
                      color: purple
                    - legend: Retail
                      percentage: 5
                      color: blue
                - title: Top 5 job roles
                  aos_animation: fade-right
                  aos_duration: 600
                  stats:
                    - legend: Software Developer / Engineer
                      percentage: 19
                      color: green
                    - legend: Development / Engineering Manager / Director
                      percentage: 13
                      color: red
                    - legend: Technology Executive (CIO/CISO/CTO/VP)
                      percentage: 10
                      color: orange
                    - legend: DevOps Manager / Director
                      percentage: 6
                      color: purple
                    - legend: DevOps Engineer
                      percentage: 5
                      color: blue
          - name : 'survey-role-bubbles'
            data:
              background: /nuxt-images/developer-survey/Orb_4.png
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Functional area
              blocks:
                - percentage: 39
                  label: Software development
                  color: red
                - percentage: 32
                  label: IT operations
                  color: green
                - percentage: 29
                  label: IT security
                  color: yellow
          - name: 'survey-stat-bars-section'
            data:
              blocks:
                - title: Organization size (employee headcount)
                  scale: 25
                  size: 12
                  aos_animation: fade-up
                  aos_duration: 600
                  stats:
                    - legend: 24 or fewer
                      percentage: 3
                      color: blue
                    - legend: 25–49
                      percentage: 11
                      color: purple
                    - legend: 50–99
                      percentage: 12
                      color: orange
                    - legend: 100–249
                      percentage: 23
                      color: red
                    - legend: 250–499
                      percentage: 14
                      color: green
                    - legend: 500–999
                      percentage: 13
                      color: purple
                    - legend: 1,000–2,499
                      percentage: 9
                      color: orange
                    - legend: 2,500–4,99
                      percentage: 5
                      color: red
                    - legend: 5,000+
                      percentage: 10
                      color: green
          - name: 'survey-region-bubbles'
            data:
              aos_animation: fade-up
              aos_duration: 600
              aos_offset: 0
              title: Region
              blocks:
                - percentage: 11
                  label: Europe
                  color: purple
                - percentage: 15
                  label: Asia
                  color: green
                - percentage: 71
                  label: Americas
                  color: yellow
                - percentage: 3
                  label: Australia & New Zealand
                  color: red
      - header: 'Coming soon'
        disabled: true
download:
  form_block:
    theme: light
    icon:
      name: mail-open
      variant: marketing
      alt: Mail Icon
    header: Get all of the 2023 Global DevSecOps Reports, right in your inbox.
    description: Sign up to receive email updates when new content is available.
    form:
      form_id: 1002
      datalayer: mail-open
  links_block:
    theme: light
    header: Dive into our previous reports
    links:
      - text: 2022
        url: previous/2022/
      - text: 2021
        url: https://about.gitlab.com/developer-survey/previous/2021/
      - text: 2020
        url: https://about.gitlab.com/developer-survey/previous/2020/
      - text: 2019
        url: https://about.gitlab.com/developer-survey/previous/2019/
      - text: 2018
        url: https://about.gitlab.com/developer-survey/previous/2018/