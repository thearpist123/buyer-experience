---
  title: Monitor
  description: Learn how GitLab helps automatically monitor metrics so you know how any change in code impacts your production environment. View more here!
  components:
    - name: sdl-cta
      data:
        title: Monitor
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Help reduce the severity and frequency of incidents.
        text: |
          Get feedback and the tools to help you reduce the severity and frequency of incidents so that you can release software frequently with confidence.
        icon:
          name: monitor-alt-2
          alt: Monitor Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Runbooks
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Runbooks are a collection of documented procedures that explain how to carry out a particular process, be it starting, stopping, debugging, or troubleshooting a particular system. Executable runbooks allow operators to execute pre-written code blocks or database queries against a given environment.
            link:
              href: https://docs.gitlab.com/ee/user/project/clusters/runbooks/
              text: Learn More
              data_ga_name: runbooks learn more
              data_ga_location: body
          - title: Metrics
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab collects and displays performance metrics for deployed apps, leveraging Prometheus. Developers can determine the impact of a merge and keep an eye on their production systems, without leaving GitLab.
            link:
              href: https://docs.gitlab.com/ee/operations/metrics/
              text: Learn More
              data_ga_name: metrics learn more
              data_ga_location: body
          - title: Incident Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Track incidents within GitLab, providing a consolidated location to understand the who, what, when, and where of the incident. Define service level objectives and error budgets, to achieve the desired balance of velocity and stability.
            link:
              href: https://docs.gitlab.com/ee/operations/incident_management/
              text: Learn More
              data_ga_name: incident management learn more
              data_ga_location: body
          - title: On-call Schedule Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Track DevSecOps responsibilities within your team by creating rotating schedules for responders.
            link:
              href: https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html
              text: Learn More
              data_ga_name: on-call schedule management learn more
              data_ga_location: body
          - title: Logging
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              GitLab makes it easy to view the logs distributed across multiple pods and services using log aggregation with Elastic Stack. Once Elastic Stack is enabled, you can view your aggregated Kubernetes logs across multiple services and infrastructure, go back in time, conduct infinite scroll, and search through your application logs from within the GitLab UI itself.
            link:
              href: https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#kubernetes-pod-logs
              text: Learn More
              data_ga_name: logging learn more
              data_ga_location: body
          - title: Tracing
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Tracing provides insight into the performance and health of a deployed application, tracking each function or microservice which handles a given request. This makes it easy to understand the end-to-end flow of a request, regardless of whether you are using a monolithic or distributed system.
            link:
              href: https://docs.gitlab.com/ee/operations/tracing.html
              text: Learn More
              data_ga_name: tracing learn more
              data_ga_location: body
          - title: Error Tracking
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Error tracking allows developers to easily discover and view the errors that their application may be generating. By surfacing error information where the code is being developed, efficiency and awareness can be increased.
            link:
              href: https://docs.gitlab.com/ee/operations/error_tracking.html
              text: Learn More
              data_ga_name: error tracking learn more
              data_ga_location: body
          - title: Product Analytics
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              You can enable and configure product analytics to track events within your project applications on a self-managed instance.
            link:
              href: https://docs.gitlab.com/ee/user/product_analytics/
              text: Learn More
              data_ga_name: product analytics learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/monitor/){data-ga-name="monitor direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Release
            icon:
              name: release-alt-2
              alt: Release Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Verify
            icon:
              name: verify-alt-2
              alt: Verify Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
          - title: Package
            icon:
              name: package-alt-2
              alt: Package Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Create a consistent and dependable software supply chain with built-in package management.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/package/
              data_ga_name: package
              data_ga_location: body
