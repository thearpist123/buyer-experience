# Do not use components in this directory

We built the page at `/ten/` on a tight due date, as well as these components. Please do not use these components elsewhere or model future code off of the structure. We prioritized expediency over reusability and good architecture. 
