module.exports = {
  ci: {
    collect: {
      numberOfRuns: 5,
      startServerCommand: 'HOST=0 PORT=3000 yarn start',
      url: ['http://localhost:3000/', 'http://localhost:3000/pricing/'],
      settings: {
        onlyCategories: [
          'performance',
          'accessibility',
          'best-practices',
          'seo',
        ],
        skipAudits: ['uses-http2'],
        chromeFlags: '--disable-dev-shm-usage --headless',
        preset: 'desktop',
      },
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
