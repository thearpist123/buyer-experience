export const DEFAULT_META_DESCRIPTION =
  'Learn more from GitLab, The One DevOps Platform for software innovation.';
export const TWITTER_CREATOR_CONTENT = '@Gitlab';
export const TWITTER_SITE_CONTENT = '@Gitlab';
export const TWITTER_CARD_CONTENT = 'summary_large_image';
export const DEFAULT_OPENGRAPH_IMAGE =
  '/nuxt-images/open-graph/open-graph-gitlab.png';
export const SITE_URL = 'https://about.gitlab.com';

export enum META_NAME {
  description = 'description',
  ogDescription = 'og:description',
  twitterDescription = 'twitter:description',
  twitterTitle = 'twitter:title',
  twitterAltImage = 'twitter:image:alt',
  twitterImage = 'twitter:image',
  twitterCreator = 'twitter:creator',
  twitterCard = 'twitter:card',
  twitterSite = 'twitter:site',
  ogImage = 'og:image',
  ogImageAlt = 'og:image:alt',
  ogTitle = 'og:title',
  article = 'article',
  website = 'website',
  ogType = 'og:type',
  ogUrl = 'og:url',
}

export const RESOURCE_ICON_PATH = Object.freeze({
  case_study: 'case-study',
  article: 'articles',
  book: 'book',
  blog: 'blog',
  podcast: 'podcast-alt',
  report: 'report',
  video: 'video',
  webcast: 'webcast',
  whitepaper: 'whitepapers',
});

export enum FEATURES_HOSTING {
  saas = 'SaaS',
  self_managed = 'Self-Managed',
}

export enum TEMPLATES {
  Default = 'default',
  Industry = 'industry',
  IndustryAlternative = 'industry-alt',
  Feature = 'feature',
}

export const DEFAULT_SCHEMA_ORG = {
  '@context': 'https://schema.org',
  '@type': 'Corporation',
  name: 'GitLab',
  legalName: 'GitLab Inc.',
  tickerSymbol: 'GTLB',
  url: 'https://about.gitlab.com',
  logo: 'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png',
  description:
    'GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form our culture.',
  foundingDate: '2011',
  founders: [
    { '@type': 'Person', name: 'Sid Sijbrandij' },
    { '@type': 'Person', name: 'Dmitriy Zaporozhets' },
  ],
  slogan:
    'Our mission is to change all creative work from read-only to read-write so that everyone can contribute.',
  address: {
    '@type': 'PostalAddress',
    streetAddress: '268 Bush Street #350',
    addressLocality: 'San Francisco',
    addressRegion: 'CA',
    postalCode: '94104',
    addressCountry: 'USA',
  },
  awards:
    "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing - Challenger, DevOps Dozen award for the Best DevOps Solution Provider for 2019, 451 Firestarter Award from 451 Research",
  knowsAbout: [
    { '@type': 'Thing', name: 'DevOps' },
    { '@type': 'Thing', name: 'CI/CD' },
    {
      '@type': 'Thing',
      name: 'DevSecOps',
    },
    { '@type': 'Thing', name: 'GitOps' },
    { '@type': 'Thing', name: 'DevOps Platform' },
  ],
  sameAs: [
    'https://www.facebook.com/gitlab',
    'https://twitter.com/gitlab',
    'https://www.linkedin.com/company/gitlab-com',
    'https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg',
  ],
};

export const SEARCH_TYPE = Object.freeze({
  MARKETING: 'marketing',
  HANDBOOK: 'handbook',
  BLOG: 'blog',
});

export const SEARCH_SORT_OPTIONS = Object.freeze([
  {
    label: 'Suggested',
    value: '_score',
    queryValue: 'suggested',
  },
  {
    label: 'Most popular',
    value: 'popularity',
    queryValue: 'most_popular',
  },
  {
    label: 'Last updated',
    value: 'updated_at',
    queryValue: 'last_updated',
  },
]);

export const LANG_PATH = Object.freeze({
  GERMAN: '/de-de/',
  JAPANESE: '/ja-jp/',
  FRENCH: '/fr-fr/',
});

export const CODE_HIGHLIGHTING = Object.freeze({
  javascript: {
    keywords: [
      'var',
      'let',
      'const',
      'if',
      'else',
      'for',
      'while',
      'function',
      'return',
      'new',
      'this',
      'import',
    ],
  },
  python: {
    keywords: ['import', 'def', 'for', 'if', 'else', 'while', 'return'],
  },
  go: {
    keywords: ['import', 'for', 'if', 'else', 'while', 'return', 'func'],
  },
  operators: [
    '=',
    '+',
    '-',
    '*',
    '/',
    '==',
    '===',
    '!=',
    '!==',
    '>',
    '<',
    '>=',
    '<=',
    ':=',
  ],
  brackets: ['{', '}', '(', ')', '[', ']'],
});
